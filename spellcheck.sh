#!/bin/bash

if [ $# -eq 1 ]; then
	aspell --lang=de_DE --home-dir=. --personal=aspell.de.pws --repl=aspell.de.prepl -t check $1
else
    echo "Too many or to few arguments. (Exactly one file needed.)"
fi

