GRAPHS= res/graph/classes-after.png \
		res/graph/classes-complete-refactoring.png \
		res/graph/classes-prev.png \
		res/graph/jpeg-classes.png \
		res/graph/jpeg-encoder-schema.png \
		res/graph/mjpeg-rtp-check-classes.png \
		res/graph/qtff-moov.png \
		res/graph/riff-avi.png \
		res/graph/srtp-classes.png \
		res/graph/vlc-mjpeg-format.png
LATEX_FLAGS += -halt-on-error
VERAPDF = /home/emanuel/bin/verapdf/verapdf

all: diplomarbeit

diplomarbeit: src/diplomarbeit.tex $(GRAPHS)
	pdflatex ${LATEX_FLAGS} $< # creates toc
	makeglossaries $@
	biber $@
	pdflatex ${LATEX_FLAGS} $< # pdf with correct toc
	pdflatex ${LATEX_FLAGS} $< # pdf with correct bib

graphs: ${GRAPHS}

res/graph/%.png: res/graph/%.dot
	dot -T png -o $@ $^

clean:
	rm -f res/graph/*.png *.acn *.acr *.alg *.aux *.bbl *.bcf *.blg *.glg *.glo \
		*.gls *.ist *.lof *.log *.lol *.lot *.out *.pdf *.toc *.xml *.xmpi

validation:
	${VERAPDF} -f 2b --format xml -v diplomarbeit.pdf > validation.xml

.PHONY: diplomarbeit clean validation

