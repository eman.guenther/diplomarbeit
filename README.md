# Diplomarbeit

Diplomarbeit im Studiengang Medieninformatik an der Hochschule für Technik und Wirtschaft Dresden

## Lizenz

Die Arbeit ist lizensiert unter der [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).
Ausgenommen davon sind die Dateien im Verzeichnis [res/img/](res/img/).

