\chapter{JPEG-Transport über RTP}

Für die Echtzeitübertragung von Daten wurde von der Internet Society das \gls{rtp} \cite{rfc3550} standardisiert.
Weitere Standards definieren die Übertragung bestimmter Datentypen über das RTP.
Neben Videokompressionsstandards wie H.264 (\acrshort{vgl} \cite{rfc6184}) und HEVC (\acrshort{vgl} \cite{rfc7798}) wird so auch das Übertragungsformat für \gls{jpeg}-Daten standardisiert.
Die entsprechenden Ausführungen sind im RFC 2435 \cite{rfc2435} zu finden.
Außerdem werden die Einschränkungen dieses Standards für die Übertragung von \gls{mjpeg}-Videos im ersten Abschnitt dieses Kapitels näher betrachtet.
Im Vergleich zu den anderen Videokompressionsverfahren hat die Übertragung solcher Videos den Vorteil, dass einzelne Bilder unabhängig voneinander codiert werden.
Auf diese Weise können die Bilder sofort komprimiert und übertragen, aber auch vom Empfänger wieder decodiert werden.
Diese Unabhängigkeit der Bilder voneinander resultiert in der Echtzeitfähigkeit des Verfahrens.

Neben der Übertragung von in Echtzeit aufgenommenen und codierten Bildern lässt das in RFC 2435 standardisierte Verfahren aber auch zu, dass aufgezeichnete \gls{mjpeg}-Daten übertragen werden.
Für die Speicherung dieser Daten stehen verschiedene Formate zur Verfügung, die in Kapitel \ref{chap:mjpeg} schon betrachtet wurden.
Da kein einheitlicher Standard für das Datenformat \gls{mjpeg} existiert und somit für die Kompression der einzelnen Bilder keine Parameter vorgegeben sind, können entsprechende Dateien nicht ohne Weiteres für die Übertragung verwendet werden.
Die Nutzung von inkompatiblen Daten für den Transport solcher Videos über \gls{rtp} würde dazu führen, dass das Medium auf der Seite des Empfängers nicht decodiert werden kann.
Deshalb ist eine Offline"=Analyse von \gls{mjpeg}-Videos enthaltenden Dateien sinnvoll.
Die Bezeichnung "`offline"' bedeutet dabei, dass die Analyse noch vor dem Start des Übertragungsprogrammes stattfindet.
Im zweiten Abschnitt wird deshalb eine Implementierung eines solchen Analysewerkzeuges erläutert.
Als Konsequenz der vorangegangenen Betrachtung schließt sich die Überlegung an, welche Konvertierungsprogramme valide Daten im Sinne des RFC 2435 \cite{rfc2435} erzeugen können.
Diese wird den Abschluss dieses Kapitels bilden.


\section{Anforderungen des RFC 2435}
\label{sec:rfc2435}

Um die Kompatibilität der \gls{mjpeg}"=Videoübertragung zwischen verschiedenen Systemen zu gewährleisten, trifft der RFC 2435 \cite{rfc2435} viele Einschränkungen.
Dazu gehören zum einen die Festlegung bestimmter Parameter für die \gls{jpeg}"=Kompression, zum anderen wird ein Format für den Transport der Daten geschaffen.
Da der Fokus dieser Arbeit auf der Bereitstellung der benötigten Videodaten liegt, wird nicht weiter auf das Übertragungsformat eingegangen.
Nachfolgend sollen die Einschränkungen der Kompressionparameter betrachtet werden.
Tabelle \ref{tab:rtp-jpeg} gibt eine kompakte Übersicht über dieselben.
Einige Festlegungen des Übertragungsstandards werden bereits durch das im \gls{jpeg}-Standard definierte Dateiformat oder durch den \gls{jfif}-Standard gefordert.
Diese sind in der Übersicht mit dem Symbol $*$ gekennzeichnet.

\begin{table}[h]
\centering
\begin{tabularx}{\linewidth}{|p{18mm}|X|X|}
    \hline
    \textbf{RFC-Abschnitt} & \textbf{Einschränkung} & \textbf{Feststellung in JFIF-Daten} \\
    \hline
    2. & sequenzieller Baseline"=Modus, Huffman"=Entropiecodierung & genau ein \gls{m-sof0}- und kein anderer \gls{m-sof}-Marker \\
    \hline
    2. & ein \gls{interleaved} Scan & genau ein \gls{m-sos}-Marker \\
    \hline
    3.1.5., 3.1.6. & \gls{max} jeweils 2040 Pixel Höhe und Breite & $Y$- und $X$-Werte im \gls{m-sof}-Segment \\
    \hline
    3.1.8. & $*$ Quantisierungswerte mit 8 oder 16 Bit Genauigkeit & $P_{q}$ im \gls{m-dqt}-Segment \\
    \hline
    3.1.9. & $*$ Kennzeichnung von \texttt{0xff}-Bytes in den Daten durch ein nachfolgendes \texttt{0x00}-Byte & - \\
    \hline
    4.1. & 8 Bit Genauigkeit für Abtastwerte & $P$ im \gls{m-sof}-Segment \\
    \hline
    4.1. & quadratische Pixel & Berechnung aus $Hdensity$ und $Vdensity$ im \gls{m-app0}-Segment \\
    \hline
    1., 4.1. & $*$ drei Komponenten im \gls{ycbcr-modell} & $N_{f}$ im \gls{m-sof}-Segment \\
    \hline
    4.1. & $*$ Scan nutzt Komponenten 1 (Y), 2 (Cb), 3 (Cr) in dieser Reihenfolge & - \\
    \hline
    4.1. & zwei Quantisierungstabellen & $T_{q}$-Werte im \gls{m-dqt}-Segment \\
    \hline
    4.1. & Huffmantabellen aus \gls{jpeg}-Standard \cite{iso10918-1}, Annex K.3 & $T_{c}$- und $T_{h}$-Werte im \gls{m-dht}-Segment \\
    \hline
    4.1. & Unterabtastung \texttt{4:2:2} oder \texttt{4:2:0} & $H_{i}$- und $V_{i}$-Werte im \gls{m-sof}-Segment \\
    \hline
\end{tabularx}
    \caption[Einschränkungen des RFC 2435 bezüglich der Kompressionsparameter.]{
        Einschränkungen des RFC 2435 \cite{rfc2435} bezüglich der Kompressionsparameter.
        Ist die Einschränkung am Beginn mit $*$ gekennzeichnet, wird diese auch vom Datenformat des \gls{jpeg}- \acrshort{bzw} dem \gls{jfif}-Standard gefordert.
        Die Einhaltung derselben ist demnach schon für die Erstellung eines standardkonformen Datenstromes erforderlich.}
    \label{tab:rtp-jpeg}
\end{table}

Wie in Kapitel \ref{chap:jpeg-jfif} schon ausgeführt, stellt das \gls{jpeg}-Verfahren verschiedene Kompressionsmodi zur Verfügung.
Um eine möglichst gute Interoperabilität zu gewährleisten, schränkt der Standard verwendbaren Modi auf den Baseline-Modus ein.
Denn dieser Modus muss von allen Decodern implementiert werden (\acrshort{vgl} \cite{strutz09}, \gls{seite} 204).
Im Baseline-Modus ist außerdem festgelegt, dass die Dekorrelation auf einer \gls{dct} basiert und zur Entropiecodierung ein Huffman-Code eingesetzt wird.
Des Weiteren unterstützt dieser Modus ausschließlich Abtastwerte von 8 Bit pro Pixel und pro Komponente.
Die Verwendung des Baseline-Modus kann im \gls{jpeg}-Bild nachgeprüft werden, indem dieses nur ein \gls{m-sof0}-Segment enthält.

Eine weitere Festlegung wird bezüglich der Anzahl im Bild enthaltener Scans getroffen.
Damit das Bild von Decodern auf verschiedenen Hardware"=Konfigurationen dekomprimiert werden kann, darf es nur ein einziges \gls{m-sos}-Segment enthalten (\acrshort{vgl} \cite{rfc2435}, \gls{seite} 2).
In diesem Scan werden die Daten der Farbkomponenten \gls{interleaved} gespeichert.
Ob ein Bild dieser Forderung genügt, lässt sich demzufolge durch die Anzahl der enthaltenen \gls{m-sos}-Marker bestimmen.

\vspace{1em}
Aus dem \gls{m-sof0}-Segment lassen sich noch weitere Parameter auslesen, die für die Übertragung bestimmte Werte vorweisen müssen.
Dazu gehören unter anderem die Höhe und Breite des Bildes.
Deren Einschränkung ergibt sich aus der Größe der Datenfelder im \gls{jpeg}-Header, welcher für die Übertragung der Daten genutzt wird.
Bei einer Genauigkeit von 8 Bit pro Wert wird die Dimension jeweils als Vielfaches von 8 Pixeln angegeben.
Daraus resultiert eine maximale Höhe und Breite von jeweils 2040 Pixeln.
Die Farbunterabtastung lässt sich anhand der Informationen zu den enthaltenen Komponenten bestimmen, die sich im \gls{m-sof0}-Segment befinden.
Der Standard zur Übertragung von \gls{jpeg}-Daten über \gls{rtp} gibt dabei vor, dass diese entweder \texttt{4:2:2} oder \texttt{4:2:0} entsprechen muss.

Die Speicherung der Bilddaten im \gls{jfif}-Format erfordert bereits, dass die Farbkomponenten des Bildes im \gls{ycbcr-modell} vorliegen müssen.
Dabei besteht jedoch die Option, nur eine Komponente einzubinden und so ein Graustufenbild zu speichern.
Die Bestimmungen des RFC 2435 \cite{rfc2435} gehen diesbezüglich noch weiter.
So müssen alle drei Komponenten des genannten Farbmodelles vorliegen.
Ein Graustufenbild müsste deshalb auch die Chrominanzen integrieren.
Die für diese Überprüfung benötigten Informationen liegen ebenfalls im \gls{m-sof0}-Segment vor.
Außerdem ist für die Übertragung festgelegt, dass quadratische Pixel verwendet werden müssen.
Aus dem \gls{m-app0}-Segment lassen sich dafür die horizontale und vertikale Pixeldichte auslesen.
Anschließend kann das Seitenverhältnis der Pixel aus diesen Angaben bestimmt werden.

Der Standard RFC 2435 \cite{rfc2435} macht ebenfalls Angaben zu den Quantisierungstabellen.
So muss jeweils eine Tabelle für die Luminanz- und Chrominanzwerte vorhanden sein.
Deren Anzahl im Bild lässt sich durch die Menge an \gls{m-dqt}-Segmenten und den darin enthaltenen Tabellen entnehmen.

Um die Menge an zu übertragenden Daten etwas zu reduzieren, wurde für die Entropiecodierung festgelegt, dass die Huffman-Tabellen aus dem \gls{jpeg}-Standard Annex K.3 \cite{iso10918-1} verwendet werden müssen.
Dabei handelt es sich um vier Tabellen, die für Luminanz- und Chrominanzwerte jeweils DC- und AC-Komponenten abdecken.
Durch diese Festlegung müssen die Huffman-Tabellen nicht mit den Daten übertragen werden.


\section{Implementierung eines JPEG-Analyseprogramms}

Die Aufgabe des Analyseprogramms für \gls{mjpeg}-Dateien liegt in der Überprüfung der Metadaten der enthaltenen Bilder auf die in Abschnitt \ref{sec:rfc2435} beschriebenen Anforderungen.
Eine grafische Benutzeroberfläche ist dafür nicht notwendig, weshalb die entwickelte Software ausschließlich Text auf die Kommandozeile ausgeben kann.

Wie bereits in Abschnitt \ref{chap:introduction} erwähnt wurde, soll die Entwicklung des Programmes in derselben Umgebung wie der des Projektes zur Übertragung von \gls{mjpeg}"=Videos stattfinden.
Aus diesem Grund erfolgt die Realisierung des Analyseprogrammes in der Programmiersprache Java.
Der Entwurf des Programmes nutzt als Kernkomponente die von der \texttt{javax.imageio}"=Bibliothek bereitgestellten Funktionalität, Metadaten aus \gls{jpeg}"=Bildern auszulesen.
Auf diese Weise wird zum einen die Komplexität der Software gering gehalten, während gleichzeitig ihre Fehleranfälligkeit durch die Verwendung einer viel getesteten Bibliothek reduziert wird.

Damit entspricht die im Analyseprogramm zu realisierende Funktionalität der Steuerung des Einlesens von \gls{jpeg}"=Bildern sowie der Verarbeitung der extrahierten Metadaten.
Der Einlesevorgang kann als separate Subfunktionalität ausgegliedert werden, sodass das Hauptprogramm neue Daten ohne Kenntnis über die Implementierung des Auslesens abrufen kann.
Die Analyse der von der Bibliotheksfunktionalität erhaltenen Metadaten beschreibt ebenfalls eine Unteraufgabe.
Ihre Grundlage bildet die strukturierte Darstellung der Daten sowie deren Vergleich mit den in Abschnitt \ref{sec:rfc2435} analysierten Vorgaben.
Dazu sind die entsprechenden Angaben in der verwendeten Struktur vorzuhalten und eine Vergleichsoperation zu implementieren.

Um verschiedene Betrachtungsfälle möglich zu machen und die Nutzerschnittstelle des Programmes erweiterbar zu halten, werden verschiedene Kommandozeilenparameter unterstützt.
Diese werden in Abschnitt \ref{sec:prog-cli-params} betrachtet.
Die interne Funktionsweise des Programmes, und insbesondere die Extraktion und Aufarbeitung der Metadaten sowie die Überprüfung derselben auf die Vorgaben des RFC 2435 \cite{rfc2435}, findet in Abschnitt \ref{sec:prog-internals} Erwähnung.


\subsection{Kommandozeilenparameter}
\label{sec:prog-cli-params}

Das Analyseprogramm muss die über die Kommandozeile angegebenen Parameter schon allein deshalb beachten und verarbeiten, um den Namen der zu analysierenden Datei zu erfahren.
Indem weitere Parameter angegeben und erkannt werden können, wird die Funktionalität der Software erweitert.
Alle gültigen Optionen für das Programm sind in Abbildung \ref{fig:prog-cli-params} dargestellt.
Es spielt dabei keine Rolle, in welcher Reihenfolge sie angegeben werden.
Wird ausschließlich der Name der zu untersuchenden Datei angegeben, erfolgt lediglich die Extraktion und Anzeige der Metadaten des ersten enthaltenen \gls{jpeg}-Bildes.
Dieses Verhalten macht sich die Annahme zunutze, dass alle Bilder einer \gls{mjpeg}-Datei mit denselben Parametern komprimiert wurden und somit dieselben Metadaten besitzen.
Statt einer Laufzeit von ungefähr 750 Millisekunden für die Überprüfung aller Bilder wird so eine deutlich kürzere Ausführungszeit von nur etwa 150 Millisekunden für ein Video mit 2800 Bildern benötigt.

\begin{figure}[h]
    \centering
\begin{lstlisting}[style=packet,language=]
usage: MJpegRtpCheck [options] file
  -c,--compliance  check for compliance with RFC 2435
  -f,--full-parse  parse all images of the file, not just one
  -h,--help        print this message
  -v,--version     print version and license info
\end{lstlisting}
    \caption{Kommandozeilenparameter des \gls{jpeg}"=Dateianalysators}
    \label{fig:prog-cli-params}
\end{figure}

Eine vollständige Überprüfung der Datei ist mit der Option \texttt{full-parse} möglich.
Damit kann die interne Konsistenz der Daten überprüft werden.
Diese erfolgt, indem die Metadaten aller Bilder extrahiert und verglichen werden.
Als Referenz für den Vergleich dienen dabei immer die Metadaten des ersten \gls{jpeg}-Bildes der Datei.
Sollten bei der Überprüfung unterschiedliche Kompressionsparameter festgestellt werden, wird der Nutzer darüber informiert.
Abschließend erfolgt in jedem Fall die Ausgabe der extrahierten Metadaten des ersten Bildes.

Der eigentliche Nutzen des Programmes liegt jedoch vor allem in der Überprüfung eines \gls{mjpeg}-Videos auf die Vorgaben des RFC 2435 \cite{rfc2435}.
Diese wird durchgeführt, wenn dem Programm die Option \texttt{compliance} als Parameter übergeben wird.
Dass dieses Verhalten durch die Angabe eines Parameters erzielt wird und dieses nicht das Standardverhalten ist, wurde im Hinblick auf die Erweiterungsmöglichkeit des Programmes entschieden.
Auf diese Weise können weitere Funktionalitäten leicht implementiert und über einen weiteren Kommandozeilenparameter zugänglich gemacht werden.
Der Aufruf des Programmes mit der Option \texttt{compliance} bewirkt, dass die angegebene Datei auf die Eignung für eine \gls{jpeg}"=Übertragung per \gls{rtp} überprüft wird.
Für jede Anforderung erfolgt eine Ausgabe, ob diese erreicht wurde oder nicht.
Abschließend stellt eine zusammenfassende Ausgabe fest, ob die überprüfte Datei für eine solche Übertragung geeignet ist.
Beispiele für die Ausgaben des Programmes sind im Anhang \ref{chap:appendix-analysator-output} zu finden.
Die Kombination der Parameter \texttt{full-parse} und \texttt{compliance} bewirkt das zu erwartende Verhalten:
Zunächst werden die Metadaten aller Bilder auf Gleichheit überprüft.
Anschließend wird verglichen, ob sie den Anforderungen der Übertragung gemäß RFC 2435 \cite{rfc2435} entsprechen.


\subsection{Interne Funktionsweise}
\label{sec:prog-internals}

Bevor Bilder aus der angegebenen Datei ausgelesen werden, erfolgt eine Prüfung der Dateinamensendung.
Dabei wird diese mit einer Liste an bekannten Endungen verglichen.
Jedoch weist die Endung nicht immer zweifelsfrei auf den Inhalt der Datei hin.
Ein Beispiel dafür sind Containerformate, die viele verschiedene Mediendatenströme enthalten können.
Im Fall des Analyseprogramms muss die Datei eindeutig als \gls{mjpeg} gekennzeichnet oder ein unterstütztes Containerformat sein.
Konkret ist diese Anforderung durch die Endungen \texttt{mjpeg}, \texttt{mjpg}, \texttt{avi} und \texttt{mov} erfüllt.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{res/graph/mjpeg-rtp-check-classes.png}
    \caption{Vereinfachte Klassenstruktur des Jpeg-Dateianalysators.}
    \label{fig:mjpeg-rtp-check-classes}
\end{figure}

Im Anschluss an die Überprüfung der Dateiendung erfolgt das Auslesen der Bilddaten sowie die Verarbeitung derselben.
Eine vereinfachte Klassenstruktur der daran beteiligten Komponenten wird in Abbildung \ref{fig:mjpeg-rtp-check-classes} gezeigt.
In dieser sind auch die Schnittstellen der Klassen dargestellt, welche Aufschluss über die Abfolge der Verarbeitung gibt.

Die Klasse \texttt{VideoFileBuffer} stellt mit der Methode \texttt{nextJpeg()} die aus der Datei ausgelesenen \gls{jpeg}"=Bilder bereit.
Dabei implementiert sie einen Puffer, welcher das Einlesen der Daten effizienter gestaltet.
Auf diese Weise werden bei jedem Lesezugriff auf die Datei mehrere Byte auf einmal ausgelesen.
So können die Zugriffe auf die Datei möglichst gering und effizient gehalten werden.
In den gelesenen Daten wird nach den \gls{m-soi}- und \gls{m-eoi}-Markern des \gls{jpeg}-Bildes gesucht.
Das auf diese Weise gefundene Bild kann an die aufrufende Umgebung zurückgegeben und anschließend weiterverarbeitet werden.
Sind jedoch mehr Daten ausgelesen worden als benötigt, werden diese an den Anfang des Puffers verschoben und zwischengespeichert.
Der so entstandene freie Speicher im Puffer kann vor der Suche nach dem nächsten Bild wieder gefüllt werden.

Aus dem ausgelesenen Bild werden anschließend die Metadaten extrahiert.
Dazu wird der statische Aufruf \texttt{JpegMetadataExtractor.extractMetadata()} verwendet, welcher die Funktionalität der Java"=Erweiterung \texttt{javax.imageio} kapselt.
Diese extrahiert Metadaten aus einem bereitgestellten \gls{jpeg}"=Bild.
Allerdings werden diese Metadaten in einer mit \gls{xml} vergleichbaren Struktur dargestellt, welche die Weiterverarbeitung erschweren würde.
Aus diesem Grund ist ein weiteres Parsen der Daten erforderlich.
Dadurch können sie in die eigene Struktur \texttt{JpegRtpMetadata} überführt werden.
Die so gegliederten Daten können dann ohne Probleme ausgegeben oder weiterverarbeitet werden.

Je nach geforderter Funktionalität können die extrahierten Metadaten weiter genutzt werden.
Eine Ausgabe der Datenstruktur erfolgt mit dem Aufruf \texttt{printMetadata()}.
Sollen die Metadaten jedoch auf die Anforderungen des RFC 2435 \cite{rfc2435} überprüft werden, müssen sie weiter untersucht werden.
Die entsprechende Verarbeitung erfolgt mit dem Aufruf der Methode \texttt{checkRtp2435Conformance()} der Klasse \texttt{JpegRtpMetadata}.
Besagte Klasse implementiert die private Methode \texttt{rtp2435ConformantData()}, welche ein mit den entsprechenden Parametern initialisiertes Objekt derselben Klasse zurückgibt.
Die Realisierung dieser Funktionalität als private Methode ermöglicht den internen Zugriff auf die benötigten Angaben, während sie vor anderen Klassen verborgen bleibt.
Damit bleibt die Schnittstelle zur Funktionalität der Klasse \texttt{JpegRtpMetadata} klar und übersichtlich.

Für die Überprüfung der extrahierten Metadaten werden diese mit den vom RFC 2435 \cite{rfc2435} geforderten verglichen.
Dabei erfolgt für jedes Datenfeld eine farbige Ausgabe, ob die Videodaten den Anforderungen entsprechen.
Ein Beispiel für einen fehlgeschlagenen Test eines Videos ist in Abbildung \ref{fig:analysator-failed-test} zu sehen.
Im Anschluss an den Vergleich fasst eine Konsolenausgabe zusammen, ob das in der Datei enthaltene Video den Forderungen des RFC 2435 \cite{rfc2435} entspricht.
Damit ist auf einen Blick ersichtlich, ob die Tests bestanden wurden.


\section{Erstellung von Motion JPEG-Daten}

Für das Streaming von \gls{jpeg}-Videos über \gls{rtp} werden geeignete Dateien benötigt.
Da das \gls{mjpeg}-Format jedoch nicht sehr üblich ist, müssen die besagten Dateien erst erstellt werden.
In den meisten Fällen ist die Konvertierung von einem anderen Videoformat die einfachste Lösung.
Viele Programme bieten eine solche Konvertierung an, doch nicht jedes dieser Programme ist geeignet.

Ein anderer Aspekt neben dem Datenformat ist das genutzte Dateiformat.
Wie bereits in Kapitel \ref{chap:mjpeg} beschrieben, können \gls{mjpeg}-Daten in verschiedenen Dateiformaten gespeichert werden.
Die kleinsten Dateien werden mit dem \texttt{mjpeg}-Dateiformat erreicht.
Allerdings befinden sich darin keine Metadaten, welche unter anderem auch für das korrekte Abspielen des Videos erforderlich sind.
Wird beispielsweise die fehlende Bildwiederholrate aus einem Standardwert abgeleitet, kann das ein etwas schnelleres oder etwas langsameres Abspielen zur Folge haben.
In Containerformaten wie \gls{avi} oder \gls{qtff} können dagegen Metadaten gespeichert werden.
Jedoch entstehen dadurch etwas größere Dateien.
Der in \ref{tab:filesizes-mjpeg} dargestellte Versuch ergab dabei, dass \gls{qtff}-Dateien unabhängig von der Länge des Videos nur wenige Kilobyte größer sind als \gls{mjpeg}-Dateien.
\gls{avi}-Dateien benötigen am meisten Speicherplatz.
Dabei hängt der für \gls{avi}"=Dateien zusätzlich benötigte Speicherplatz mit der Länge des Videos zusammen.
Je mehr Bilder im Video enthalten sind, desto mehr zusätzlicher Speicherplatz wird benötigt.
Zusammen mit einem grundsätzlichen Inkrement im Bereich von wenigen Kilobyte ist in \gls{avi}"=Dateien pro 1000 Bildern im Video ein Anstieg von etwa 20 Kilobyte in der Dateigröße zu erwarten.

\begin{table}
    \begin{tabularx}{\textwidth}{|X|X|X|}
        \hline
        & Video "`SdI"' & Video "`LNdW"' \\
        \hline
        Anzahl Bilder & 2817 & 1800 \\
        \hline
        Größe \texttt{mjpeg} [kB] & 24968 & 18072 \\
        Größe \texttt{avi} [kB] & 25036 & 18120 \\
        Größe \texttt{mov} [kB] & 24976 & 18080 \\
        \hline
    \end{tabularx}
    \caption[Übersicht der Dateigrößen zweier Videos für verschiedene Formate.]{
        Übersicht der Dateigrößen zweier Videos für verschiedene Formate, angegeben in Kilobyte.
        Bei den Videos handelt es sich um Bewegtbilder der \acrshort{htwdd} zum Studium der Informatik ("`SdI"') \cite{youtube-sdi17} und zur Langen Nacht der Wissenschaften 2019 ("`LNdW"') \cite{youtube-lndw19}.}
    \label{tab:filesizes-mjpeg}
\end{table}

Der VLC Media Player ist ein weit verbreitetes Abspielprogramm für Audio- und Videodateien.
Als zusätzliche Funktionalität implementiert er eine Möglichkeit zur Konvertierung von Dateien.
Unter den unterstützten Dateiformaten befindet sich neben den bereits genannten Containerformaten auch ein \gls{mjpeg}-Format.
Dieses wurde in Abschnitt \ref{sec:mjpeg-vlc} näher beleuchtet.
Auch die Skalierung des Videos ist möglich.

Um Videodateien mit der englischsprachigen grafischen Oberfläche des VLC Media Players zu konvertieren, ist die Option \texttt{Media -> Convert / Save ...} zu wählen.
Es öffnet sich ein neues Fenster, in welchem im Reiter \texttt{File} über den Button \texttt{Add} die entsprechende Videodatei ausgewählt werden kann.
Der Button \texttt{Convert / Save} am rechten unteren Rand des Fensters bestätigt die Auswahl und führt zu einem neuen Fenster, in welchem die Parameter für die Konvertierung festgelegt werden können.
Neben der Angabe des Pfades der Ausgabedatei muss darin das Konvertierungsprofil ausgewählt werden.
In diesem Profil werden Voreinstellungen für jeweils eine bestimmte Konvertierung gespeichert.
Da kein vorgefertigtes Profil für die Konvertierung zu \gls{mjpeg} besteht, muss ein neues angelegt werden.
Zur Erstellung des Profiles öffnet sich erneut ein Fenster, in welchem die einzelnen Parameter festgelegt werden können.
Im Reiter \texttt{Encapsulation} ist die Auswahl \texttt{MJPEG} zu treffen.
Damit wird das Ausgabeformat der Datei als \gls{mjpeg} definiert.
Der Reiter \texttt{Video codec} lässt Angaben zu den Codierungsparametern machen.
Für den Unterreiter \texttt{Encoding parameters} ist als Codec \texttt{M-JPEG} auszuwählen.
Die Dimensionen des Videos können im Unterreiter \texttt{Resolution} mit der Angabe von \texttt{Width} und \texttt{Height} festgelegt werden.
Der Button \texttt{Save} in der rechten unteren Ecke des Fensters speichert das erstellte Profil.
Sind sowohl Ausgabedateipfad als auch das gewünschte Profil ausgewählt, kann die Konvertierung über den Button \texttt{Start} gestartet werden.

Mithilfe des entwickelten Analyseprogrammes konnte jedoch festgestellt werden, dass die vom VLC Media Player konvertieren \gls{mjpeg}-Daten nicht den Anforderungen des RFC 2435 \cite{rfc2435} entsprechen.
Getestet wurde das mit der Version 3.0.9.2 des Programmes.
Einerseits werden für die Entropiecodierung nicht die geforderten Huffmantabellen verwendet.
Die vom VLC Media Player verwendeten Tabellen entsprechen nicht denen aus Annex K.3 des \gls{jpeg}-Standards \cite{iso10918-1}.
Andererseits tritt bei der Skalierung von Videodaten ein Effekt auf, der die Daten für die Übertragung per \gls{rtp} unbrauchbar macht.
Wird bei der Skalierung das ursprüngliche Seitenverhältnis nicht beibehalten, werden die Dimensionen der Pixel verändert.
Das Seitenverhältnis der Pixel entspricht dann nicht mehr \texttt{1:1}, sondern wird zur Erhaltung des ursprünglichen Seitenverhältnisses angepasst.
Damit ist der VLC Media Player als Konvertierungswerkzeug für diese Anforderungen nicht geeignet.
Erwähnt werden soll jedoch noch, dass das Entfernen der Audiospuren kein Problem darstellte.

Ein anderes Programm mit vielfältiger Funktionalität für Audio- und Videodaten ist ffmpeg.
Der Schwerpunkt dieses Programmes liegt in der Bearbeitung der erwähnten Multimediadaten.
Das Programm wurde in der Version 4.2.4 getestet.
Auch ffmpeg unterstützt ein \gls{mjpeg}-Format.
Dabei handelt es sich um die einfachste Form dieses Formates, die Konkatenation von \gls{jpeg}-Bildern.
Auch Containerformate wie \gls{avi} und \gls{qtff} werden unterstützt.
Die Skalierung von Videos ist mit dem Programm problemlos.
Wird bei dieser das Seitenverhältnis verändert, werden für das Resultat wieder quadratische Pixel berechnet.
Ohne weitere Angaben konvertiert ffmpeg das Video in das gewünschte Format.
Wird als Ausgabedatei jedoch ein Containerformat wie \gls{avi} oder \gls{qtff} angegeben, so wird auch die Audiospur übernommen.
Die Option \texttt{-an} sorgt deshalb dafür, dass alle Audiospuren des Videos entfernt werden.
Bei der Analyse der auf diese Weise konvertierten Dateien mit dem entwickelten Analyseprogramm wurde ein weiterer Aspekt festgestellt.
Auch ffmpeg nutzt standardmäßig nicht die geforderten Huffmantabellen.
Im Gegensatz zum VLC Media Player lässt sich das aber mit der Option \texttt{-huffman default} beheben.
Der vollständige Befehl zur Konvertierung von Videodaten lautet demnach:

\begin{lstlisting}[style=packet,language=]
ffmpeg -i <input> -s <width>x<height> -c:v mjpeg
       -huffman default -an <output>
\end{lstlisting}

Die daraus entstehende Videodatei enthält \gls{mjpeg} Daten mit den gewünschten Abmessungen, für deren Kompression die Huffman"=Tabellen aus Annex K.3 des \gls{jpeg}"=Standards verwendet wurden.
Handelt es sich bei dem Ausgabedateiformat um ein Containerformat, werden jegliche Audiospuren des Videos entfernt.
Damit ist die durch die Konvertierung erhaltene Datei vollständig kompatibel zur in RFC 2435 \cite{rfc2435} definierten Übertragung.

