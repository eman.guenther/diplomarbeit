\chapter{Das Protokoll SRTP}
\label{sec:enc-srtp}

Ein generisches Protokoll für die Verschlüsselung und Authentifikation von Medienpaketen ist das \gls{srtp}.
Definiert wird es im RFC 3711 \cite{rfc3711} als Erweiterung des "`RTP Audio/Video Profils"' aus RFC 3551 \cite{rfc3551}.
Als solche besitzt \gls{srtp} auch die eigene Profilbezeichnung "`RTP/SAVP"', welche unter anderem per \gls{sdp} kommuniziert wird.
Damit gibt die Serverantwort auf eine "`DESCRIBE"'"=Anfrage des Empfängers im \gls{rtsp} ein Indiz für die Verwendung dieses Protokolls.
Ein weiterer Effekt der Erweiterung des besagten Medienprofils ist, dass auch bereits definierte Datenformate unterstützt werden.
Beispielsweise bleibt der "`Payload-Type"' \texttt{26} weiter als \gls{jpeg} identifizierbar.
Neben der \gls{rtp}"=Erweiterung im RFC 3711 \cite{rfc3711} wird im selben Dokument auch eine äquivalente Erweiterung für das \gls{rtcp} vorgestellt.
Damit wird die Verschlüsselung der Kontrollnachrichten für eine \gls{rtp}"=Übertragung optional verfügbar, während die Authentifikation der besagten Nachrichten zwingend erforderlich wird.

Anwendung findet \gls{srtp} beispielsweise in der Technologie \gls{webrtc}.
Diese ist ein weitverbreitetes browserbasiertes Protokollstack für die echtzeitfähige Übertragung von Video-, Audio und anderen Daten.
Wie \citeauthor{zschorlich17} bei der Analyse verschiedener Video"=Streaming"=Verfahren feststellte, wird die angesprochene Technologie immer häufiger für interaktive Webanwendungen verwendet (\acrshort{vgl} \cite{zschorlich17}, \acrshort{seite} 49).
Beispiele dafür sind die Verwendung von \gls{webrtc} in Videokonferenz"=Anwendungen wie "`BigBlueButton"' (vgl. \cite{bigbluebutton21}) und "`Jitsi Meet"' (vgl. \cite{jitsimeet21}).

\gls{srtp} wird zwischen der Transportschicht und der \gls{rtp} nutzenden Anwendung eingesetzt.
Alle \gls{rtp}"=Pakete werden in ein äquivalentes \gls{srtp}"=Paket umgeformt und anschließend transportiert (\acrshort{vgl} \cite{rfc3711}, \acrshort{seite} 5).
Das Verhalten auf der Empfängerseite ist dementsprechend umgekehrt.
\gls{srtp} entspricht dabei einer auf Paketebene agierenden Transportverschlüsselung und Authentifikation.
Der Schutz der Mediendaten erfolgt nur indirekt während der Übertragung.
Nach derselben sind die in den Paketen enthaltenen Daten nicht weiter geschützt.
Dieses Verhalten bedingt jedoch die Interoperabilität mit anderen Erweiterungen für \gls{rtp}, beispielsweise \gls{fec}"=Paketen für den Fehlerschutz oder \gls{jpeg}"=Pakten.
\gls{srtp} behandelt diese unabhängig von ihrem eigentlichen Inhalt und generiert ein entsprechendes verschlüsseltes Paket, welches übertragen wird.
Der darüberliegenden Anwendung ist es somit möglich, mit speziellen Pakettypen zu arbeiten, ohne auf eine sichere Übertragung verzichten zu müssen.

Der Kern von \gls{srtp} besteht aus der Bereitstellung eines kryptografischen Kontexts, welcher verschiedene Funktionen für die Sicherung von \gls{rtp}"=Paketen bereitstellt.
Dieser enthält unter anderem Informationen wie den Verschlüsselungsverfahren mit den dazugehörigen Parametern, den Algorithmus für die Authentifikation sowie Schlüssel für die verwendeten Verfahren.
Mithilfe des genannten Kontextes lassen sich Pakete verschlüsseln und authentifizieren.
Der organisatorische Aufwand der Schlüsselverwaltung kann dabei durch die Verwendung von "`Master"=Key"' und "`Master"=Salt"' verringert werden.
Der "`Master"=Key" dient dabei als Hauptschlüssel, aus welchem alle Sitzungsschlüssel abgeleitet werden.
Die Sicherheit wird zusätzlich noch vom "`Master"=Salt"' erhöht, welches in jede dieser Ableitungen einfließt und somit bestimmte Angriffe gegen das Verfahren erschwert.
Ein Beispiel dieser Angriffe ist die Offline"=Berechnung von Sitzungsschlüsseln und Paketdaten für bestimmte "`Master"=Keys"'.
Abgefangene verschlüsselte Pakete könnten so mit den im Voraus berechneten Daten verglichen werden, woraus sich der verwendete Hauptschlüssel ableiten und somit die gesamte Kommunikation mitlesen lassen würde.
Das für jede Sitzung neu erzeugte "`Master"=Salt"' verhindert solche Angriffe, da die Menge an möglichen Kombinationen, die im Voraus berechnet werden müssten, sich so um mehrere Potenzen erhöht.

Mit diesem Kernkonzept wird außerdem deutlich, dass \gls{srtp} nicht die Aufgabe der Aushandlung von Sitzungsparametern wie Verschlüsselungsverfahren oder Schlüsseln übernimmt.
Diese Aushandlung muss bei der Sitzungsinitialisierung von anderen Protokollen durchgeführt werden.
Beispiele dafür sind "`Multimedia Internet KEYing"' (RFC 3830) oder die Erweiterungen für \gls{sdp} und \gls{rtsp} im RFC 4567 \acrshort{bzw} ausschließlich für \gls{sdp} in RFC 4568.

Nachfolgend soll zunächst auf den Aufbau von \gls{srtp}"=Paketen eingegangen werden.
Im Anschluss daran werden die drei Hauptbestandteile von \gls{srtp} erläutert.
Dabei handelt es sich um die Verschlüsselung, die Authentifikation und Integritätssicherung von Nachrichten sowie Algorithmen zur Ableitung von Unterschlüsseln aus dem "`Master"=Key"'.
Für jeden dieser Funktionsbestandteile definiert \gls{srtp} Standards, die in jedem Fall implementiert werden müssen.
Auf diese soll in den Ausführungen besonders eingegangen werden.
Außerdem besteht im Protokoll die Möglichkeit, weitere Verfahren aufzunehmen und zu nutzen.
Diese ist jedoch nicht Gegenstand der weiteren Betrachtungen.


\section{Aufbau von SRTP-Paketen}

Da das \gls{srtp} auf dem Protokoll \gls{rtp} aufbaut, besitzen die so erzeugten Pakete auch nahezu dasselbe Format.
Dieses in in Abbildung \ref{fig:srtp-packet-format} dargestellt.
Die Headerdaten der Pakete sind identisch zu denen in \gls{rtp}"=Paketen und bleiben unberührt.
Dazu gehören neben Informationen zur Protokollversion, zum Typ der enthaltenen Daten, zur Paketnummer und zum Zeitstempel unter anderem auch der Indentifikator für die Quelle der Pakete.

\begin{figure}[h]
    \centering
\begin{lstlisting}[style=packet,language=]
     0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<+
  |V=2|P|X|  CC   |M|     PT      |       sequence number         | |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ |
  |                           timestamp                           | |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ |
  |           synchronization source (SSRC) identifier            | |
  +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+ |
  |            contributing source (CSRC) identifiers             | |
  |                               ....                            | |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ |
  |                   RTP extension (OPTIONAL)                    | |
+>+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ |
| |                          payload  ...                         | |
| |                               +-------------------------------+ |
| |                               | RTP padding   | RTP pad count | |
+>+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<+
| ~                     SRTP MKI (OPTIONAL)                       ~ |
| +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ |
| :                 authentication tag (RECOMMENDED)              : |
| +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ |
|                                                                   |
+- Encrypted Portion                       Authenticated Portion ---+
\end{lstlisting}
    \caption[Format von \acrshort{srtp}"=Paketen.]{
        Format von \acrshort{srtp}"=Paketen.
        (aus \cite{rfc3711}, \acrshort{seite} 6)}
    \label{fig:srtp-packet-format}
\end{figure}

An den \gls{rtp}"=Header schließen sich die Paketdaten an.
Diese unterliegen vollständig der im Vorfeld definierten Verschlüsselungsoperation.
Besonderer Fokus liegt dabei auf der Datenmenge.
Alle im \gls{srtp} definierten Verschlüsselungen sind so entworfen, dass die Längen der verschlüsselten Daten und der Daten im Klartext identisch sind.
Damit ist die Verschlüsselungsoperation auch im Sinne der Paketgröße transparent.

Vergrößert wird ein durch \gls{srtp} verarbeitetes \gls{rtp}"=Paket ausschließlich durch das Anhängen des Identifikators für den "`Master"=Key"' oder die Authentifikationsdaten.
Die Übermittlung des Erstgenannten in jedem Paket ist optional.
Er dient entweder zur Erneuerung der Schlüssel in einer laufenden Sitzung oder zur Identifikation eines "`Master"=Keys"', sollten mehrere zur Auswahl stehen.
Existiert nur einer dieser Hauptschlüssel und wird dieser zu Beginn der Sitzung ausgehandelt, ist die Übermittlung dieses Identifikators nicht nötig.
Anders steht es mit den Authentifikationsdaten.
Diese werden mithilfe eines erzeugten Sitzungsschlüssels für das gesamte Paket, mit Ausnahme der angehängt Daten, ermittelt.
Die Verwendung einer solchen Authentifikation wird vom \gls{srtp}"=Protokoll empfohlen.
Damit kann ein empfangenes Paket verifiziert und Replay"=Angriffen vorgebeugt werden.
Als Replay"=Angriff ist das Aufzeichnen und später wiederholte Abspielen von Paketen zu verstehen.
Diese Art von Angriffen zielt hauptsächlich darauf ab, dass ein Dritter die Identität des Senders einnimmt.
Auf diese Weise entstehen verschiedene Angriffsszenarien, wozu auch das Offenlegen von Sitzungsschlüsseln gegenüber Dritten gehört.
Replay"=Angriffe bei \gls{srtp}"=Übertragungen zielen im Allgemeinen weniger auf das Kompromittieren von Schlüsseln ab, sondern eher auf das wiederholte Senden bestimmter Daten.
Ein Beispiel dafür ist das Mitschneiden der Aufnahmen von Überwachungskameras, welche zu einem späteren Zeitpunkt wieder übertragen werden (\acrshort{vgl} \cite{rfc3711}, \acrshort{seite} 41 \acrshort{folgend}).
Für den Zeitraum der Übertragung dieser aufgezeichneten Daten ist die Überwachung des gewünschten Objektes ausgesetzt.


\section{Verschlüsselung im SRTP}
\label{sec:enc-srtp-encryption}

Die vom \gls{srtp} vorgegebenen und zu implementierenden Verschlüsselungsverfahren sind \gls{aes-cm} und \gls{null}.
Dabei bezeichnet die \gls{null}"=Chiffre die Verarbeitung eines Paketes ohne die Daten desselben zu verschlüsseln.
Sie kann vor allem zu Testzwecken eingesetzt werden.
Die Chiffre \gls{aes-cm} steht für den Counter"=Modus des \gls{aes} mit einem Schlüssel von 128 Bit.
Bei der Verwendung dieses Verfahrens müssen sowohl der Modus als auch das Verfahren selbst von einem anderen Protokoll ausgehandelt werden.
Der Grund dafür ist, dass die Aushandlung außerhalb des Rahmens von \gls{srtp} ist und das besagte Protokoll deshalb nicht die erforderliche Funktionalität implementiert.

Der \gls{aes} ist "`die heutzutage am meisten genutzte symmetrische Chiffre überhaupt"' (\cite{paar16}, \acrshort{seite} 103) und gilt zur Zeit als sicher.
Zu den bekanntesten Standards, die \gls{aes} verwenden, gehören unter anderem das Protokoll \gls{tls} und das Secure"=Shell"=Protokoll SSH (\acrshort{vgl} \cite{paar16}, \acrshort{seite} 103).
Als symmetrisches Verfahren ist diese Chiffre, gemessen an asymmetrischen Verfahren, vergleichsweise schnell.
Der Counter"=Modus bezeichnet einen Betriebsmodus für Blockchiffren, bei welchem für die Verschlüsselung jedes Blockes ein Initialisierungsvektor und ein Zählerwert genutzt werden (\acrshort{vgl} \cite{paar16}, \acrshort{seite} 154).
Für die Nutzung im \gls{srtp} ist der Initialisierungsvektor \texttt{IV} wie folgt definiert:

\begin{center}
    \texttt{IV = (k\_s * 2\string^16) XOR (SSRC * 2\string^64) XOR (index * 2\string^16)}
\end{center}

Dabei bezeichnet \texttt{k\_s} den für das "`Salting"' erzeugte Sitzungsschlüssel.
Dieser sorgt dafür, dass keine Kombinationen für Initialisierungsvektoren und verschlüsselte Daten vorberechnet werden können.
Mit \texttt{SSRC} und \texttt{index} gehen außerdem die Identifikation des Senders und der Nummer des aktuellen Paketes mit in die Berechnung ein.
Für den Zähler sind die 16 \glspl{lsb} des Initialisierungsvektors vorbehalten.
Der Wert des Zählers wird bei der Verschlüsselung für jeden Klartextblock inkrementiert.
Insbesondere der Zähler verhindert, dass gleiche Eingangsdaten auf dieselben verschlüsselten Daten abgebildet werden.
Das Erkennen von Inhalten in den verschlüsselten Daten wie beim \gls{ecb}"=Modus kann auf diese Weise verhindert werden.

Weitere Vorteile des Counter"=Modus sind, dass er gut parallelisierbar ist und als Stromchiffre eingesetzt werden kann.
Die Nutzung als Stromchiffre kommt besonders dann zur Geltung, wenn ein Klartextblock kleiner ist als die Blockgröße des Verschlüsselungsverfahrens.
Bei einer reinen Blockchiffre müsste der Klartextblock durch \gls{padding} auf die Blockgröße des Verfahrens erweitert werden.
Für die Verschlüsselung mit einer Stromchiffre ist diese Erweiterung nicht erforderlich.
Das resultiert in einem Chiffrat mit derselben Größe wie der Klartext, was besonders bei der Übertragung von Daten in Netzwerken von Vorteil ist.
Das Chiffrat einer Blockchiffre wäre in einem solchen Fall größer als der ursprüngliche Klartext.


\section{Der Authentifikationsmechanismus des SRTP}

Neben der Verschlüsselung ist \gls{srtp} auch in der Lage, Pakete zu authentifizieren.
Die Authentifikation erfolgt standardmäßig mit dem Verfahren "`HMAC-SHA1"', welches in RFC 2104 \cite{rfc2104} definiert wird.
Dieses Verfahren muss außerdem durch jede Implementierung des \gls{srtp} unterstützt werden.
Bei "`HMAC-SHA1"' handelt es sich um einen \gls{mac}, welcher auf dem Hashverfahren SHA1 basiert.
Das allgemeine Funktionsprinzip von \glspl{mac} ist, dass aus einem Schlüssel und den zu authentifizierenden Daten eine Prüfsumme berechnet wird.
Diese Prüfsumme wird zusammen mit den Daten übertragen.
Der Empfänger kann anschließend dieselben Berechnungen wie der Sender durchführen.
Stimmt die vom Empfänger berechnete Prüfsumme mit der übertragenen überein, ist das Datenpaket authentifiziert.

Die Sicherheit des \gls{mac}"=Verfahrens basiert vor allem auf der Geheimhaltung des Schlüssels sowie einem sicheren Berechnungsalgorithmus.
Ist der Schlüssel nur dem Sender und dem Empfänger bekannt, kann ein authentifiziertes Paket nur vom Sender stammen.
Dasselbe gilt für Pakete, die in umgekehrter Richtung versendet werden.
Der Berechnungsalgorithmus für die Prüfsumme ist nur dann sicher, wenn dieselbe Prüfsumme nicht aus einer anderen Schlüssel"=Daten"=Kombination erzeugt werden kann.
Hashalgorithmen bringen solche Eigenschaften mit und werden deshalb häufig für \glspl{mac} genutzt, woraus die Bezeichnung "`HMAC"' entsteht.
Zur Zeit gilt unter anderem SHA1 als sicheres Hashverfahren.
Mit diesen Voraussetzungen definiert RFC 2104 \cite{rfc2104} die Berechnung der Prüfsumme \texttt{c} für das "`HMAC-SHA1"'"=Verfahren wie folgt:

\begin{center}
    \texttt{c = SHA1(key XOR opad, SHA1(key XOR ipad, packet))}
\end{center}

Zunächst wird der Schlüssel mit einem ersten Datenfeld \texttt{ipad} per XOR verknüpft und mit den Daten des Paketes konkateniert.
Das Datenfeld \texttt{ipad} besteht dabei aus einer Aneinanderreihung des Bytes \texttt{0x36} von der Länge der Blockgröße des Hashverfahrens.
Anschließend wird eine erste Prüfsumme aus den konkatenierten Daten berechnet.
Diese wird an die Verknüpfung des Schlüssels mit einem zweiten Datenfeld \texttt{opad} angehängt.
Das zweite Datenfeld \texttt{opad} besitzt dieselbe Länge wie \texttt{ipad}, besteht aber aus der Aneinanderreihung des Bytes \texttt{0x5C}.
Aus den so gewonnenen Daten wird abschließend die Prüfsumme berechnet, die als \gls{mac} verwendet wird.

Neben der Authentizität eines Pakets wird durch \glspl{mac} auch die Integrität desselben gesichert.
Würde der Inhalt eines Paketes bei der Übertragung verändert werden, könnte der Empfänger die Prüfsumme des Paketes nicht selbst berechnen.
Anhand der mangelnden Übereinstimmung der übertragenen und der berechneten Prüfsumme würde er feststellen, dass der Inhalt des Paketes verändert wurde.


\section{Die Ableitung von Schlüsseln aus dem "`Master-Key"'}
\label{sec:srtp-key-deriv}

Das \gls{srtp} ermöglicht die Nutzung eines "`Master-Keys"', sodass nur dieser in der Sitzungsinitialisierung ausgetauscht werden muss.
Dieser Hauptschlüssel dient ausschließlich zur Berechnung weiterer Schlüssel, welche in den verschiedenen Funktionsbestandteilen zum Einsatz kommen.
Auf diese Weise werden die für die Verschlüsselung, für die Authentifikation und für das "`Salting"' benötigten Schlüssel abgeleitet.
Bei der Verwendung eines "`Master-Keys"' muss diese Ableitung mindestens einmal zur Initialisierung aller Schlüssel stattfinden.
Weitere Neuberechnungen der Schlüssel sind von der im kryptografischen Kontext gesetzten \texttt{key\_derivation\_rate} abhängig (\acrshort{vgl} \cite{rfc3711}, \acrshort{seite} 26).

Auch für die Ableitung von Schlüsseln definiert \gls{srtp} ein Standardverfahren.
Dieses basiert ebenso wie die Verschlüsselung auf dem Counter"=Modus des \gls{aes}.
Das hat den Vorteil, dass nur wenige kryptografische Algorithmen implementiert werden müssen.
Außerdem werden die Schlüssel standardmäßig nur einmal pro Sitzung berechnet (\acrshort{vgl} \cite{rfc3711}, \acrshort{seite} 27), was den Berechnungsaufwand derselben minimiert.
Weiterhin wird für jede Operation ein Label in der Größe eines Bytes vergeben, zu sehen in Tabelle \ref{tab:srtp-keys}.
So kann sichergestellt werden, dass sich die erzeugten Schlüssel für Verschlüsselung und Authentifikation unterscheiden.
Um die Sicherheit der Schlüssel weiter zu erhöhen, kann das "`Master"=Salt"' verwendet werden.
Damit werden die bereits genannten Angriffe der Offline"=Berechnung von Schlüssel"=Daten"=Paaren erschwert.
Der RFC 3711 legt fest, dass das "`Master"=Salt"' zufällig sein muss (\acrshort{vgl} \cite{rfc3711}, \acrshort{seite} 9).

\vspace{2em}
Aus "`Master-Key"', Label und "`Master"=Salt"' können anschließend die Schlüssel berechnet werden.
Das geschieht mit der folgenden Vorschrift:

\begin{center}
    \texttt{key = AES-CM(master-key, ((label << 48) XOR master\_salt) << 16)}
\end{center}

Je nach benötigter Schlüssellänge wird der berechnete \texttt{key} nach der entsprechenden Menge an Bytes abgeschnitten, um den zu verwendenden Schlüssel zu erhalten.
Die Standardwerte die benötigten Variablen sind in Tabelle \ref{tab:srtp-keys} zu finden.

\begin{table}[h]
\centering
\begin{tabularx}{\textwidth}{|X|c|c|}
    \hline
    \textbf{Verwendung Schlüssel} & \textbf{Label} & \textbf{Benötigte Schlüssellänge [Bit]} \\
    \hline
    Verschlüsselung & \texttt{0x00} & 128 \\
    \hline
    Authentifikation & \texttt{0x01} & 160 \\
    \hline
    "`Salting"' & \texttt{0x02} & 112 \\
    \hline
\end{tabularx}
    \caption{Standardwerte für die Ableitung von Schlüsseln}
    \label{tab:srtp-keys}
\end{table}


\section{Implementierung des SRTP-Protokolls zur Verschlüsselung}

Mit der in Abschnitt \ref{sec:refactoring} besprochenen Erstellung der Klasse \texttt{RtpHandler} und der damit verbundenen Restrukturierung ist es möglich, das \gls{srtp}"=Protokoll auf einfache Weise zu implementieren.
Jegliche zu dem Protokoll gehörende Funktionalität wird in der neuen Klasse \texttt{SrtpHandler} implementiert.
Gleichzeitig stellt die Klasse Methoden als Schnittstelle für den \texttt{RtpHandler} bereit, welche die Verarbeitung von \gls{srtp}"=Paketen ermöglichen.
Diese werden in Abbildung \ref{fig:enc-srtp-classes} gezeigt.
Da der Fokus dieser Arbeit allerdings nur auf der Verschlüsselung von Datenpaketen liegt, wird auf die Implementierung der Authentifikation im Rahmen des \gls{srtp} verzichtet.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/srtp-classes.png}
    \caption[Vereinfachte Klassenstruktur der Einbindung des \texttt{SrtpHandlers}.]{
        Vereinfachte Klassenstruktur der Einbindung des \texttt{SrtpHandlers} in das Projekt.
        Dargestellt sind ebenfalls die Methoden, welche die Schnittstellen zum \texttt{RtpHandler} bilden.}
    \label{fig:enc-srtp-classes}
\end{figure}

Wie bereits ausgeführt, umfasst das Protokoll \gls{srtp} ausschließlich die Verschlüsselung und Authentifikation von Paketen.
Der Schlüsselaustausch zum Beginn einer Übertragung wird deshalb nicht implementiert.
Stattdessen wird ein "`Master-Key"' eingebaut, der als bereits vor der Sitzung zwischen Server und Client ausgetauscht gilt.
Dieselbe Begründung trifft auch auf die Bereitstellung der Verschlüsselung in den grafischen Oberflächen zu.
Um die Implementierung der Absprache der zu verwendenden Algorithmen zu umgehen und damit den Fokus auf \gls{srtp} zu belassen, sollen die Nutzer dieselbe simulieren.
Die Realisierung dieser Simulation geschieht in Form von Radiobuttons, bei welchen jeweils nur eine der möglichen Optionen gewählt sein kann.
Ein weiterer Grund für die Gestaltungsentscheidung der Verschlüsselungsoptionen ist die bessere Möglichkeit für das Testen verschiedener Szenarien.
Beispielsweise kann damit das Verhalten eines Client simuliert werden, welcher das Protokoll \gls{srtp} nicht implementiert, aber einen solchen Datenstrom erhält.

Der einprogrammierte "`Master"=Key"' ist für Nutzer nicht editierbar.
Zwar könnte diese Funktion implementiert werden, sie dient aber ausschließlich der Simulation des Schlüsselaustausches.
Die Unterstützung desselben ist jedoch nicht das Ziel der Implementierung.
So ist die Bereitstellung einer Möglichkeit zur Änderung des Schlüssels nicht erforderlich, da diese im Falle der Realisierung des Schlüsselaustausches obsolet würde.

Die Implementierung der Klasse \texttt{SrtpHandler} lässt sich in verschiedene Phasen teilen.
Diese entsprechen jeweils der Realisierung der \gls{aes}"=Verschlüsselung von Daten, der Ableitung von Sitzungsschlüsseln aus dem "`Master-Key"' und der Verarbeitung von \gls{rtp}"=Paketen.
Dabei wurde auf die Java"=Erweiterung \texttt{javax.crypto} zurückgegriffen, die unter anderem auch das Verschlüsselungsverfahren \gls{aes} bereitstellt.
Demnach hat der \texttt{SrtpHandler} bei der Verschlüsselung von Daten lediglich die benötigten Parameter bereitzustellen und vorzubereiten sowie das besagte \gls{aes}"=Verfahren anzuwenden.
Die Bereitstellung beinhaltet vor allem die Konvertierung der Parameter in einen gemeinsamen Datentyp.
Da die Verarbeitung von Daten, wie hier die Verschlüsselung, meist bytebasiert ist, wurden für die Repräsentation der Angaben Bytearrays genutzt.
Ein weiterer Schritt in der Vorbereitung der Verschlüsselung ist die Definition des Initialisierungsvektors.
Dieser besteht aus der XOR"=Verknüpfung des "`Salting Keys"', der \texttt{SSRC}"=Kennzeichnung der Paketquelle sowie dem Paketindex.
Der "`Salting Key"' ist dabei ein aus dem "`Master Key"' abgeleiteter Sitzungsschlüssel, welcher unter anderem von der Ableitungsrate der Schlüssel und dem Paketindex abhängig ist.
Ist die Ableitungsrate gleich "`0"', wird dieser Schlüssel nur einmal zu Beginn der Sitzung berechnet.
Ansonsten gilt er ausschließlich für eine begrenzte Anzahl an Paketen.
Besagter Schlüssel dient zum Schutz vor semantischen Angriffen wie beispielsweise bei dem im Abschnitt \ref{sec:enc-requirements} angesprochenen ECB"=Modus.
Die \texttt{SSRC}"=Kennzeichnung bei der Berechnung des Initialisierungsvektors ist ein im \gls{rtp}"=Protokoll definierter Wert zur Identifikation verschiedener Quellen für Videoströme.
Eine Besonderheit des \gls{aes}"=Verfahrens ist, dass Verschlüsselung und Entschlüsselung denselben Mechanismus verwenden.
Somit ist die Verschlüsselung eines Ciphertextes gleich der Entschlüsselung desselben.
Jedoch bietet die Klasse \texttt{Cipher} des Paketes \texttt{javax.crypto} eine generische Schnittstelle für verschiedene Verschlüsselungsverfahren.
Deshalb muss bei der Initialisierung dieser Klasse auf die Angabe des korrekten Modus, Verschlüsselung oder Entschlüsselung, geachtet werden.
Abgesehen davon sind beide Operationen der Implementierung identisch.

\vspace{1em}
Der zuvor angesprochene "`Salting Key" muss, wie auch der entsprechende Schlüssel zur Verschlüsselung, mindestens einmal zu Beginn der Sitzung aus dem "`Master Key"' berechnet werden.
Dazu wird die Funktion zur Ableitung von Schlüsseln genutzt.
Diese basiert, wie bereits im Abschnitt \ref{sec:srtp-key-deriv} erwähnt, genauso wie die Verschlüsselung auf dem Counter"=Modus des \gls{aes}"=Verfahrens.
Als bislang sicheres Verschlüsselungsverfahren hat es die Eigenschaft, stark von den Eingabeparametern abhängig zu sein und als Ausgabe zufällig aussehende Daten zu generieren.
Diese Eigenschaft wird bei der Schlüsselableitung im \gls{srtp} genutzt.
Ebenso wie bei der Verschlüsselung der Daten besteht der größte Aufwand bei der Implementierung dieser Funktionalität in der Vorbereitung der benötigten Parameter.
Besonders sind dabei die Eingangsdaten, welche verschlüsselt werden.
Dabei handelt es sich um ein Datenfeld aus Null"=Bytes, welches mindestens so lang wie die gewünschte Schlüssellänge sein muss.
Der Grund für die Verwendung von Null"=Bytes liegt in der Funktionsweise des Counter"=Modus.
Bei diesem wird aus dem Initialisierungsvektor und dem Schlüssel ein verschlüsselter Datenblock berechnet.
Dieser wird anschließend per XOR"=Operation mit einem zu verschlüsselnden Datenblock verknüpft.
Auf diese Weise wird nicht der Klartext"=Datenblock selbst im Verschlüsselungsverfahren verarbeitet.
Die XOR"=Verknüpfung sorgt jedoch effektiv für eine Verschlüsselung desselben.
Für die Schlüsselgenerierung im Rahmen von \gls{srtp} ist ausschließlich der vom Verfahren erzeugte verschlüsselte Datenblock ohne die Verknüpfung mit den zu schützenden Daten von Interesse.
Deshalb werden Null"=Bytes verwendet.
Bei der XOR"=Verknüpfung von Daten mit Null"=Daten werden die Daten nicht verändert.
So ist es möglich, den gewünschten Schlüsselstrom aus der generischen Verschlüsselungsfunktion der Klasse \texttt{Cipher} zu generieren.

Für die Überprüfung der Schlüsselableitung stellt der RFC 3711 \cite{rfc3711} in Anhang B.3 Testdaten bereit.
Es wird dabei von festen Werten für den "`Master Key"' und das "`Master Salt"' ausgegangen.
Anschließend werden für einen gegebenen Paketindex die Schlüssel für Verschlüsselung, Authentifikation und "`Salting"' angegeben.
Auf diese Weise kann überprüft werden, ob die Implementierung wie vorgesehen arbeitet.
Deshalb implementiert die Klasse \texttt{SrtpHandler} eine Testfunktion für die Schlüsselableitung.
In dieser werden alle benötigten Werte manuell gesetzt und anschließend die Schlüssel berechnet.
Die Ausführung dieser Methode ergab, dass die erarbeitete Implementierung korrekt funktioniert.

Um die bereits implementierten Verfahren in das Projekt "`RTSP-Streaming"' integrieren zu können, müssen auch Methoden zur Verarbeitung von \gls{rtp}"=Paketen bereitgestellt werden.
Diese dienen zeitgleich als Schnittstelle zur Klasse \texttt{SrtpHandler}.
Maßgeblich für den Entwurf dieser Methoden war auch deren Anwendung im \texttt{RtpHandler}.
Da das Protokoll \gls{srtp} zwischen Transportebene und Anwendung agiert, müssen ausgehende Pakete verschlüsselt und empfangene Pakete entschlüsselt werden.
Diese Operationen finden direkt vor dem Versenden \acrshort{bzw} direkt nach dem Empfang statt, sodass die weitere Verarbeitung der \gls{rtp}"=Pakete nicht beeinflusst wird.
Deshalb nimmt die Methode \texttt{transformToSrtp()} ein Objekt der Klasse \texttt{RTPpacket} entgegen und gibt ein Bytearray zurück, welches sofort versendet werden kann.
Ihre Funktionalität umfasst die Ableitung des Paketindexes, die Verschlüsselung des Paketinhaltes, die Authentifikation des Paketes und die Konkatenation des ursprünglichen \gls{rtp}"=Headers mit den verschlüsselten Paketdaten.
Dabei wird auf die zuvor implementierte Funktionalität zur Verschlüsselung der Daten mittels \gls{aes} zurückgegriffen.
Die Authentifikation von Paketen wurde bei der Entwicklung vorgesehen, jedoch nicht implementiert.
So besteht bereits bei der Konstruktion des \texttt{SrtpHandler}s die Möglichkeit, eine Authentifizierung zu aktivieren.
Die Implementierung dieser Funktionalität kann als Fortsetzung der vorliegenden Arbeit realisiert werden.

Die Entschlüsselung von Paketen wird mit der Methode \texttt{retrieveFromSrtp()} ermöglicht.
Sie nimmt die empfangenen Daten als Parameter entgegen und gibt das entschlüsselte \gls{rtp}"=Paket zurück.
Dabei gleicht sie in ihrem Ablauf und ihrer Funktionalität der Methode \texttt{transformToSrtp()}.
Notwendig ist dabei die umgekehrte Reihenfolge von Entschlüsselung und Authentifikation.
Da die Daten zur Authentifikation an die verschlüsselten Paketdaten angehängt wurden, müssen diese zunächst wieder entfernt und zugleich authentifiziert werden.
Erst danach kann die Entschlüsselung stattfinden.

Damit die Paketverarbeitung vor Client und Server gekapselt wird, stellt der \texttt{RtpHandler} Methoden als Schnittstellen bereit.
Demzufolge findet auch der Aufruf der Verarbeitung mit \gls{srtp} in dieser Klasse statt.
Zunächst muss sich der \texttt{RtpHandler} merken, welcher Art der Verschlüsselung gewünscht ist.
Neben der Option, keine Verschlüsselung anzuwenden, soll auch das Protokoll \gls{srtp} gewählt werden können.
Eine Verarbeitung hinsichtlich der Verschlüsselung erfolgt somit erst nach einer Überprüfung des Verschlüsselungsmechanismus.
Für die Bereitstellung von Paketen müssen die Methoden \texttt{jpegToRtpPacket()} und \texttt{createFecPacket()} auf die Verwendung der Verschlüsselung prüfen.
Soll mit \gls{srtp} verschlüsselt werden, wird die entsprechende Methode beim \texttt{SrtpHandler} aufgerufen.
Dabei muss der Inhalt des Paketes nicht berücksichtigt werden.

Auf der Seite des Clients ist gegebenenfalls die Entschlüsselung der empfangenen Pakete erforderlich.
Ebenso wie im Server wird dabei auf das gewählte Verschlüsselungsverfahren geprüft.
Die Verarbeitung erfolgt dabei ausschließlich beim Empfang von Paketen in der Methode \texttt{processRtpPacket()}.
Im Anschluss daran kann das Paket wie zuvor weiterverarbeitet werden.
Deshalb ist auch keine Berücksichtigung der Verschlüsselung in der Methode \texttt{nextPlaybackImage()} notwendig.
Das macht den Charakter der Verschlüsselung mit \gls{srtp} als Transportverschlüsselung deutlich.
Die Verarbeitung laut \gls{srtp}"=Protokoll erfolgt ausschließlich in direkter Nähe zur Übertragung, sodass alle anderen Verarbeitungsschritte keiner Veränderung bedürfen.


\section{Test der Verschlüsselung}

Die Funktionalität der \gls{srtp}"=Verschlüsselung kann bei der Ausführung des Projektes getestet werden, indem die Verschlüsselungsoption in den grafischen Oberflächen des Clients und des Servers auf \texttt{SRTP} gestellt wird.
Dadurch initialisiert der für die Paketverarbeitung zuständige \texttt{RtpHandler} den \texttt{SrtpHandler}.
In der Folge werden alle Pakete vor dem Versenden verschlüsselt \acrshort{bzw} direkt nach dem Empfang entschlüsselt.

Verglichen mit der Übertragung ohne Verschlüsselung lassen sich bei der Übertragung mit \gls{srtp}"=Paketen subjektiv keine Unterschiede feststellen.
Die Verarbeitung der Daten verläuft sowohl auf Senderseite als auch auf der Seite des Empfängers ohne Probleme.
Der Client kann die Bilder korrekt und flüssig anzeigen.
Insofern erfüllt die Verschlüsselung die Anforderung der Echtzeitfähigkeit.
Bei der Simulation von Paketverlusten zeigt das Projekt ebenfalls keine Veränderung des Verhaltens.
Niedrige Verlustraten werden per \gls{fec} ausgeglichen, während höhere Verluste nicht zu kompensieren sind.

Durch die Analyse der gesendeten Pakete mit dem Programm Wireshark lässt sich die erfolgreiche Verschlüsselung feststellen.
Beweis dafür sind unter anderem die im \gls{jpeg}"=Header enthaltenen Bilddimensionen, welche sich im Paketstrom von Bild zu Bild ändern.
Tatsächlich wird jedoch eine Video mit konstanten Bildmaßen übertragen.
Die sich ändernden Bilddimensionen sind dementsprechend ein Anzeichen für die erfolgte Verschlüsselung.

Die Betrachtung der gesendeten Pakete zeigt auch, dass diese nach wie vor als \gls{jpeg}"=Pakete erkannt werden.
Das liegt vor allem am \gls{rtp}"=Header, welcher nicht verschlüsselt wurde und demnach noch dasselbe Format besitzt.
Diese Tatsache zeigt, dass die übertragenen Pakete nach außen hin bei oberflächlicher Betrachtung als unverschlüsselt erscheinen.
Empfänger, welche die Verschlüsselung per \gls{srtp} nicht unterstützen oder nicht über die verschlüsselte Übertragung informiert sind, können demnach die Pakete nicht korrekt entschlüsseln.
Dieses Problem ist jedoch eher theoretischer Natur.
Die fehlerhafte Entschlüsselung wird im vorliegenden Projekt nur durch eine falsche Einstellung der Verschlüsselung in den Oberflächen von Server und Client ermöglicht.
Wird zusätzlich ein Protokoll für den Schlüsselaustausch und die Sitzungsinitialisierung implementiert, ist ein Fehlschlagen der Entschlüsselung nur bei einer fehlerhaften Initialisierung möglich.

Zum selben Ergebnis kommt auch der Versuch, die \gls{srtp}"=Verschlüsselung im Projekt nur einseitig zu aktivieren.
Dabei entschlüsselt der Client entweder unverschlüsselte Pakete oder er versucht, verschlüsselte Daten anzuzeigen.
Beide Fälle führen dazu, dass verschlüsselte und somit nicht \gls{jpeg}"=konforme Daten weiterverarbeitet werden.
Das führt zum Fehlschlagen der Decodierung des Bildes.
Die Erkenntnis daraus ist, dass sowohl Sender als auch Empfänger die Verschlüsselung unterstützen müssen.
Außerdem müssen alle benötigten Parameter, wie beispielsweise das Verschlüsselungsverfahren und der Schlüssel, im Vorfeld ausgetauscht und beiden Parteien bekannt sein.

Um die Implementierung der \gls{srtp}"=Verschlüsselung zu verifizieren, sollten auch Übertragungen mit dem VLC Media Player getestet werden.
Allerdings zeigte sich schon bei der Erprobung der \gls{srtp}"=Übertragung zwischen zwei Instanzen dieses Programmes, dass das Zusammenspiel von \gls{rtsp} und \gls{srtp} nicht unterstützt wird.
In einem weiteren Schritt wurden weitere Programme ermittelt, welche die erforderlichen Protokolle implementieren.
Dabei ergab sich, dass viele Programme zum Abspielen von Mediendateien und -Strömen die Bibliothek LIVE555 verwenden.
Diese implementiert die Funktionalität verschiedener Protokolle für die Echtzeitübertragung von Daten, wozu auch \gls{rtp} und \gls{rtsp} gehören.
Zur Funktionalität der Bibliothek gehört auch eine Implementierung des \gls{srtp}"=Protokolls.
Jedoch ist diese sehr eingeschränkt und unterstützt zur Zeit ausschließlich die Übertragung bestimmter Netzwerkkameras, welche über eine \gls{tls}"=Verbindung senden (\acrshort{vgl} \cite{live555-changelog}, Eintrag vom 11.02.2020).
Eine solche Verbindung ist im Projekt "`RTSP"=Streaming"' nicht implementiert.
Da keine anderen freien Programme das \gls{srtp}"=Protokoll implementieren, konnte die Implementierung im besagten Projekt nicht weiter getestet werden.

