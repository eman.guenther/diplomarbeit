\chapter{Aufbau von Motion JPEG-Daten}
\label{chap:mjpeg}

Im Vergleich zu einzelnen Bildern besitzen Videos neben Höhe und Breite noch die Dimension Zeit.
Für die Kompression von Videodaten kann deshalb neben der Ähnlichkeit von Bildausschnitten ein und desselben Bildes ausgenutzt werden, dass aufeinanderfolgende Bilder meist ebenfalls Ähnlichkeiten besitzen.
Das ist zum Beispiel bei einem sich bewegenden Objekt vor stillem Hintergrund oder einem Kameraschwenk der Fall.
Diese Ähnlichkeit wird zeitliche Redundanz genannt.
In modernen Videokompressionsverfahren wird sie ausgenutzt, um die Informationsmenge durch die Vorhersage von Bildinhalten zu reduzieren.
Dafür werden meist umfangreiche Berechnungen durchgeführt, was zu hohen Kompressionsraten führt.

Eine andere Gruppe an Videokompressionsverfahren nutzt die zeitliche Redundanz nicht aus.
Stattdessen komprimieren diese Verfahren jedes Bild eines Videos einzeln.
Eines dieser Verfahren ist \gls{mjpeg}, welches dem Namen nach Einzelbilder im \gls{jpeg}-Format komprimiert.
Im Vergleich zu den modernen Videokompressionsverfahren erreichen Verfahren wie \gls{mjpeg} keine hohen Kompressionsraten.
Dafür bieten sie andere Vorteile.
Durch die relativ geringe Verarbeitung entstehen kaum Kompressionsfehler.
Außerdem sind die Einzelbilder durch die voneinander unabhängige Kompression sehr leicht auszulesen und zu verarbeiten.
Beides ist wichtig für den Videoschnitt.
Durch die Unabhängigkeit der Bilder voneinander kann an beliebigen Positionen geschnitten werden, ohne dass aufgrund der Kompression ein Qualitätsverlust zu verzeichnen ist.
Auch für Echtzeitübertragungen ist diese Art von Videokompression geeignet.
Jedes Bild kann einzeln codiert werden, ohne dass es von vorigen oder folgenden Bildern abhängig ist.
Dass die dafür durchzuführenden Berechnungen deutlich weniger aufwändig sind als die für moderne Kompressionsverfahren, ist vor allem in Anwendungsfällen mit weniger leistungsfähiger Hardware von Vorteil.
Dazu gehören beispielsweise Überwachungskameras, die ihre Bilder in Echtzeit in ein lokales Netzwerk übertragen.

Eine Schwierigkeit beim Verarbeiten von \gls{mjpeg}-Daten besteht darin, dass kein einheitliches Format für die Speicherung dieser Daten definiert ist.
Die eigentlichen Videodaten werden zwar als Strom aufeinanderfolgender \gls{jpeg}-Bilder gespeichert, allerdings unterscheiden sich die Dateiformate in der Struktur und Definition ihrer Informationsblöcke.
Im einfachsten Fall enthält eine Datei mit der Endung \texttt{mjpeg} oder \texttt{mjpg} nur den Strom aufeinanderfolgender Bilder, ohne Angaben zu beispielsweise Bildwiederholrate, Abspieldauer oder Datenrate zu machen.
Andererseits existieren Containerformate wie \gls{qtff} und \gls{avi}, welche zwar die genannten Videodaten beinhalten, diese aber in eine interne Struktur einbetten und daneben noch Datenfelder für aus dem Video abgeleitete Informationen beschreiben.
Diese Formate können grundsätzlich von verschiedenen Kompressionsverfahren erzeugte Datenströme enthalten.
Deshalb stellt ihr Aufbau eine Strukturierung bereit, in der neben den Daten selbst auch Angaben wie die Art der Daten, das verwendete Kompressionsformat und die Beschreibung des Handlers zum Dekomprimieren der Daten Platz finden.
Mitunter implementieren die genannten Formate auch Felder, die mit dem Begriff "`Metadaten"' benannt sind.
Diese enthalten beispielsweise einen Urheberrechtshinweis oder den Autor der Datei.
Da sich auf ein Video beziehende Informationen wie die Bildwiederholrate oder die Abspieldauer ebenfalls als "`Metadaten"' bezeichnet werden können, erfährt dieser Begriff eine Bedeutungsüberladung.
Deshalb soll hier darauf hingewiesen werden, dass die Bezeichnung "`Metadaten"' im weiteren Verlauf auf die aus dem Video abgeleiteten Informationen und nicht auf dateibezogene Angaben bezieht.

Im Anschluss soll für drei verschiedene Formate erläutert werden, welche Möglichkeiten jeweils zur Speicherung von \gls{mjpeg}-Daten bestehen.
Bei den Formaten handelt es sich um das \gls{mjpeg}-Format des VLC Media Players, das \gls{qtff} und das \gls{avi}-Format, welche in dieser Reihenfolge betrachtet werden.
Für die beiden Containerformate \gls{qtff} und \gls{avi} werden jedoch nur die Grundstruktur der Formate sowie relevante Metadaten betrachtet.
Eine vollständige Beschreibung dieser beiden Dateiformate ist nicht Gegenstand der nachfolgenden Betrachtungen.


\section{Das MJPEG-Format des VLC Media Players}
\label{sec:mjpeg-vlc}

Der VLC Media Player \cite{vlc} ist ein Programm zum Abspielen von Dateien mit multimedialen Inhalten.
Dabei werden verschiedene Formate und Medien unterstützt, wie beispielsweise DVDs und Audio-CDs.
Auch einige Streaming-Protokolle sind implementiert.
Die Software wird von der gemeinnützigen Organisation VideoLAN frei und quelloffen entwickelt und läuft auf allen gängigen Plattformen.
Neben dem Abspielen von Mediendaten ist das Programm auch in der Lage, Dateien in verschiedene Formate zu konvertieren.
Dazu gehört auch die Konvertierung von Videodaten ins \gls{mjpeg}-Format.

Zur Speicherung von \gls{mjpeg}-Daten verwendet der VLC Media Player ein auf den \gls{mime} basierendes Format.
\gls{mime} ist eine standardisierte Form, auf den Inhalt und Aufbau von Daten und Dateien hinzuweisen.
Die entsprechenden Informationen befinden sich meist am Beginn der Daten oder Datenabschnitte.
Während die Grundstruktur des \gls{mime}-Formats in RFC 2045 \cite{rfc2045} standardisiert ist, gibt RFC 2046 \cite{rfc2046} Aufschluss über die möglichen Medientypen.
Zu letztgenannten gehört auch der "`multipart"'-Typ.
Er beschreibt, dass die nachfolgenden Daten aus mehreren Einheiten bestehen.
Beispielsweise können die aufeinanderfolgenden \gls{jpeg}-Bilder eines Videos einen solchen Typ bilden.
Die Trennung der Einheiten voneinander erfolgt mit sogenannten "`boundaries"'.
Diese bestehen aus zwei Bindestrichen, auf die eine Zeichenkette von maximal 70 Zeichen folgt.
Auf diese Weise wird die Trennung zweier Einheiten gekennzeichnet.
Dabei ist die verwendete Zeichenkette innerhalb der gesamten Daten identisch.
Danach folgen \gls{mime}-übliche Angaben zum Inhalt der nächsten Dateneinheit.
Beispiele dafür sind der Typ der Daten, der im Falle eines \gls{mjpeg}-Videos \texttt{image/jpeg} lautet, sowie deren Länge.
Im direkten Anschluss an diese Informationen beginnen die eigentlichen Daten der Einheit.
Auf diese Weise können mehrere \gls{mime}-Einheiten aufeinander folgen, jedoch immer durch die "`boundaries"' abgegrenzt.
Die "`multipart"'-Einheit muss an ihrem Ende noch einmal mit der Zeichenkette zur Trennung der Einheiten abgeschlossen werden.
Jedoch folgen auf die Kennzeichnung noch einmal zwei Bindestriche.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/vlc-mjpeg-format.png}
    \caption{Schematische Darstellung einer vom VLC Media Player konvertierten \acrshort{mjpeg}"=Datei.}
    \label{fig:mjpeg-vlc}
\end{figure}

Bei dem vom VLC Media Player verwendeten \gls{mime}-Format werden die Kennzeichnung als "`multipart"'-Typ sowie die abschließende "`boundary"' weggelassen.
So kann die Datenmenge etwas reduziert werden.
Jedoch ist das Format weiterhin als \gls{mime} erkennbar.
Ein Beispiel für eine vom VLC Media Player konvertierte \gls{mjpeg}"=Datei ist in Abbildung \ref{fig:mjpeg-vlc} dargestellt.
Die zusätzlichen Informationen dieses Datenformates enthalten außerdem keine Daten, die den im \gls{jpeg}-Format verwendeten Markern entsprechen.
Damit können vom VLC Media Player erzeugte \gls{mjpeg}-Dateien als aufeinanderfolgende \gls{jpeg}-Bilder gelesen werden.
Denn die \gls{mime}-Informationen zwischen den Bildern können ohne Weiteres übersprungen werden.


\section{Motion JPEG im QuickTime File Format}

Das \gls{qtff} ist ein von Apple entwickeltes proprietäres Containerformat.
Eine Dokumentation des Formates existiert auf den Webseiten der Apple Developer \cite{qtff16}.
Diese dient allerdings ausschließlich informationellen Zwecken und stellt keine für die Nutzung des Dateiformates erforderliche Lizenz bereit.

Aufgebaut ist das \gls{qtff} aus Datenblöcken, die \Glspl{atom} genannt werden.
Diese können ineinander verschachtelt werden und ergeben auf diese Weise eine QuickTime-Videodatei.
Neben einem Datenfeld für seine Größe enthält jedes \Gls{atom} ein aus vier ASCII"=Zeichen bestehendes Typfeld, mit welchem es identifiziert werden kann.
Im Anschluss an diese beiden Felder folgen die typabhängigen Daten.
Deren Länge und Aufbau unterscheidet sich je nach \Gls{atom}-Typ.

Eine QuickTime-Videodatei besteht immer aus mindestens zwei verschiedenen \Glspl{atom}.
Der Datenblock vom Typ "`File type"' wird mit den Zeichen \texttt{'ftyp'} gekennzeichnet.
Er enthält Informationen, welche das Dateiformat als \gls{qtff} identifizieren und die Unterscheidung zu anderen Dateiformaten ermöglicht.
Das kann vor allem bei Dateien nützlich sein, deren Dateinamensendung nicht mit den tatsächlichen Inhalt übereinstimmt.
Auf die Informationen zum Dateityp folgen die Videodaten im "`Movie data atom"'.
Es ist durch das Typfeld \texttt{'mdat'} identifizierbar.
Neben den Bewegtbilddaten sind in diesem Datenblock auch Audiodaten oder die Untertitel zu finden.

Optional auf der obersten Gliederungsebene der Datei ist das "`Movie atom"' mit der Kennzeichnung \texttt{'moov'}.
Ist es in einer Datei vorhanden, muss es sich zwischen den beiden genannten \Glspl{atom} befinden.
Dieser Datenblock speichert Metadaten zu den im "`Movie data atom"' enthaltenen Daten und deren Struktur.
Deshalb ist seine Definition ungeachtet seiner fakultativen Natur sinnvoll.
Weitere \Glspl{atom} sind auf der obersten Strukturebene im \gls{qtff} nicht erlaubt.
Alle konkreteren Informationen zum enthaltenen Video werden in speziellen \Glspl{atom} als Substrukturen der bisher vorgestellten Datenblöcke gespeichert.

Nachfolgend soll zunächst auf die Speicherung bestimmter Metadaten im \gls{qtff} eingegangen werden.
Im Anschluss daran wird eine Übersicht darüber gegeben, in welchen Formaten \gls{mjpeg}-Daten im \gls{qtff} gespeichert werden können.


\subsection{Metadaten im "`Movie atom"'}

Das "`Movie atom"' enthält eine ausgeprägte hierarchische Struktur, in der Metadaten gespeichert werden.
Für jede Hierarchieebene gibt es ein spezielles "`header atom"', in welchem allgemeine Informationen für die entsprechende Ebene vorgehalten werden.
Teilweise werden dabei Daten aus niedrigeren Ebenen akkumuliert.
Dazu gehören beispielsweise die Abspieldauer, der Zeitpunkt der letzten Modifikation und die verwendete Zeitskala.
Daraus ergibt sich, dass die Angaben höherer Hierarchieebenen nicht auf alle Unterstrukturen zutreffen.
Im Zweifelsfall sind deshalb niedrigere Ebenen als Informationsquelle vorzuziehen.

Der hierarchische Aufbau der Metadaten spiegelt die strukturelle Gliederung der audiovisuellen Inhalte in QuickTime-Videodateien wieder.
Diese ist in vereinfachter Form in Abbildung \ref{fig:qtff-moov} dargestellt.
Das in der Datei enthaltene Video ist in verschiedene Tracks unterteilt.
Ein Track enthält beispielsweise einen Video- oder einen Audiostream.
Demzufolge besteht ein Track aus einem Medium.
Für den Fall, dass es sich bei dem Medium um Bewegtbilder handelt, spezifiziert das \gls{qtff} dafür eine Unterstruktur:
Das "`Video media information atom"'.
Diese enthält wiederum eine Tabellenstruktur, aus der sich Informationen zum verwendeten Kompressionsformat und zur Bildwiederholrate ermitteln lassen.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.55\textwidth]{res/graph/qtff-moov.png}
    \caption[Die vereinfachte hierarchische Struktur des "`Media atoms"' im \acrshort{qtff}.]{
        Die vereinfachte hierarchische Struktur des "`Media atoms"' im \acrshort{qtff} für den Fall von einem Video-Track.
        Aufgeführt werden nur die hier benötigten \Glspl{atom}.
        (verändert nach \cite{qtff16}, Abschnitt "`Overview of Movie Atoms"')}
    \label{fig:qtff-moov}
\end{figure}


In dem hier betrachteten Fall des \gls{mjpeg}-Videos gibt es pro Video nur einen Track.
Dieser enthält außerdem nur das Medium Video und keinen Audiostream, da sich das \gls{mjpeg}-Format nur auf Bewegtbilddaten beschränkt.
Im "`Media header atom"' befinden sich unter anderem die Informationen zur verwendeten Zeitskala, die in Zeitschritten pro Sekunde gemessen wird.
Außerdem ist darin die Dauer des Medienstroms zu finden, die in Einheiten der Zeitskala angegeben wird.
Weiter unten in der Hierarchie finden sich Daten zu den Abtastwerten, die als \Glspl{atom} in der Tabellenstruktur gespeichert werden.
Dazu gehören die Beschreibung der Daten, welche unter anderem über das verwendete Kompressionsverfahren Aufschluss geben, und die Zuweisung von Zeitspannen zu einzelnen Videoframes.
Letztgenannte findet im "`Time-to-sample atom"' statt.
Dieses enthält eine Tabelle, in der für jedes Frame des Videostreams die Abspieldauer festgelegt wird.
Für konstante Bildwiederholungsraten können die Einträge zusammengefasst werden.
Der Gesamtanzahl der Bilder im Video steht dann die Zeit gegenüber, die jedes dieser Bilder beim Abspielen angezeigt wird.
Diese Dauer ist in Einheiten der Zeitskala angegeben, die im "Media header atom"' festgelegt wurde.
Die Division der Zeitschritte pro Sekunde durch die Anzeigedauer eines Bildes ergibt schließlich die Bildwiederholrate.


\subsection{MJPEG-Daten im "`Movie data atom"'}

Videodaten werden im \gls{qtff} im "`Movie data atom"' gespeichert.
Für das Auslesen und Verarbeiten dieser Daten sind die Metadaten notwendig, die im "`Movie atom"' angegeben werden.
Es gibt verschiedene Formate, in denen \gls{mjpeg}-Daten gespeichert werden können.
Als erstes ist die Speicherung der Daten im \gls{jpeg}-Format möglich.
Dabei werden die einzelnen Bilder nacheinander in die Datei geschrieben.
Das so entstehende Datenformat entspricht exakt dem Konzept von \gls{mjpeg}.
In den Metadaten wird dieses Format mit \texttt{'jpeg'} gekennzeichnet.

Die weiteren Möglichkeiten nutzen im \gls{qtff} definierte \acrlong{mjpeg}-Formate.
Motion JPEG Format A beschreibt \gls{jpeg}-Bilder, die einen eigens definierten APP1-Marker verwenden.
Dieser Marker enthält ausschließlich Informationen zu den Offsets bestimmter Felder und Informationen zur Größe des \gls{jpeg}-Bildes.
Damit beschreibt dieses Format valide \gls{jpeg}-Bilder, die der Definition im \gls{jpeg}-Standard entsprechen.
Das zweite Format mit der Benennung Motion-JPEG Format B wählt einen anderen Ansatz.
Anstatt das \gls{jpeg}-Format mit einem APP-Marker zu erweitern, kapselt es das Bild und entfernt dabei Informationen wie die \gls{m-soi}- und \gls{m-eoi}-Marker.
Das so entstandene Format ist also nicht valide im Sinne des \gls{jpeg}-Standards.
Aus der Datei extrahierte Bilder im Motion-JPEG Format B können also nicht von üblichen Bildbetrachtungsprogrammen decodiert werden.


\section{Motion JPEG im AVI Format}

Das \gls{avi}-Format wurde von der Microsoft Corporation auf der Basis des \gls{riff}-Formats entwickelt.
Es ist in der Entwicklerdokumentation für die Softwarebibliothek DirectShow \cite{ms-avi} beschrieben.
\gls{riff} ist ein Containerformat für Audio- und Videodaten und ist unter anderem auch Grundlage vom \gls{wav}.
Es stellt die Struktureinheiten \gls{chunk} und \gls{list} zur Verfügung.
Ein \gls{chunk} ist ein Datenblock, äquivalent zu den \Glspl{atom} beim \gls{qtff}.
Allerdings beginnt ein \gls{chunk} im Gegensatz zu den \Glspl{atom} mit dem Typen, auf den die Größe des Blockes und anschließend die typspezifischen Daten folgen.
Der Typ wird als \gls{fourcc} angegeben, der als Text interpretiert leicht für Menschen lesbar ist.
Hierarchische Strukturen werden mit \glspl{list} erzeugt.
Diese sind mit dem \gls{fourcc} \texttt{'LIST'} gekennzeichnet.
Im Anschluss an diese Zeichen kommen die Größe des Blocks, der Typ der \gls{list} und wiederum die spezifischen Daten.
Eine \gls{riff}-Datei besteht aus einer beliebigen Menge und Anordnung dieser beiden Strukturen, denen ein Header vorangestellt ist.
Dieser beginnt mit der \gls{fourcc}"=Kennzeichnung \texttt{'RIFF'}.
Auf diesen Code folgen die Dateigröße und der Dateityp.

Im Anschluss soll zunächst auf Grundstruktur von \gls{avi}-Dateien und die von dieser bereitgestellten Metadaten eingegangen werden.
Darauf folgen Erläuterungen zu den "`OpenDML AVI File Format Extensions"', in denen insbesondere die verschiedenen Speichermöglichkeiten von \gls{mjpeg}-Videos in \gls{avi}-Dateien Betrachtung finden.


\subsection{Die Grundstruktur des von Microsoft entwickelten AVI-Formates}

Die Spezifikation des \gls{avi}-Formates erweitert das \gls{riff}-Format insofern, als dass weitere Vorgaben bezüglich der Struktur aus \glspl{chunk} und \glspl{list} gemacht werden.
Diese sind in Abbildung \ref{fig:riff-avi-ext} visualisiert.
Die Erweiterung gibt vor, dass eine \gls{avi}-Datei jeweils eine \gls{list}-Struktur für Header und Daten enthalten muss.
In den Headerinformationen sind Metadaten zu den enthaltenen Streams gespeichert.
Die Videodaten befinden sich im mit \texttt{'movi'} gekennzeichneten Chunk.
Optional ist ein Index-\gls{chunk}, welcher Referenzen zur Position der Daten in der Datei enthält.
Diese können für den schnellen Zugriff auf bestimmte Bereiche genutzt werden.
Da die Struktur im Folgenden nicht weiter von Bedeutung ist, wird sie hier nicht näher erläutert.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{res/graph/riff-avi.png}
    \caption[Die auf dem \acrshort{riff}-Format basierende Struktur einer \acrshort{avi}-Datei.]{
        Die auf dem \acrshort{riff}-Format basierende Struktur einer \acrshort{avi}-Datei.
        Für diese Arbeit nicht relevante Inhalte wurden weggelassen.
        Dargestellt ist die Struktur für eine Datei mit einem Stream.}
    \label{fig:riff-avi-ext}
\end{figure}

Im \gls{avi}-Header stehen Angaben zur maximalen Datenrate des Videos, der Anzahl der in der Datei enthaltenen Streams sowie der Höhe und Breite des Videos.
Ähnlich wie beim \gls{qtff} gilt dabei auch:
Der Header fasst Informationen aus den einzelnen Streams zusammen.
Deshalb können die Angaben der Streams davon abweichen.
Werden genaue Werte benötigt, sind diese deshalb aus den jeweiligen Streamheaders auszulesen.

Auf diese Informationen folgt ein \gls{avi}-Stream-Header für jeden in der Datei enthaltenen Stream.
Im Fall eines \gls{mjpeg}-Videos ist beispielsweise nur ein einziger Stream vorhanden.
Der Stream-Header macht unter anderem die Angaben "`Scale"' und "`Rate"'.
Diese sind im Bezug auf die zeitliche Skala zu sehen.
Die Division von "`Scale"' durch "`Rate"' ergibt die Bildwiederholrate des Streams.
Diese Einheit wird auch verwendet, um aus der Angabe "`Length"' des Headers die Abspieldauer des Streams zu ermitteln.

Das "`Stream format"'-\gls{chunk} macht Angaben zum Datenformat der komprimierten Bilder.
Dazu zählen unter anderem die Höhe und Breite der Bilder.
Aber auch die verwendeten Bits pro Pixel, das genutzte Kompressionsformat und die horizontale und vertikale Auflösung der Bilder werden in diesem Datenblock festgehalten.


\subsection{Die OpenDML AVI File Format Extensions}

Das \gls{avi}-Dateiformat kann als Containerformat mehrere verschiedene Arten an Daten speichern.
Dennoch legt das Format bestimmte Einschränkungen fest.
Dazu gehört beispielsweise eine maximale Dateigröße von 4 Gigabyte.
Diese resultiert aus dem Datentyp, der für die Größe von \glspl{chunk} verwendet wird.
So können nur $ 2^{32} $ Byte adressiert werden.
Eine zweite Einschränkung ist die Struktur des optionalen Indexes.
Laut der \gls{avi}-Spezifikation befindet sich dieser am Ende einer Datei.
Zum einen bedeutet das eine große Verschiebung des Lese-Schreibkopfes der Datei am Anfang des Lesevorgangs, da der Index zum effizienten Auslesen der Daten benötigt wird.
Zum anderen muss die Position des Indexes verschoben werden, wenn die Daten des Streams erweitert werden.
Die wesentlichste Einschränkung besteht jedoch bei den unterstützten Datenformaten.
Verschiedene Videoformate und auch \gls{jpeg}-Bilder können gespeichert werden.
Jedoch gibt es kein einheitliches Format für \gls{mjpeg}-Daten.
Stattdessen werden solche Daten als Strom einzelner Bilder gespeichert.

Diese Restriktionen wurden in den "`OpenDML AVI File Format Extensions"' \cite{odml-avi} von einer eigens zu diesem Zweck ins Leben gerufene Arbeitsgruppe adressiert.
Diese Gruppe gehörte allerdings nicht zu Microsoft, sondern zum Unternehmen Matrox Electronic Systems Ltd.
Das führte zu einem als "`AVI 2.0"' benannten Format, welches erst später von Microsofts Softwarebibliotheken unterstützt wurde.
Üblich sind jedoch weiterhin beide \gls{avi}-Formate.

In der genannten Erweiterung des Dateiformates wird ein neuer \gls{chunk} vom Typ \texttt{'hdrx'} vorgestellt.
Dieser weist in einer \gls{riff}-Struktur darauf hin, dass sich in der Datei eine weitere Struktur desselben Typs befindet.
Auf diese Weise lässt sich eine \gls{avi}-Datei unabhängig vom Datentyp der \gls{chunk}-Größe beliebig erweitern.
Für die Indizierung wird vorgeschlagen, eine hierarchische Indexstruktur zu nutzen.
So können sich zwischen den Videodaten Index-\glspl{chunk} befinden.
Diese werden wiederum in einem "`Super"=Index"' erfasst, der sich wie der vorige Indextyp am Ende der Datei befindet.
Auf diese Weise kann die Datei kontinuierlich erweitert werden.
Außerdem wird ein schnellerer Zugriff auf die Daten ermöglicht.

Für die Speicherung von \gls{mjpeg}-Daten wird eine Variante des \gls{jpeg}-Formates vorgeschlagen.
In Abschnitt B.4 der \gls{jpeg}-Spezifikation \cite{iso10918-1} wird die Möglichkeit für ein verkürztes Datenformat gegeben.
Dazu werden Tabellendefinitionen aus den Daten entfernt und außerhalb der Bilder definiert.
Die "`OpenDML AVI File Format Extensions"' nutzt diese Variante und gibt vor, dass die zur Kodierung verwendeten Huffman-Tabellen außerhalb der \gls{jpeg}-Bilder definiert werden müssen.
Ansonsten werden die Bilder, wie bei \gls{mjpeg} üblich, aufeinanderfolgend als Datenstrom gespeichert.
% some hack, again
Ähnlich wie beim Motion JPEG Format B des \glspl{qtff} bedeutet das wiederum, dass keine selbstständigen \gls{jpeg}-Bilder aus dem Datenstrom extrahiert werden können.

