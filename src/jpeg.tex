\chapter{JPEG-Kompression und das JFIF-Datenformat}
\label{chap:jpeg-jfif}

Die Abkürzung \gls{jpeg} des vielverwendeten Kompressionsverfahrens steht für die \gls{jpeg-group}, welche das Verfahren entworfen hat.
Standardisiert ist die \gls{jpeg}-Kompression als ISO/IEC 10918-1 \cite{iso10918-1}.
In diesem Dokument werden die Anforderungen an Encoder und Decoder beschrieben, welche für die Erzeugung und das Verarbeiten von \gls{jpeg}-Daten erfüllt sein müssen.
Dazu gehören neben den für die Kompression zu verwendenden Algorithmen auch das in Annex B des Standards beschriebene Datenformat für die komprimierten Daten.
Auf dieses Format wird in Abschnitt \ref{sec:jfif} genauer eingegangen.
Vorgaben bei der Verwendung bestimmter Parameter für die Algorithmen gibt es nicht.
Allerdings empfiehlt der Standard bestimmte Werte und Tabellen, mit denen im Allgemeinen gute Ergebnisse erzielt werden.

Aufbauend auf dem im \gls{jpeg}-Standard definierten Datenformat hat sich das \gls{jfif}-Format entwickelt.
Dieses ist als ISO/IEC 10918-5 \cite{iso10918-5} standardisiert.
% use glspl to get Genitiv
Das Dokument erweitert das \gls{jpeg}-Format um einen Abschnitt, in welchem Daten zur Anzeigegröße des Bildes gespeichert werden und die Möglichkeit für das Speichern eines \glspl{thumbnail} gegeben wird.
Außerdem werden Einschränkungen bezüglich bestimmter Parameter der Kompression getroffen, zu denen unter anderem die Vorgabe eines zu verwendenden Farbmodells gehört.
Aufgrund dieser Anpassungen ist das \gls{jfif}-Format mittlerweile das übliche Format zur Speicherung und Übertragung von \gls{jpeg}-Kompressionsdaten.

Nachfolgend soll zunächst eine kurze Übersicht über das \gls{jpeg}-Kompressionsverfahren gegeben werden.
Dabei wird ausschließlich auf den Baseline-Modus der Kompression eingegangen, da nur dieser im weiteren Verlauf der Arbeit eine Rolle spielen wird.
Im Anschluss daran wird der Aufbau des \gls{jfif}-Dateiformates beschrieben.
Dieser wird später beim Auslesen und der Analyse der Kompressionsparameter benötigt.


\section{Das JPEG-Kompressionsverfahren im Baseline-Modus}
\label{sec:jpeg}

Im \gls{jpeg}-Standard werden vier verschiedene Kompressionsmodi beschrieben.
Neben dem Baseline-Modus gibt es den erweiterten, den verlustfreien und den hierarchischen Modus.
Jedoch muss jeder Encoder oder Decoder für \gls{jpeg}-Bilder den Baseline-Modus implementieren (\acrshort{vgl} \cite{strutz09}, \acrshort{seite} 204).
Auch deshalb ist dieser der am weitesten verbreitete der \gls{jpeg}-Modi.
Dass der Baseline"=Modus eine verlustbehaftete Kompression durchführt, ist dabei keine Einschränkung hinsichtlich seiner Verbreitung.
Im Gegensatz zu verlustfreien Verfahren wird dabei nur eine Annäherung an das Originalbild codiert.
So kann die zur Speicherung von Bildern benötigte Datenmenge deutlich reduziert werden.
Das komprimierte Bild ist dem Originalbild jedoch visuell sehr ähnlich, wodurch keine negativen Effekte für die Betrachtung enstehen.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/jpeg-encoder-schema.png}
    \caption[Der Ablauf einer \gls{jpeg}-Kompression im Baseline-Modus.]{
        Der Ablauf einer \gls{jpeg}-Kompression im Baseline-Modus.
        Die Farbkonvertierung ist optional.
        Quantisierungs- und Huffmantabellen gehen als Parameter in den jeweiligen Kompressionsschritt ein.
        (verändert nach \cite{strutz09}, \acrshort{seite} 195).}
    \label{fig:jpeg-encoder-schema}
\end{figure}

Die Kompression eines digitalen Bildes erfolgt im Baseline-Modus in mehreren Schritten, die auch in Abbildung \ref{fig:jpeg-encoder-schema} dargestellt sind.
Zu Beginn steht die optionale Konvertierung der Farbkomponenten in ein anderes Farbmodell.
Die Farbkomponenten können beispielsweise im \gls{rgb-modell} vorliegen und in das \gls{ycbcr-modell} konvertiert werden.
Das \gls{jpeg}-Verfahren selbst ist farbenblind (\acrshort{vgl} \cite{strutz09}, \acrshort{seite} 194).
Die Algorithmen funktionieren also unabhängig von der Interpretation der Farbinformationen.
Im \gls{jfif}-Format ist die Interpretation jedoch auf das \gls{ycbcr-modell} festgelegt.
Deshalb ist die Konvertierung der Bilddaten notwendig, sollten diese nicht im genannten Farbmodell vorliegen.
Für solche Konvertierungen existieren auch standardisierte Umrechnungsvorschriften.
Beispielsweise wird die Umrechnung vom \gls{rgb-modell} ins \gls{ycbcr-modell} in der Empfehlung "`Studio encoding parameters of digital television for standard 4:3 and wide-screen 16:9 aspect ratios"' (\cite{itur-bt601-7}, \acrshort{seite} 3) beschrieben und im \gls{jfif}-Standard (\cite{iso10918-5}, \acrshort{seite} 3) konkretisiert.

Liegen die Bildinformationen im gewünschten Farbraum vor, können sie einer Farbunterabtastung unterzogen werden.
Diese macht sich zunutze, dass das menschliche Auge Helligkeitsinformationen besser wahrnimmt als Farbinformationen.
Somit fällt es dem Auge nicht weiter auf, wenn nur noch ein Teil der Farbinformationen vorhanden sind.
Auf diese Weise kann die Datenmenge eines Bildes deutlich reduziert werden.

Für die übliche Farbunterabtastung ist es erforderlich, dass die erste Komponente den Helligkeitswert beschreibt.
Die beiden anderen Komponenten beschreiben Farbanteile.
Für den Fall, dass keine Unterabtastung vorgenommen wurde, wird das Verhältnis der Komponenten als \texttt{4:4:4} beschrieben.
Bei \texttt{4:2:2} wird die erste Komponente unberührt gelassen, während die beiden Farbanteile jeweils horizontal halbiert werden.
Damit wird die Datenmenge um ein Drittel reduziert.
Durch eine horizontale und vertikale Halbierung der Farbinformationen wird das \texttt{4:2:0}-Format erreicht.
Die Helligkeitskomponente behält ihr ursprüngliches Format.
Diese Unterabtastung resultiert in einer Halbierung der Datenmenge.
Neben den genannten Verhältnissen gibt es noch weitere, die durch Farbunterabtastung entstehen können.
Allerdings werden meist die hier beschriebenen Formate verwendet.

Im Anschluss wird das Bild in Blöcke von 8x8 Pixeln unterteilt.
Das vereinfacht nicht nur die Weiterverarbeitung, sondern setzt auch den Grundstein für die weitere Kompression.
Danach wird für jeden Block eine \gls{2d-dct} ausgeführt.
Diese Transformation beruht auf dem Prinzip, dass jede Funktion durch eine Reihe an Kosinusfunktionen beschrieben werden kann.
Im Fall von \gls{jpeg} entspricht der Bildblock von 8x8 Pixeln dieser Funktion.
Die Basisfunktionen der \gls{2d-dct}, welche zur Beschreibung eines Bildblocks zur Verfügung stehen, sind in Abbildung \ref{fig:dct_base_8x8} dargestellt.
Durch die \gls{dct} werden die Koeffizienten der Funktionen berechnet, aus deren Summe sich das Originalbild erzeugen lässt.
Der Bildblock des Originalbildes wird also lediglich in eine andere Repräsentation überführt (\acrshort{vgl} \cite{strutz09}, \acrshort{seite} 155).
Das hat jedoch zur Folge, dass die einzelnen Werte des Blocks dekorreliert werden.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{res/img/dct_basefunctions_8x8.png}
    \caption[Die Basisfunktionen der \acrshort{2d-dct} für einen Block von 8x8 Pixeln.]{
        Die Basisfunktionen der \gls{2d-dct} für einen Block von 8x8 Pixeln.
        Der Wertebereich ist das Intervall $[-1, 1]$, wobei Werte nahe 1 hell und Werte nahe -1 dunkel dargestellt sind.
        (aus \cite{strutz09}, \acrshort{seite} 196)}
    \label{fig:dct_base_8x8}
\end{figure}

Nach der Transformation des Bildblocks werden die entstandenen Koeffizienten quantisiert.
Bei der Quantisierung wird jeder Koeffizient $S_{uv}$ auf einen kleineren Wert $Sq_{uv} = round ( S_{uv} / Q_{uv} )$ abgebildet.
Dabei wird die Größe der Quantisierungsstufen $Q_{uv}$ aus einer Quantisierungstabelle entnommen.
Auf diese Weise werden wichtige Koeffizienten beibehalten.
Dazu gehören vor allem die der mittleren Helligkeit und der Grundstrukturen, die in Abbildung \ref{fig:dct_base_8x8} oben links zu sehen sind.
Die Koeffizienten unwichtiger Anteile, in Abbildung \ref{fig:dct_base_8x8} unten rechts zu sehen, werden auf den Wert \texttt{0} reduziert.

Im Anschluss an die Quantisierung erfolgt eine Aufarbeitung der Koeffizienten für die Entropiecodierung.
Statt des DC-Koeffizienten selbst, der die mittlere Helligkeit des Blockes repräsentiert, wird die Differenz zum DC-Koeffizienten des vorigen Blockes codiert.
So werden die Werte für die mittlere Helligkeit benachbarter Blöcke dekorreliert.
Anschließend werden die AC-Koeffizienten, das heißt alle verbleibenden Koeffizienten, umsortiert.
Das geschieht nach absteigender Relevanz in einem Zick"=Zack"=Muster.
Damit stehen der DC-Anteil und die wichtigen AC-Anteile am Anfang.
Alle Koeffizienten am Ende der sortierten Daten, die Null betragen, werden weggelassen.
Damit wird wiederum die Menge an Daten reduziert.

Abschließend werden die quantisierten Transformationskoeffizienten entropiecodiert.
Im Baseline-Modus wird dafür das Huffman-Verfahren verwendet.
Das Prinzip dieses Verfahrens ist, dass häufig auftretende Koeffizienten durch einen kurzen Code repräsentiert werden.
Selten auftretende Werte erhalten einen längeren Code.
Die so erzeugten Codes werden als Huffman-Tabellen im komprimierten Bild integriert, damit dieses auch wieder decodiert werden kann.
Auf diese Weise wird die Datenmenge des Bildes weiter reduziert.
Ein weiterer Vorteil des Huffman-Verfahrens ist die Erzeugung von präfixfreien Codes (\acrshort{vgl} \cite{salomon10}, \acrshort{seite} 214).
Das bedeutet, dass kein Codewort der Beginn eines anderen Codeworts sein kann.
So kann kein Codewort durch unvollständiges Auslesen für ein anderes Codewort gehalten werden.


\section{Das JFIF-Datenformat}
\label{sec:jfif}

Die bei der \gls{jpeg}-Kompression entstandenen Daten werden im \gls{jfif}-Format in einer Datei gespeichert.
Die Abkürzung \gls{jfif} steht für \gls{jfif-gl}.
Das Format ist im Standard ISO10918-5 \cite{iso10918-5} definiert.
Es basiert auf dem Vorschlag für ein binäres Datenformat in Annex B des \gls{jpeg}-Standards \cite{iso10918-1}.
Dieser Vorschlag wird um ein Datenfeld erweitert, in welchem Informationen zum Dateiformat und zur Pixeldichte enthalten sind.
Außerdem besteht auch die Möglichkeit, ein \gls{thumbnail} zu integrieren.
% small hack: The glossary package puts a 's' for the plural form at the end of the entry if nothing else is specified.
Eine weitere Festlegung des \gls{jfif}-Formats ist die Verwendung des \glspl{ycbcr-modell}.
Damit müssen Graustufenbilder eine und farbige Bilder alle drei Farbkomponenten enthalten.
Zuletzt besteht auch die Möglichkeit für \gls{jfif}-Erweiterungen.
% use glspl to get Genitiv
Die im Standard definierten Erweiterungen beziehen sich ausschließlich auf verschiedene Datenformate des integrierbaren \glspl{thumbnail}.
Deshalb soll im weiteren Verlauf nicht weiter diese eingegangen werden.
Des Weiteren wird hier, wie schon zum Beginn des Abschnitts \ref{chap:jpeg-jfif} erwähnt, ausschließlich auf den Baseline-Modus des \gls{jpeg}-Verfahrens eingegangen.

Eine \gls{jfif}-Datei besteht aus einer Sequenz von Segmenten.
Der Beginn jedes Segmentes wird durch einen Marker gekennzeichnet, auf welchen ein Datenfeld mit der Länge des Segmentes folgt.
Es gibt verschiedene Typen an Markern, die Aufschluss auf die Art der im Segment enthaltenen Daten und deren Struktur geben.
Die in dieser Arbeit relevanten Marker sind in Tabelle \ref{tab:jpeg-marker} zu finden.
Ausnahmen von dieser Struktur bilden die \gls{m-soi}-, \gls{m-eoi}- und \gls{m-rst}-Marker.
Diese stehen für sich allein und sind nicht der Anfang eines Segmentes.
\gls{m-soi}- und \gls{m-eoi}-Marker beschreiben den Start \acrshort{bzw} das Ende eines \gls{jpeg}-Bildes.
Alle Informationen, die zur Decodierung des Bildes benötigt werden, müssen sich zwischen den beiden genannten Markern befinden.
Die \gls{m-rst}-Marker werden später noch genauer erläutert.

\begin{table}
    \centering
    \begin{tabularx}{0.8\textwidth}{|l|l|X|}
        \hline
        \textbf{Hexadezimalcode} & \textbf{Symbol} & \textbf{Beschreibung} \\
        \hline
        \texttt{0xFFC0} & SOF$_0$ & Start of frame, Baseline DCT \\
        \texttt{0xFFC4} & DHT & Define Huffman table(s) \\
        \texttt{0xFFD0 - 0xFFD7} & RST$_m$* & Restart with modulo 8 count "`m"' \\
        \texttt{0xFFD8} & SOI* & Start of image \\
        \texttt{0xFFD9} & EOI* & End of image \\
        \texttt{0xFFDA} & SOS & Start of scan \\
        \texttt{0xFFDB} & DQT & Define quantization table(s) \\
        \texttt{0xFFDD} & DRI & Define restart interval \\
        \texttt{0xFFE0 - 0xFFEF} & APP$_n$ & Reserved for application segments \\
        \hline
    \end{tabularx}
    \caption[Wichtige Marker in \acrshort{jfif}-Dateien.]{
        Wichtige Marker in \gls{jfif}-Dateien.
        Symbole mit Asterisk (*) beschreiben Marker, auf die kein Segment folgt.
        (verändert nach \cite{iso10918-1}, \acrshort{seite} 32)}
    \label{tab:jpeg-marker}
\end{table}

Auf den \gls{m-soi}-Marker folgt das \gls{m-app0}-Segment.
Dieses macht von der im \gls{jpeg}-Standard gegebenen Möglichkeit Gebrauch, anwendungsspezifische Daten in das Bild zu integrieren.
Das \gls{m-app0}-Segment ist im \gls{jfif}-Format beschrieben und enthält den nullterminierten String "`JFIF"' und die Versionsnummer des verwendeten Formates.
Außerdem kann das optionale \gls{thumbnail} an dieser Stelle definiert werden.
Da es jedoch im weiteren Verlauf der Arbeit nicht von Bedeutung ist, folgen hier keine weiteren Ausführungen zu den damit verbundenen Inhalten.
In dem genannten Segment finden sich weiterhin auch die Angaben zur horizontalen und vertikalen Pixeldichte.
Diese sind entweder ohne Einheit, in Bildpunkten pro Inch oder Bildpunkten pro Zentimeter angegeben.
Unabhängig von deren Einheit lässt sich aus den beiden Pixeldichten das Seitenverhältnis der Pixel berechnen.
Quadratische Pixel mit dem Seitenverhältnis \texttt{1:1} sind in der digitalen Bildverarbeitung mittlerweile üblich.
Allerdings treten bei manchen Verfahren, wie beispielsweise der digitalen Codierung analoger Fernsehsysteme, nicht"=quadratische Pixel auf (\acrshort{vgl} \cite{itur-bt601-7}).

Auf diese Informationen folgen \gls{m-dqt}- und \gls{m-dht}-Segmente.
Diese beschreiben die bei der \gls{jpeg}-Kompression verwendeten Quantisierungs- und Huffman-Tabellen.
Der Aufbau ist dabei für beide Segmente ähnlich:
Bei Quantisierungstabellen wird zunächst die Präzision der Daten definiert, gefolgt von einer Identifikationsnummer.
Darauf folgen die Einträge der Tabelle.
Die Einträge von Huffman-Tabellen besitzen eine definierte Genauigkeit von 8 Bit.
Dafür wird neben der Identifikationsnummer auch eine Klasse angegeben, welche die Tabelle für die Verwendung für Gleich- \acrshort{bzw} Wechselanteile qualifiziert.
Daran anschließend folgen wieder die Inhalte der Tabelle.
Sowohl für \gls{m-dqt}- als auch für \gls{m-dht}-Segmente gilt:
Es können mehrere dieser Segmente im Bild enthalten sein und jedes Segment kann mehrere Tabellen definieren.
Im Fall von mehreren Tabellen pro Segment wiederholt sich der eben beschriebene Aufbau für jede Tabelle.

Sind die bei der Kompression verwendeten Tabellen definiert, folgt im Baseline-Modus das Segment \gls{m-sof0}.
Neben diesem gibt es noch weitere \gls{m-sof}-Marker, die jeweils den bei der Kompression verwendeten Modus angeben.
Der \gls{m-sof0}-Marker beschreibt im Baseline-Modus komprimierte Daten, die mit dem Huffman-Verfahren entropiecodiert wurden.
In den meisten Fällen darf ein \gls{jpeg}-Bild nur ein \gls{m-sof}-Segment besitzen.
Die einzige Ausnahme bildet dabei die Kompression im hierarchischen Modus, auf den hier jedoch nicht weiter eingegangen wird.

In jedem \gls{m-sof}-Segment werden die Präzision der komprimierten Daten sowie die Dimensionen des Bildes angegeben.
Zusätzlich dazu wird die Anzahl der Komponenten im Bild definiert, auf welche die Beschreibung jeder einzelnen Komponente folgt.
Die Beschreibung einer Komponente besteht dabei aus ihrer Identifikationsnummer, der horizontaler und vertikaler Abtastrate sowie der Identifikationsnummer der verwendeten Quantisierungstabelle.
Werden die Abtastraten aller Komponenten miteinander verglichen, lässt sich daraus das aus der Farbunterabtastung entstandene Verhältnis ermitteln.

Nach den Informationen des \gls{m-sof}-Segments folgen ein oder mehrere \gls{m-sos}-Segmente.
Wie viele dieser Segmente im Bild enthalten sind, hängt von der Reihenfolge der codierten Blöcke im Datenstrom ab.
Werden die Blöcke der Farbkomponenten getrennt voneinander gespeichert, sodass alle Blöcke einer Komponente aufeinander folgen, heißt die Art der Speicherung \gls{non-interleaved}.
Daraus resultieren mehrere \gls{m-sos}-Segmente, da in jedem Scan alle codierten Blöcke einer Komponente gespeichert werden.
Für jede Komponente des Bildes würde so ein \gls{m-sos}-Segment entstehen.
Alternativ dazu können die Daten \gls{interleaved} gespeichert werden.
Das bedeutet, dass die codierten Blöcke aller Farbkomponenten eines Bildblockes aufeinanderfolgend gespeichert werden.
Aus dieser Art der Speicherung entsteht ein einziges \gls{m-sos}-Segment, da die Blöcke der Farbkomponenten immer abwechselnd nacheinander folgen.
Eine Visualisierung des Unterschieds zwischen den beiden genannten Arten der Speicherung ist in Abbildung \ref{fig:jfif-interleaved} zu sehen.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{res/img/interleaved-vs-non-interleaved.png}
    \caption[Unterschied zwischen \gls{interleaved} und \gls{non-interleaved} bei der Speicherung von Datenblöcken.]{
        Unterschied zwischen \gls{interleaved} und \gls{non-interleaved} bei der Speicherung von Datenblöcken.
        (aus \cite{iso10918-1}, \acrshort{seite} 24)}
    \label{fig:jfif-interleaved}
\end{figure}

Ein \gls{m-sos}-Segment gibt die Anzahl der Farbkomponenten in diesem Scan an.
Danach wird für jede Komponente spezifiziert, welche der zuvor definierten Huffman-Tabellen zur Codierung von Gleich- \acrshort{bzw} Wechselanteilen genutzt werden soll.
Im Anschluss daran folgen die Kompressionsdaten.
Nach jedem in diesen Daten natürlich vorkommenden \texttt{0xFF}-Byte wird ein Null-Byte ergänzt, um die Verwechslung mit den im Datenformat verwendeten Markern zu verhindern.

Auf eine weitere Funktionalität des \gls{jfif}-Dateiformates soll hier noch eingegangen werden.
In den entropiecodierten Daten des \gls{m-sos}-Segments können \gls{m-rst}-Marker vorkommen.
Diese Marker signalisieren, dass die folgenden Daten ohne Wissen über die davor liegenden Daten decodiert werden können.
Dafür muss allerdings ein \gls{m-dri}-Segment vor dem \gls{m-sos}-Segment vorkommen, welches die Abstände dieser Marker definiert.
Ohne ein solches Segment sind \gls{m-rst}-Marker nicht zulässig.
Von Vorteil sind diese Marker, wenn Daten während einer Datenübertragung verloren gehen und nicht erneut übertragen werden können.
Ein Beispiel dafür ist die Übertragung von Echtzeitstreams.
Außerdem können sie gegebenenfalls Fehler reduzieren, die durch die Prädiktion der DC-Koeffizienten entstehen.

