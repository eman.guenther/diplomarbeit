\chapter{JPEG-spezifische Verschlüsselung}

Eine generische Verschlüsselung wie beispielsweise das angesprochene \gls{srtp}"=Protokoll erfüllt nicht immer alle Anforderungen (\acrshort{vgl} \cite{li20}, \acrshort{seite} 1).
Beispielsweise schützt \gls{srtp} die Daten zwar während der Übertragung, nach der Entfernung der Übertragungsverschlüsselung können diese jedoch ungehindert weiterverarbeitet werden.
Zur Verschlüsselung von \gls{jpeg}"=Dateien wurde deshalb schon viel geforscht.
Das Ergebnis dieser Forschung ist eine Vielzahl spezieller Verfahren, deren Eignung für die gewünschten Anwendungsszenarien jeweils überprüft werden muss.

Wie bei anderen Verschlüsselungsverfahren auch spielt die Aushandlung des Verfahrens und des Schlüssels zwischen den Kommunikationsteilnehmern eine wesentliche Rolle.
Das \gls{jfif}"=Format bietet dabei keine standardisierte Möglichkeit, ein bei der Verschlüsselung verwendetes Verfahren anzugeben.
Auch aufgrund der Vielfalt unterschiedlicher Verfahren ist die Kommunikation des Verfahrens schwierig.
Deshalb muss diese zwangsläufig über einen anderen Kanal als das \gls{jpeg}"=Bild selbst stattfinden.

Nachfolgend soll zunächst genauer auf die Gründe für die Anwendung einer \gls{jpeg}"=spezifischen Verschlüsslung eingegangen werden.
Dem folgt ein Überblick über die verschiedenen Ansätze einer solchen Verschlüsselung.
Da im "`RTSP-Streaming"' bereits komprimierte Bilder verschlüsselt werden sollen, liegt ein besonderer Fokus auf der Verarbeitung der Bilddaten im \gls{jfif}"=Format.
Ein weiterer Abschnitt beschäftigt sich mit dem Entwurf einer \gls{jpeg}"=Verschlüsselung.
Deren Implementierung und Unsicherheit wird im sich daran anschließenden Abschnitt besprochen.
Abschließend wird ein besseres, doch deutlich aufwendigeres Verschlüsselungsverfahren für \gls{jpeg}"=Bilder vorgestellt.


\section{Gründe für die Anwendung JPEG-spezifischer Verschlüsselung}

Der deutlichste Unterschied zwischen generischen Übertragungsverschlüsselungen und \gls{jpeg}"=spezifischer Verschlüsselung ist die logische Ebene, auf der die Verfahren angewendet werden.
Bei Übertragungsverschlüsselungen, zu denen auch \gls{srtp} gehört, werden meist Netzwerkpakete verarbeitet.
Ihr Schutz währt nur für die Dauer der Übertragung vom Sender zum Empfänger.
Nachdem die Pakete vom Empfänger entschlüsselt wurden, sind die Originaldaten wieder hergestellt und können weiterverarbeitet werden.
\gls{jpeg}"=spezifische Verschlüsselungen werden für jedes Bild einzeln angewendet.
Bei der Übertragung eines auf diese Weise verschlüsselten Bildes wäre das Datenformat als \gls{jpeg} identifizierbar.
Jedoch könnte der originale Bildinhalt nicht ohne die korrekte Entschlüsselung angezeigt werden.
Der Vorteil einer auf Bildebene agierenden Verschlüsselung ist dabei, dass die Daten auch nach dem Empfang noch vor Weiterverarbeitung geschützt sind.
Darunter sind sowohl die Bearbeitung der Bildinhalte selbst als auch das Kopieren und Weitergeben des Bildes zu verstehen.

Ein weiterer Vorteil spezifischer Verschlüsselungen liegt in der Menge der zu verschlüsselnden Daten.
Generische Verfahren verschlüsseln meist alle zu schützenden Daten ohne Rücksicht auf deren Struktur.
Das erleichtert die Verschlüsselung vieler verschiedener Datenstrukturen.
Schwierigkeiten können dabei jedoch entstehen, wenn große Mengen an Daten in Echtzeit zu verarbeiten sind.
Auch wenn moderne Rechner ohne Probleme größere Datenmengen verarbeiten können, liegen die Grenzen der Verarbeitbarkeit entweder in noch größeren Datenmengen oder schwächerer Hardware, wie beispielsweise "`Internet of Things"'"=Geräten.
Bei \gls{jpeg}"=spezifischen Verfahren ist es möglich, nur einen Teil der Daten zu verschlüsseln und dabei dieselbe Sicherheit zu erlangen.
Der Grund dafür ist, dass bestimmte Datenfelder für die Decodierung der Bilder besonders wichtig sind.
Beispielsweise kann die Decodierung nicht das Originalbild wiederherstellen, wenn die Huffman"= oder Quantisierungstabellen verschlüsselt sind.
Gleiches trifft auch auf die Verschlüsselung der entropiecodierten Daten zu.
In der Entwicklung der Verschlüsselung multimedialer Inhalte wurde partielle Verschlüsselung deshalb dazu genutzt, um die Echtzeitfähigkeit der Verfahren herzustellen (\acrshort{vgl} \cite{lian09}, \acrshort{seite} 3).

Schlussendlich können auch Anforderungen an das Datenformat Gründe für die Verwendung von \gls{jpeg}"=Verschlüsselung sein.
Für generische Verfahren gibt es häufig keine Notwendigkeit eines bestimmten Datenformates.
Bei \gls{srtp} ist zwar der Header der \gls{rtp}"=Pakete gefordert, die enthaltenen Daten müssen allerdings kein bestimmtes Format erfüllen.
Im Gegensatz dazu ist das Ergebnis \gls{jpeg}"=spezifischer Verfahren ein verschlüsseltes Bild im \gls{jpeg}"=Format.
Durch die Beachtung des Datenformates bei der Verschlüsselung werden wiederum gültige \gls{jpeg}"=Bilder erzeugt.
Dabei muss vor allem auf die Marker geachtet werden.
Zum einen dürfen diese nicht verschlüsselt werden, weil sonst Formatinformationen verloren gingen.
Zum anderen müssen die Verschlüsselungsverfahren darauf achten, keine Daten zu erzeugen, die fälschlicherweise als Marker erkannt werden könnten.
Diese würden sonst zu einer Kompromittierung des Datenformates führen.
Im schlimmsten Fall könnte ein solches verschlüsseltes \gls{jpeg}"=Bild auch nicht wieder korrekt entschlüsselt werden.
Neben der Notwendigkeit für die erfolgreiche Entschlüsselung hat die Beibehaltung des \gls{jpeg}"=Formates außerdem den Effekt, dass verschlüsselte Bilder von Decodern verarbeitet werden können, welche das Verschlüsselungsverfahren nicht implementieren.


\section{Gliederung von JPEG-basierten Verschlüsselungsverfahren}
\label{sec:enc-jpeg-possibilities}

Abhängig von den Anforderungen an die Verschlüsselung von \gls{jpeg}"=Bildern erweisen sich Verfahren mit unterschiedlichen Ansätzen als geeignet.
Diese Verfahren lassen sich entsprechend ihrer Abfolge im Bezug auf die \gls{jpeg}"=Kompression gliedern.
Dabei ist die Verschlüsselung von Bilddaten vor, während oder nach der Kompression möglich.
Eine Übersicht über exemplarische Verfahren der verschiedenen Ansätze haben \citeauthor{li20} in ihrer Untersuchung aus dem Jahr \citeyear{li20} erstellt (\acrshort{vgl} \cite{li20}).

Die Verschlüsselung von Bilddaten vor der Kompression ist im Allgemeinen einfach zu implementieren, da sie die unkomprimierten Daten verarbeitet.
Sie wird häufig als \gls{enc-etc} bezeichnet.
Da für die anschließende Kompression keine Änderungen stattfinden müssen, ist keine angepasste Software für die Codierung und Decodierung erforderlich.
Das Ergebnis der Kompression eines verschlüsselten Bildes entspricht außerdem dem geforderten Format der Daten.
Der deutlichste Nachteil solcher Verfahren ist ihr Einfluss auf die Kompressionsqualität.
Durch die Verschlüsselung der Eingangsdaten der Kompression wird die Korrelation der Pixel im Originalbild zerstört (\acrshort{vgl} \cite{li20}, \acrshort{seite} 478).
Auf dieser beruht allerdings das \gls{jpeg}"=Kompressionsverfahren.
Verschlüsselungen, welche diese Tatsache nicht berücksichtigen, verringern die Datenreduktion durch die Kompression deutlich.
Ein \gls{enc-etc}"=Verfahren sollte Bilder deshalb auf solche Weise verschlüsseln, dass die Korrelation der Pixel bestmöglich beibehalten wird.

Bei der Verschlüsselung von Bildern während der Kompression gibt es deutliche Unterschiede in den Abläufen und den Eigenschaften der Verfahren.
Diese sind vor allem davon abhängig, an welcher Stelle der Kompression das jeweilige Verfahren ansetzt.
Derartige Verschlüsselungen haben den Vorteil, dass sie auf Zwischenergebnisse der Datenreduktion zugreifen und diese modifizieren können.
So agieren diese Verfahren beispielsweise auf der Basis der quantisierten Koeffizienten oder durch Veränderungen in der Entropiecodierung.
Viele der während der Kompression ablaufenden Verschlüsselungen können jedoch einen negativen Einfluss auf das Gesamtergebnis haben.
Die Schwierigkeit dieser Verfahren besteht dabei darin, die Eigenschaften der Originaldaten bei der Verschlüsselung beizubehalten.
Durch die Modifikation von Werten, wie beispielsweise Huffmantabellen, können die Daten gegebenenfalls nicht mehr so effizient reduziert werden.
Für die verbreitete Nutzung solcher Verfahren ist außerdem hinderlich, dass angepasste Codierer und Decodierer verwendet werden müssen.
Standardsoftware für die Anzeige von \gls{jpeg}"=Bildern könnten die so erzeugten Bilder zwar verarbeiten.
Da jedoch die entsprechenden Verschlüsselungsverfahren im Allgemeinen nicht unterstützt werden, ist die Entschlüsselung und Anzeige des Originalbildes nicht möglich.

\gls{jpeg}"=Verschlüsselungsverfahren, die nach der Kompression verschlüsseln, werden "`bitstream"=based"' genannt.
Da sie den Kompressionsvorgang nicht beeinflussen, sind sie mit Standardsoftware für die Verarbeitung von \gls{jpeg}"=Bildern kompatibel.
Außerdem ist es solchen Verfahren aufgrund ihrer Unabhängigkeit von der Kompression möglich, verschlüsselte Bilder mit ähnlicher Größe wie die Originalbilder zu erzeugen.
Allerdings muss besondere Vorsicht angewandt werden, um das Datenformat beizubehalten.
Generische Verschlüsselungsalgorithmen wie beispielsweise \gls{aes} würden bei der Anwendung auf große Datenblöcke das Format der Datei zerstören.
Deshalb ist eine umsichtige Auswahl der zu verschlüsselnden Datenabschnitte nötig.
Für das \gls{jpeg}"=Format muss darauf geachtet werden, dass keine Marker verschlüsselt werden.
Andererseits dürfen bei der Kompression keine Daten entstehen, die als Marker fehlinterpretiert werden könnten.
Ähnliches gilt für die Verschlüsselung von entropiecodierten Daten im \gls{m-sos}"=Segment.
Um ein Bild auch nach der Verschlüsselung noch decodieren zu können, dürfen weder Huffmancodes verschlüsselt noch Daten erzeugt werden, die fälschlicherweise als solche Codes interpretiert werden können.


\section{JPEG-Verschlüsselung auf Basis der Quantisierungstabellen}

Für den Entwurf einer auf der \gls{jpeg}"=Struktur basierenden Verschlüsselung bieten die Huffman- und Quantisierungstabellen einen guten Ansatzpunkt.
Beide Arten von Tabellen sind zwingend für die korrekte Decodierung der Bilder erforderlich.
Eine Verschlüsselung dieser Daten hätte demnach zur Folge, dass ein Bild nur mit dem korrekten Schlüssel betrachtet werden kann.
Weiterhin handelt es sich bei den Tabellen um einige Blöcke an Daten, die als Gesamtes verarbeitet werden können.
Das ist besonders für die Verschlüsselungsalgorithmen von Vorteil.
Nicht zuletzt ist durch die Verschlüsselung der Tabellen nur ein Bruchteil des ganzen Bildes zu verarbeiten.
Das macht das Verfahren schnell und in Echtzeit ausführbar.

Die weiteren Anforderungen an die Verschlüsselungen werden im nächsten Abschnitt diskutiert.
Außerdem wird darin ein Entwurf des Verfahrens erstellt.
Daran schließt sich ein Abschnitt zur Betrachtung der Implementierung der Verschlüsselung an.
Zuletzt werden die Schwachstellen des entwickelten Verfahrens analysiert und ein Angriffsversuch auf dasselbe unternommen.


\subsection{Entwurf des Verfahrens}
\label{sec:enc-jpeg-draft}

Für den Entwurf einer \gls{jpeg}"=basierten Verschlüsselung ist zunächst die Analyse der Anforderungen erforderlich.
Diese entsprechen größtenteils den in Abschnitt \ref{sec:enc-requirements} ausgeführten.
Für den Anwendungsfall der Übertragung per \gls{rtp} sollen diese genauer beschrieben werden.
Auf diese Weise ist ein konkreter Entwurf der Verschlüsselung möglich.
Eine Authentifikation der Bilder ist dabei nicht integriert.
Sie ist zwar grundsätzlich möglich, erfordert jedoch eigene Verfahren.
Diese können beispielsweise den Urheber des entsprechenden Bildes vermerken.
Aufgrund des Umfangs der Authentifikationsmechanismen und deren Implementierung spielen sie im weiteren Verlauf der Arbeit allerdings keine Rolle.

Angewendet werden soll die Verschlüsselung bei der \gls{rtp}"=Übertragung von Videodateien im \gls{mjpeg}"=Format.
Als Grundlage der Implementierung wird das Projekt "`RTSP"=Streaming"' verwendet.
Da die Bilder im besagten Projekt gemäß RFC 2435 \cite{rfc2435} zu \gls{rtp}"=Paketen umgeformt werden, muss das \gls{jpeg}"=Datenformat eingehalten werden.
Dementsprechend findet die geplante Verschlüsselung zwischen dem Auslesen der Bilddaten und deren Verarbeitung zu Paketen statt.
Die Verschlüsselung operiert damit auf den komprimierten Bilddaten.
Daraus folgen die in Abschnitt \ref{sec:enc-jpeg-possibilities} erläuterten Schwierigkeiten für die Verschlüsselung komprimierter Daten.
Diese erweisen sich jedoch für das Grundkonzept der zu entwerfenden Verschlüsselung als nicht besonders kompliziert.

Der Kerngedanke des Entwurfes ist die Verschlüsselung von für die Decodierung benötigten Tabellen.
Dies sind entweder die bei der Entropiecodierung verwendeten Huffmantabellen oder die Quantisierungstabellen, welche die für die \gls{dct}"=Koeffizienten benötigte Datenmenge reduzieren.
Allerdings sind für die Übertragung von \gls{jpeg}"=Bildern per \gls{rtp} bestimmte Huffmantabellen vorgegeben (siehe Abschnitt \ref{sec:rfc2435}).
Eine Verschlüsselung der Huffmantabellen wäre deshalb nutzlos, da diese ohnehin nicht in die Pakete eingebracht und somit nicht übertragen werden.
Die Quantisierungstabellen können dagegen in einem eigenen Abschnitt der \gls{rtp}"=Pakete für \gls{jpeg} transportiert werden.

Im \gls{jpeg}"=Datenformat sind Quantisierungstabellen in Form von \gls{m-dqt}"=Segmenten gespeichert.
Diese enthalten mindestens eine der genannten Tabellen.
Sind für die Decodierung eines \gls{jpeg}"=Bildes mehrere Quantisierungstabellen erforderlich, können diese dementsprechend in einem oder auch in mehreren \gls{m-dqt}"=Segmenten definiert werden.
Solche Segmente besitzen ein vorgegebenes Format, was die Verarbeitung der enthaltenen Daten vereinfacht.
Die Tabellen liegen dabei als Datenblock mit fester Länge vor.
Solange sie bei der Verschlüsselung auf ein Chiffrat derselben Länge abgebildet werden, vergrößern sich die zu übermittelnden Daten nicht.
Auch eine Fehlinterpretation der verschlüsselten Daten wird ausgeschlossen, wenn die für die Speicherung der Quantisierungstabellen benötigten Datenmenge gleich bleibt.
Da auf diese Weise die vorgegebene Struktur nicht verändert wird, besitzt auch das verschlüsselte Bild ein valides \gls{jpeg}"=Format.
Weil mit den Quantisierungstabellen außerdem nur Teile eines Bildes verschlüsselt werden, ist die Verarbeitung schneller als bei einer Verschlüsselung des gesamten Bildes.
Diese Tatsache trägt dazu bei, dass die Verschlüsselung in Echtzeit stattfinden kann.

Um die Datenmenge bei der Verschlüsselung nicht zu vergrößern, muss ein geeignetes Verschlüsselungsverfahren gewählt werden.
Als schnelle Verfahren kommen dabei Blockchiffren und Stromchiffren infrage.
Allerdings sind nur Stromchiffren in der Lage, alle möglichen Eingangsdaten auf ein Chiffrat derselben Länge abzubilden.
Da Blockchiffren jedoch deutlich weiter verbreitet sind, liegt die Lösung in der Wahl eines geeigneten Betriebsmodus.
Ein Beispiel dafür ist der \gls{aes-cm}, welcher auch im \gls{srtp} verwendet wird und für die zu entwickelnde Verschlüsselung genutzt werden soll.
Der Counter"=Modus erlaubt die Verwendung des \gls{aes} als Stromchiffre, womit die für die Speicherung benötigte Datenmenge durch die Verschlüsselung gleich bleibt.
Wie im Abschnitt \ref{sec:enc-srtp-encryption} bereits erläutert, handelt es sich beim \gls{aes} um ein sicheres Verschlüsselungsverfahren.
Die Anforderung der kryptografischen Sicherheit für den Verschlüsselungsentwurf ist damit Genüge getan.
Weiterhin gilt ebenfalls, dass die schnelle Abarbeitung der \gls{aes}"=Chiffre die Echtzeitfähigkeit der Verschlüsselung begünstigt.


\subsection{Implementierung und Test der Verschlüsselung}

Für die Implementierung der entworfenen Verschlüsselung im "`RTSP"=Streaming"'"=Projekt wurde die neue Klasse \texttt{JpegEncryptionHandler} angelegt.
Diese realisiert die vorgestellte Funktionalität und stellt sie dem \texttt{RtpHandler} zur Verwendung bereit.
In Abbildung \ref{fig:enc-jpeg-classes} ist die Anbindung des \texttt{JpegEncryptionHandler}s an das Projekt dargestellt.
Als Schnittstellen fungieren dabei die beiden Methoden \texttt{encrypt()} und \texttt{decrypt()}, welche jeweils die Verschlüsselung \acrshort{bzw} Entschlüsselung von \gls{jpeg}"=Bildern vornehmen.
Da die Verschlüsselung im Gegensatz zu \gls{srtp} jedoch auf Bildebene agiert, muss sie an anderen Stellen in der Verarbeitung der Bilder zu Paketen aufgerufen werden.
Direkt nach dem Auslesen eines Bildes aus der Videodatei wird das Bild verschlüsselt.
Damit können alle nachfolgenden Schritte ohne Änderung durchgeführt werden.
Die Entschlüsselung eines Bildes erfolgt erst kurz vor der Anzeige desselben in der grafischen Oberfläche.
Jegliche Verarbeitung davor dient der Extraktion des Bildes aus dem \gls{rtp}"=Paket.
Erst kurz vor der Darstellung ist das Bild im \gls{jpeg}"=Format vorhanden, welches zur Entschlüsselung erforderlich ist.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/jpeg-classes.png}
    \caption[Anbindung des \texttt{JpegEncryptionHandler}s mit vereinfachter Klassenstruktur.]{
        Anbindung des \texttt{JpegEncryptionHandler}s an das Projekt mit vereinfachter Klassenstruktur.
        Die Schnittstelle zur Funktionalität besteht aus Methoden zur Verschlüsselung und Entschlüsselung von \acrshort{jpeg}"=Bildern und einer Methode zum Angriff auf die Verschlüsselung.}
    \label{fig:enc-jpeg-classes}
\end{figure}

Die Abläufe von Ver- und Entschlüsselung sind im entworfenen Verfahren nahezu identisch.
Zunächst wird das zu verarbeitende Bild nach \gls{m-dqt}"=Markern durchsucht.
Wird ein solcher Marker gefunden, beginnt die weitere Verarbeitung.
Dabei werden die im durch den Marker gekennzeichneten Segment enthaltenen Quantisierungstabellen einzeln ver- \acrshort{bzw} entschlüsselt.
Der dafür benötigte Schlüssel und der "`Salt"'"=Parameter werden dabei als bereits ausgetauscht angenommen.
Auf diese Weise ist die Implementierung des Schlüsselaustausch nicht erforderlich.
Im Anschluss an die Verschlüsselung der Tabellen des Segments wird nach weiteren \gls{m-dqt}"=Markern gesucht.
Sind im Bild weitere solche Segmente vorhanden, werden diese äquivalent verarbeitet.
Alle bei dieser Verarbeitung nicht verschlüsselten Abschnitte der Datei werden ohne Änderung übernommen.

\begin{figure}[h]
    \centering
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{res/img/htw_plain.jpeg}
        \caption{Originalbild "`SdI"'}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{res/img/lndw_plain.jpeg}
        \caption{Originalbild "`LNdW"'}
    \end{subfigure}
    \hfill
    \caption[Originalbilder aus dem Video zum Studium der Informatik und dem Video zur Langen Nacht der Wissenschaften 2019.]{
        Originalbilder aus dem Video zum Studium der Informatik \cite{youtube-sdi17} und dem Video zur Langen Nacht der Wissenschaften 2019 \cite{youtube-lndw19}.
        Bei den Bildern handelt es sich jeweils um das erste Frame des entsprechenden Videos.}
    \label{fig:enc-plain}
\end{figure}

Um die Korrektheit der Verschlüsselung zu überprüfen, wurde ein einfacher Test implementiert.
Dieser liest ein Bild aus der als Parameter angegebenen Datei ein und verarbeitet es.
Für jeden Schritt in der Verarbeitung wird ein separates Bild in das Arbeitsverzeichnis geschrieben, welches nach der Ausführung angesehen und inspiziert werden kann.
Als Testbilder wurden jeweils das erste Frame aus zwei Videos genutzt.
Dabei handelt es sich um das Video zum Studium der Informatik an der \gls{htwdd} \cite{youtube-sdi17} sowie das Video zur Langen Nacht der Wissenschaften an der \gls{htwdd} im Jahr \citeyear{youtube-lndw19} \cite{youtube-lndw19}.
Die für das Testen der Verschlüsselung verwendeten Bilder sind in Abbildung \ref{fig:enc-plain} dargestellt.
Bei dem Test werden die Bilder zunächst verschlüsselt und anschließend wieder entschlüsselt.

Die Durchführung des Tests ergab, dass die Verschlüsselung korrekt implementiert wurde und umkehrbar ist.
Das heißt, bei der Entschlüsselung des verschlüsselten Bildes entsteht wieder das Originalbild.
Eine Schwachstelle zeigte sich bei der Betrachtung der verschlüsselten Bilder, welche in Abbildung \ref{fig:enc-cipher} zu sehen sind.
Während die Farbwerte und die blockinternen Strukturen zwar deutlich unkenntlich gemacht sind, können einige Konturen des Originalbildes immer noch erkannt werden.
In der Verschlüsselung des Bildes "`SdI"' bezieht sich das vor allem auf die Umrisse der Augen des Roboters.
Dagegen können im verschlüsselten Bild "`LNdW"' hauptsächlich die Konturen von Farbflächen erkannt werden.
Aber auch detailliertere Strukturen, wie beispielsweise die Umrisse der Panele auf der linken Seite oder zwei an der Decke hängende Luftballons, können erahnt werden.
Dass solche Konturen auch noch in den verschlüsselten Bildern erkannt werden können, weist auf eine geringe wahrnehmungsbezogene Sicherheit hin.
Konkret bedeutet das, dass trotz angewendeter Verschlüsselung noch Inhalte des Originalmediums erkennbar sind.
Da nur ein Teil der in den Bildern enthaltenen Daten verschlüsselt wurden, ergibt sich daraus, dass die Auswahl der zu verschlüsselnden Daten beim Entwurf der Verschlüsselung für diese nicht geeignet ist.

\begin{figure}[h]
    \centering
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{res/img/htw_cipher.jpeg}
        \caption{Verschlüsseltes Bild "`SdI"'}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{res/img/lndw_cipher.jpeg}
        \caption{Verschlüsseltes Bild "`LNdW"'}
    \end{subfigure}
    \hfill
    \caption[Bilder mit verschlüsselten Quantisierungstabellen.]{
        Bilder mit verschlüsselten Quantisierungstabellen.}
    \label{fig:enc-cipher}
\end{figure}


\subsection{Angriff auf die Verschlüsselung der Quantisierungstabellen}

Die entworfene Verschlüsselung besitzt einen nachvollziehbaren und sinnvollen Grundgedanken.
Mit der Verschlüsselung der Quantisierungstabellen werden die Farbwerte der Bildblöcke unkenntlich gemacht und das Originalbild kann nicht mehr angezeigt werden.
Durch die Verwendung des \gls{aes} mit Counter"=Modus können die Daten nicht ohne das Wissen über den verwendeten Schlüssel und das "`Salt"' wiederhergestellt werden.
Weil im Verhältnis zur Größe des gesamten Bildes wenig Daten verschlüsselt werden und ein schnelles symmetrisches Verschlüsselungschiffre verwendet wird, kann die Verschlüsselung in Echtzeit stattfinden.

Trotz der größtenteils guten Entscheidungen beim Entwurf der Verschlüsselung zeigte sich bereits im vorigen Abschnitt, dass das entworfene Verfahren eine Schwachstelle besitzt.
Mit der Verschlüsselung der Quantisierungstabellen wurde darauf geachtet, dass ein für die Decodierung notwendiger Teil der Daten geschützt wird.
Allerdings verändert dieser Schutz nicht die Korrelation zwischen benachbarten Bildblöcken.
Da die entropiecodierten Daten in das verschlüsselte Bild übernommen werden, bleibt die Prädiktion der DC"=Koeffizienten bestehen.
Somit ist die ähnliche Helligkeit benachbarter Bildblöcke auch im verschlüsselten Bild zu sehen.
Die auf diese Weise erkennbaren Konturen verletzen die wahrnehmungsbezogene Sicherheit.
Darum ist die entworfene Verschlüsselung unsicher.

Die Tatsache, dass die Quantisierungstabellen verschlüsselt wurden, lässt sich noch weiter ausnutzen.
Wie schon festgestellt wurde, können die verschlüsselten Daten nicht ohne Weiteres von Dritten entschlüsselt werden.
Allerdings lässt sich die Verschlüsselung durch eine Ersetzung angreifen.
Deren Prinzip beruht darauf, dass Quantisierungstabellen immer eine feste Größe besitzen und der \gls{jpeg}"=Standard Beispieltabellen angibt.
Diese Tabellen des Standards wurden empirisch ermittelt und erzielen gute Ergebnisse (\acrshort{vgl} \cite{iso10918-1}, \acrshort{seite} 143).
Insofern weichen sie aller Wahrscheinlichkeit nicht sehr von den in den Beispielbildern verwendeten Quantisierungstabellen ab.

Anstatt der Entschlüsselung empfangener Bilder können dementsprechend die Speicherorte der verschlüsselten Quantisierungstabellen gesucht und diese durch die Tabellen des \gls{jpeg}"=Standards ersetzt werden.
Im "`RTSP"=Streaming"'"=Projekt wurde diese Vorgehensweise implementiert und im Client als Option verfügbar gemacht.
Außerdem wurde ein Testfall für diesen Angriff erstellt.
Die Ergebnisse für die Ersetzung der Tabellen in den Beispielbildern sind in Abbildung \ref{fig:enc-attack} dargestellt.

\begin{figure}[h]
    \centering
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{res/img/htw_attack.jpeg}
        \caption{Ergebnis des Ersetzungsangriffs auf das verschlüsselte Bild "`SdI"'}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{res/img/lndw_attack.jpeg}
        \caption{Ergebnis des Ersetzungsangriffs auf verschlüsselte Bild "`LNdW"'}
    \end{subfigure}
    \hfill
    \caption[Ersetzungsangriff auf die verschlüsselten Bilder.]{
        Ersetzungsangriff auf die verschlüsselten Bilder.}
    \label{fig:enc-attack}
\end{figure}

Wie in der Darstellung zu sehen, erweist sich der Angriff auf die verschlüsselten Bilder als erfolgreich.
Die Farbwerte wurden weitestgehend wieder hergestellt und nur einige Blockartefakte verbleiben.
Im Vergleich zu den Originalbildern scheinen die Ergebnisse der Angriffe einen etwas erhöhten Kontrast zu besitzen.
Das ist auf die Unterschiede der Quantisierungstabellen aus dem \gls{jpeg}"=Standard zu den tatsächlich bei der Kompression verwendeten Tabellen zurückzuführen.

Dieser Angriff zeigt außerdem, dass der Entwurf einer Verschlüsselung für Mediendaten viel Aufwand erfordert.
Neben der Erfüllung der anwendungsspezifischen Anforderungen ist jederzeit die Sicherheit des Verfahrens zu beurteilen.
Auch bei ansonsten gut gewählten Verschlüsselungsparametern reicht eine Schwachstelle aus, um das Verfahren zu kompromittieren.
Bei der hier entworfenen Verschlüsselung liegt der Schwachpunkt in der Auswahl der zu verschlüsselnden Daten.
Da es Beispieldaten für Quantisierungstabellen gibt und sich diese generell aufgrund ihrer Struktur und Bedeutung bei der Kompression ähnlich sehen, eignen sie sich nicht als Ziel der Verschlüsselung.
Außerdem hat die Verschlüsselung dieser Tabellen keinen Einfluss auf die Korrelation der Bildblöcke, weshalb diese auch nach der Verarbeitung bestehen bleibt.
Stattdessen könnte eine Verschlüsselung der entropiecodierten Daten zu einem sichereren Verfahren führen.


\section{Bitstream-basierte Verschlüsselung mit Beibehaltung der Dateigröße}

Wie sich in den vorigen Abschnitten gezeigt hat, sind bei der Gestaltung \gls{jpeg}"=basierter Verschlüsselungen viele verschiedene Aspekte zu beachten.
Der in \ref{sec:enc-jpeg-draft} gewählte Ansatz berücksichtigt die meisten dieser Anforderungen.
Seine Schwachstelle liegt jedoch, wie bereits ermittelt, bei der Auswahl der zu verschlüsselnden Daten.
Ein weiteres Verfahren, welches komprimierte Bilddaten verschlüsselt, stellen \citeauthor{kobayashi18} in ihrem Artikel \cite{kobayashi18} vor.
Statt der Verschlüsselung der Quantisierungstabellen stellen sie ein Verfahren vor, welches die entropiecodierten Daten schützt.

Bei der Huffmancodierung in der \gls{jpeg}"=Kompression werden die quantisierten Koeffizienten auf einen Huffmancode abgebildet.
Dieser leitet sich aus der Größenordnung des Wertes ab.
Maßgeblich ist dafür das höchste gesetzte Bit im ein Byte großen Wert.
Der Koeffizient wird anschließend in Form des Huffmancodes codiert.
Um den ursprünglichen Wert vollständig wiederherstellen zu können, wird eine Repräsentation desselben im Anschluss an den Huffmancode gespeichert.
Da diese eine je nach Huffmancode unterschiedliche Größe besitzt, werden die Daten der Repräsentation auch als zusätzliche Bits bezeichnet.
Das Grundprinzip des von \citeauthor{kobayashi18} vorgestellten Verfahrens basiert auf der Verschlüsselung dieser Bits.
Da diese die ursprünglich quantisierten Werte codieren, macht eine Verschlüsselung derselben die Darstellung des Originalbildes unmöglich.
Dadurch ist die Sicherheit des Verfahrens im Bezug auf die Wahrnehmung gewährleistet.
Die Schwierigkeit des Verfahrens liegt dabei in der Ermittlung der zu verschlüsselnden Daten.
Für die Extraktion der zusätzlichen Bits muss das vollständige Segment der entropiecodierten Daten geparst werden.
Erforderlich sind dafür die vollständigen Huffmantabellen, wie sie zur Decodierung von Bildern gebraucht werden.
Damit bedarf die Implementierung der Verschlüsselung die vollständige Funktionalität der Huffmandecodierung aus der \gls{jpeg}"=Kompression.
Außerdem werden die entropiecodierten Daten bei der Verschlüsselung byteweise verarbeitet.
Wie in Abbildung \ref{fig:kobayashi-huffman-coding} zu sehen ist, überschreiten Huffmancodes und zusätzliche Bits aber teilweise die Bytegrenzen.
So ist eine reine byteweise Verarbeitung zur Verschlüsselung der Daten nicht möglich.
Trotz des einfachen Grundgedankens des Verschlüsselungsverfahrens stellt die Realisierung desselben einen sehr hohen Aufwand dar.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{res/img/huffman-byte-stuffing.png}
    \caption[Beispiel für das Ergänzen eines Null"=Bytes in den entropiecodierten Daten.]{
        Beispiel für das Ergänzen eines Null"=Bytes in den entropiecodierten Daten.
        Huffmancodes und zusätzliche Bits überschreiten dabei häufig die Bytegrenzen.
        (aus \cite{kobayashi18}, \acrshort{seite} 385).
    }
    \label{fig:kobayashi-huffman-coding}
\end{figure}

Für die Verschlüsselung nach \citeauthor{kobayashi18} wird zunächst byteweise über die entropiecodierten Daten iteriert.
Dabei werden die zuvor beschriebenen zusätzlichen Bits extrahiert, wenn zwei Bedingungen erfüllt sind.
Diese Bedingungen gewährleisten, dass die Decodierbarkeit des \gls{jpeg}"=Bildes beibehalten wird.
Zum einen müssen in dem entsprechenden Byte sowohl Teile von Huffmancodes als auch Teile von zusätzlichen Bits enthalten sein.
Zum anderen muss der enthaltene Huffmancode mindestens ein Null"=Bit haben.
Der Grund dafür ist, keine neuen \texttt{0xFF}"=Bytes zu erzeugen, da diese im \gls{jpeg}"=Format als Kennzeichnung eines Markers gelten.
Um ein auf diese Weise erzeugtes Byte als Datum zu kennzeichnen, müsste ein zusätzliches Null"=Byte eingefügt werden.
Bei der Entschlüsselung könnten die dabei entstehenden Bytes \texttt{0xFF 0x00} allerdings nicht eindeutig entschieden werden, ob sie durch die Verschlüsselung oder durch die Kompression entstanden sind.
Eine forcierte Entscheidung könnte dabei zum Fehlschlagen der Entschlüsselung und anschließenden Decodierung führen.
Mit dem Ausschluss solcher Bytes von der Verschlüsselung wird einer Vergrößerung des Bildes vorgebeugt.

Im Anschluss an die Extraktion wird unter Verwendung eines geheimen Schlüssels eine zufällige Bitsequenz generiert.
Das kann mithilfe eines Verschlüsselungsverfahrens bewerkstelligt werden.
Dabei ist darauf zu achten, dass ein sicheres Verfahren verwendet wird.
Ein Beispiel dafür ist \gls{aes} im Counter"=Modus, wie es auch bei \gls{srtp} eingesetzt wird.
Die extrahierten Bits werden nachfolgend per XOR"=Operation mit der Bitsequenz verknüpft.
Diese Verarbeitung entspricht der Verschlüsselung mit einer Stromchiffre.
Anstelle einer solchen Chiffre könnte alternativ auch eine Blockchiffre mit einem Betriebsmodus verwendet werden, der die Nutzung als Stromchiffre möglich macht.
Zuletzt wird das verschlüsselte Bild erzeugt, indem die nicht verschlüsselten Daten mit dem verschlüsselten Bitstrom kombiniert werden.

Das vorgestellte Verschlüsselungsverfahren macht es auch möglich, nur Teile der zusätzlichen Bits zu verarbeiten.
Beispielsweise können nur die DC"=Koeffizienten verschlüsselt werden.
Dadurch würde die zu verschlüsselnde Datenmenge reduziert und die Ausführungszeit verringert werden.
Auf diese Weise kann die Echtzeitfähigkeit hergestellt oder begünstigt werden.

