\chapter{Optimierung einer Software zum Streaming mit RTP/RTSP}
\label{chap:optimierung}

An der \gls{htwdd} gibt es bereits ein Projekt, welches das Streaming mit \gls{rtsp} und \gls{rtp} realisiert.
Dieses Projekt "`RTSP-Streaming"' wird im Modul Internettechnologien II verwendet.
Bearbeitet wurde es unter anderem schon für die Implementierung des \gls{rtsp}-Protokolls \cite{pohl15} und für die Erweiterung mit der Möglichkeit der Fehlerkorrektur unter Nutzung von \gls{fec} \cite{zschorlich17}.
Das Projekt besteht aus einer Server"=Client"=Architektur.
Der Server liest Videodaten aus einer Datei ein und schickt sie mit \gls{rtp}-Paketen an den Client.
Dieser verarbeitet die Pakete und zeigt das Video in einer grafischen Anwendung an.

In der praktischen Verwendung stellte sich allerdings heraus, dass die Ausführung des Projektes auf schwächerer Hardware zu hoher Auslastung des Prozessors führt.
Als Grund dafür wurde vor allem das kostspielige Einlesen des Videos erkannt.
Vor der eingehenderen Betrachtung der Software wurde ein Ziel für die Optimierungen definiert.
Das aus Server und Client bestehende Projekt soll flüssig auf einem Raspberry Pi 3B laufen.
Darunter ist vor allem das unterbrechungsfreie Abspielen des Videos in der grafischen Anwendung zu verstehen.
Der Raspberry Pi ist ein Einplatinenrechner und einerseits weit verbreitet.
Andererseits besitzen die aktuellen Modelle ausreichend Leistung, um komplexere Aufgaben zu lösen.
In der Version 3B besitzt der Rechner einen Prozessor der ARMv8"=Architektur und 1 Gigabyte Arbeitsspeicher.
Aus Gründen der Kompatibilität läuft er allerdings als ARMv7 (\acrshort{vgl} \cite{rpi-os}).
Als Entwicklungsumgebung wird ein Laptop mit einem Prozessor des Modells Intel Core i5-4210M und 8 Gigabyte Arbeitsspeicher verwendet.
Diese dient als erstes Testszenario für die Modifikationen und wird auch zur Bewertung der Effektivität der Änderungen herangezogen.
Außerdem werden die Ausführungszeiten einzelner Methoden mit dem Profiler VisualVM in der Version 2.0.7 ermittelt.
Auf diese Weise können die für verschiedene Optimierungsversuche ermittelten Zeiten mit den Referenzzeiten des Projektes vor den Optimierungen verglichen und so der Erfolg der Änderung bewertet werden.
Die Referenzzeiten sind in Tabelle \ref{tab:opt-pre} dargestellt.

Ein weiterer Grund für die Verbesserung des Projektes liegt in der Kompatibilität mit dem VLC Media Player.
Bereits in ihrer Arbeit am "`RTSP-Streaming"' hat Zschorlich ermöglicht, das Programm als Alternative für den Server in der bestehenden Projektstruktur einzusetzen (\acrshort{vgl} \cite{zschorlich17}, \acrshort{seite} 100 \acrshort{fortfolgend}).
Dadurch konnte die Implementierung der Protokolle \gls{rtp} und \gls{rtsp} im Projekt einseitig validiert werden.
Aufgrund der Weiterentwicklung des VLC Media Players war die Kompatibilität des Projektes mit diesem nicht mehr gesichert.
Ziel der Optimierung war deshalb die Überprüfung der Kompatibilität mit dem VLC Media Player als Server sowie die Herstellung der Kompatibilität mit der externen Software als Client.

\vspace{1em}
Im Folgenden werden die Maßnahmen erläutert, mit denen die Prozessorauslastung des Projektes reduziert werden soll.
Außerdem wird jeweils reflektiert, wie erfolgreich die entsprechende Maßnahme zum Erreichen dieses Ziels war.
Zunächst wird dazu die Überarbeitung des Einlesens der Videodaten beschrieben.
Im Anschluss daran folgen Ausführungen zur Reduktion der Ausgaben auf der Kommandozeile.
Auch die seltenere Aktualisierung der Angaben in der grafischen Oberfläche des Clients finden dabei Erwähnung.
Ein weiterer Abschnitt befasst sich mit der Verringerung des Zeitintervalls für den Empfang der \gls{rtp}-Pakete.
Daraufhin wird beschrieben, welchen Einfluss die Ersetzung der Bildbereitstellung mittels \texttt{ImageIcon}s durch eine selbst entwickelte Zeichenfunktion auf die Reduktion der Prozessorauslastung des Clients hat.
Abschließend wird betrachtet, welche Maßnahmen zur Herstellung der Kompatibilität zwischen dem "`RTSP-Streaming"'"=Projekt und dem VLC Media Player erforderlich sind.


\section{Reduktion der Lesezugriffe auf die Videodatei}

Beim Einlesen des Videos aus der Datei müssen die einzelnen \gls{jpeg}"=Bilder erkannt und getrennt voneinander verarbeitet werden.
Der Grund dafür ist, dass in der \gls{rtp}"=Übertragung jedes Bild einzeln in ein oder mehreren Paketen übertragen wird.
Demzufolge müssen die aus der Datei gelesenen Daten auf \gls{m-soi}- und \gls{m-eoi}"=Marker durchsucht werden.
Innerhalb dieser Marker befinden sich die Kompressionsdaten eines \gls{jpeg}"=Bildes.

Eine Möglichkeit, diese Suche zu realisieren, ist das byteweise Lesen aus der Datei.
So können die Bytes sofort überprüft und \gls{jpeg}"=Marker schnell erkannt werden.
Ein weiterer Vorteil dieses Verfahrens besteht darin, dass sich die Leseposition der Datei nach der Erkennung eines Bildes direkt am Ende desselben befindet.
Es werden nicht zu viele Daten gelesen und die Suche nach dem nächsten Bild kann anschließend ohne Weiteres fortgesetzt werden.

Dieses byteweise Lesen verwendet auch das "`RTSP-Streaming"'"=Projekt.
Der größte Nachteil des Verfahrens ist aber der große rechentechnische Aufwand.
Das feingranulare Auslesen der Datei erzeugt sehr viele Anfragen an das Dateisystem.
Auch wenn diese mitunter durch das Betriebssystem gebündelt werden können, dauert die Bereitstellung der ausgelesenen Daten mehrere Prozessorzyklen.
Während dieser Zeit wartet der Prozessor auf die Beantwortung seiner Anfrage und kann keine anderen Berechnungen durchführen.
So wird Prozessorlast erzeugt, die keine Ergebnisse bringt.

Mittlerweile sind die meisten Computer auch in der Lage, größere Mengen an Daten auf einmal aus einer Datei zu lesen.
Deshalb können die Lesezugriffe auf die Videodatei reduziert werden, um eine geringere Prozessorlast zu bewirken.
Pro Anfrage an das Dateisystem können dann jeweils mehrere Bytes gelesen werden.
Die gelesenen Daten können anschließend auf die \gls{jpeg}"=Marker untersucht werden.
Besondere Beachtung gilt den Daten, die über den \gls{m-eoi}"=Marker hinaus gelesen wurden.
Diese müssen in einem Puffer zwischengespeichert und für die Suche nach dem nächsten Bild bereitgehalten werden.
Auf diese Weise müssen weder die Leseposition der Datei verschoben noch Daten mehrfach eingelesen werden.

Das Einlesen eines Datenblocks von mehreren Bytes und die Verwendung eines Pufferspeichers bewirkte eine deutlich verbesserte Leistung des Projektes auf dem Raspberry Pi.
Statt einer stark verlangsamten und verzögerten Wiedergabe des Videos erfolgte dieselbe nun vergleichsweise flüssig.
In Tabelle \ref{tab:opt-video} ist diese Verbesserung nicht eindeutig erkennbar.
Die für die ausgewählten Methoden benötigte Ausführungszeit auf dem Raspberry Pi hat sich insgesamt leicht verringert, was ein Anzeichen für den etwas weniger ausgelasteten Prozessor ist.
Am deutlichsten ist die Reduktion der Paketverluste.
Bei der Ausführung auf dem Laptop zeigten sich lediglich leichte Schwankungen, welche auf die Varianzen durch zeitgleich laufende Prozesse zurückzuführen sind.
Der erste Ansatzpunkt der Optimierung stellte sich somit als richtig heraus und brachte eine wesentliche Verbesserung.
Da die Videowiedergabe im Bezug auf die Originalgeschwindigkeit jedoch noch leicht verlangsamt lief, bestand weiterer Optimierungsbedarf.


\section{Reduktion der Programmausgaben}

Auch die Ausgaben eines Programmes haben einen wesentlichen Einfluss auf die Leistung desselben.
Dazu zählen neben den Anzeigen in einer grafischen Oberfläche auch Textausgaben auf der Kommandozeile.
Im Projekt "`RTSP-Streaming"' finden die Anzeige des Videos, der statistischen Werte zu empfangenen Paketen, des Abspielfortschritts und des Pufferfüllstandes der Videoframes in der grafischen Oberfläche des Clients statt.
Der Server besitzt eine solche Benutzeroberfläche für die Steuerung der Übertragungsparameter.
Dagegen erfolgen Ausgaben zu gesendeten und empfangenen \gls{rtsp}"=Paketen, fehlenden Medienpaketen sowie der Status der Fehlerkorrektur mittels \gls{fec} auf der Kommandozeile.
Beispielhafte Kommandozeilenausgaben von Server und Client sind in den Abbildungen \ref{fig:cli-out-server} \acrshort{bzw} \ref{fig:cli-out-client} dargestellt.
Auf die gleiche Weise werden serverseitig Headerdaten von \gls{rtp}"=Pakten in Binärform ausgegeben.

\begin{figure}[h]
    \centering
\begin{lstlisting}[style=packet,language=]
Send frame: 59 media
Frame size: 14395
Send frame: 60 media
FEC-Encoder ready...
FEC-Header
00000000 00000000 00000000 00111011 00000000 00000000 00001100 01001000 00000000 00100001
FEC-Level-Header
00000000 00000000 11000000 00000000
FEC-Payload
00000000 00000000 00000000
FEC packet: 13942 13942
Send frame: 60 fec
Frame size: 14569
\end{lstlisting}
    \caption[Beispiel der Konsolenausgaben des Servers]{
		Beispiel der Konsolenausgaben des Servers für zwei \acrshort{rtp}"= und ein dazugehöriges \acrshort{fec}"=Paket.
		Ausgegeben werden neben Paketnummer und -Länge auch die Binärdaten des \acrshort{fec}"=Headers.}
    \label{fig:cli-out-server}
\end{figure}

\begin{figure}[h]
    \centering
\begin{lstlisting}[style=packet,language=]
---------------- Receiver -----------------------
Got RTP packet with SeqNum # 60 TimeStamp: 180000 ms, of type 26 Size: 13923
FEC: set list: 29 [59, 60]
---------------- Receiver -----------------------
Got RTP packet with SeqNum # 29 TimeStamp: 180000 ms, of type 127 Size: 13968
----------------- Play timer --------------------
FEC: get RTP nu: 9
-> Get list of 1 RTPs with TS: 27000
Display TS: 27000 size: 18068
FEC: set media nr: 61
FEC: set sameTimestamps: 183000 [61]
\end{lstlisting}
    \caption[Beispiel der Konsolenausgaben des Clients]{
		Beispiel der Konsolenausgaben des Clients für den Empfang eines \acrshort{rtp}"= und eines \acrshort{fec}"=Paketes sowie das Abspielen eines Bildes.
		Dabei werden für jeden Schritt sehr detaillierte Informationen ausgegeben, zu denen unter anderem die Paketnummer und der Zeitstempel gehören.}
    \label{fig:cli-out-client}
\end{figure}

Bei den statistischen Werten handelt es sich um Angaben zu verlorengegangenen und korrigierten \gls{rtp}-Paketen.
Diese werden im Takt der Bildwiederholrate aktualisiert.
Da diese Rate als Konstante codiert ist, werden die Daten bei jeder Ausführung 25 Mal pro Sekunde aktualisiert.
Das ist deutlich häufiger, als einzelne Werte von Menschen erkannt werden können.
Denn bei einer Wiederholfrequenz von 25 Hertz werden aufeinanderfolgende Einzelbilder als Animation wahrgenommen.
Ähnliches gilt auch für sich ändernde Textangaben.
Ein weiterer Effekt ist, dass die Aktualisierungen in der grafischen Oberfläche sehr aufwendig sind.
Das spiegelt sich in der für die Änderung von Textanzeigen benötigten Ausführungszeit wider.
In der Praxis reichen für Texte wie die statistischen Angaben eine Aktualisierung von fünf Mal pro Sekunde aus.
Im konkreten Fall des Software"=Projektes lässt sich das realisieren, indem die Statistik nur aller fünf aktualisierten Bilder erneuert wird.

Weiteres Optimierungspotential liegt in den Informationen, die auf der Kommandozeile ausgegeben werden.
Auch wenn solche Ausgaben schneller bewerkstelligt werden als die in grafischen Oberflächen, benötigen sie doch deutlich mehr Ausführungszeit als normale Berechnungen.
Im Projekt werden neben wichtigen und für didaktische Zwecke relevante Angaben wie die \gls{rtsp}-Kommunikation auch Daten ausgegeben, die ausschließlich für das Debugging der Anwendungen benötigt werden.
Dazu zählen unter anderem die Headerdaten von Paketen (siehe Abbildung \ref{fig:cli-out-server}) sowie Angaben zu empfangenen Pakten und abgespielten Bildern (siehe Abbildung \ref{fig:cli-out-client}).
Eine Lösung für die Kategorisierung von Textausgaben ist die Verwendung eines Loggers.
Dieser kann Nachrichten von verschiedener Priorität ausgeben.
Ein Vorteil ist dabei, dass die gewünschte Priorität der Ausgaben konfigurierbar ist.
Sollen also nur Warnungen und Fehler ausgegeben werden, werden alle Nachrichten mit niedrigerer Priorität übersprungen.
Auf diese Weise können alle bisher bestehenden Ausgaben beibehalten werden.
Bei der Überarbeitung des Ausgabemechanismus wird ihnen jeweils eine Priorität zugeordnet.
Außerdem wird am Beginn der Anwendungen festgelegt, wie detailliert die Ausgaben erfolgen sollen.
Standardmäßig ist so eine geringe Menge an Ausgaben konfiguriert, welche Angaben zum Verkehr der \gls{rtsp}-Pakete, zu fehlenden Paketen und zur Korrigierbarkeit fehlender Pakete umfasst.
Soll jedoch eine Anwendung debuggt werden, können auch alle weiteren Daten ausgegeben werden.
Eine weitere Modifikation ist notwendig, um die Ausgaben in derselben Formatierung beizubehalten.
Die voreingestellte Formatierung des Loggers schließt den Zeitpunkt und die Priorität der Ausgabe mit ein.
Um das zu umgehen, muss der Logger in der Anwendung mit der entsprechenden Formatierungseinstellung konfiguriert werden.

Die Verwendung des Loggers inklusive der Prioritätseinstellung stellte sich als deutliche Verbesserung heraus.
Auf diese Weise war eine flüssige Wiedergabe des über \gls{rtp} empfangenen Videos möglich.
Sichtbar wird das in Tabelle \ref{tab:opt-logger}, welche im Vergleich zu \ref{tab:opt-pre} deutlich verringerte Ausführungszeiten zeigt.
Die Reduktion der Kommandozeilenausgaben zeigt sich dabei auch darin, dass die für diese Ausgaben genutzte Methode \texttt{println()} nicht mehr verzeichnet wird.
Ursächlich dafür ist die Verringerung dieser Ausgaben auf ein Minimum, wodurch die dafür benötigte Ausführungszeit vernachlässigbar wird.
Die Erhöhung der Zeit für die Methode \texttt{receive()} lässt sich damit erklären, dass als Standardverhalten auf weitere Pakete gewartet wird, sollten in diesem Moment keine weiteren Berechnungen erforderlich sein.
Auf dem Raspberry Pi zeigte sich allerdings auch eine Verzögerung beim Beginn der Wiedergabe, welche mit Paketverlusten im Wert von etwa zwei Sekunden Videodaten verbunden ist.
Ein weiterer Paketverlust von etwa einer halben Sekunde an Videodaten war kurz nach der ersten Verzögerung festzustellen.
Das restliche Video konnte allerdings ohne weitere Verluste abgespielt werden.
Im Versuch zeigte sich, dass weder die anfängliche Verzögerung noch die Paketverluste auftraten, wenn in ein und derselben Ausführung des Projektes zuvor schon ein anderes Video abgespielt wurde.
Diese Beobachtung legt nahe, dass es sich bei der Ursache der genannten Probleme um die Initialisierung von Strukturen handelt, welche am Beginn der ersten Übertragung stattfinden.
Abschließend sei hier noch erwähnt, dass es in der Entwicklungsumgebung des Laptops keine Probleme nach dieser Optimierung gab.


\section{Reduktion des Zeitintervalls für den Empfang der RTP-Pakete}

Um den anfänglichen Paketverlusten entgegenzuwirken, wurde die Implementierung des \gls{rtsp}-Projektes weiter analysiert.
Dabei fiel eine Diskrepanz in den Sende- und Empfangsraten der \gls{rtp}-Pakete bei Server und Client auf.
Der Server sendet die \gls{rtp}-Pakete in regelmäßigen Abständen, sodass ungefähr eine Rate von 25 Bildern in der Sekunde entsteht.
Nach jedem auf diese Weise gesendeten Paket wird außerdem überprüft, ob ein \gls{fec}-Paket zum Senden bereit ist.
Diese dienen der Korrektur von verloren gegangenen Paketen beim Empfänger.
Ist ein solches Korrekturpaket vorhanden, wird dieses gleich im Anschluss an das vorhergehende Medienpaket verschickt.
Damit ist es trotz der festen Intervalle beim Senden der \gls{rtp}-Pakete möglich, dass im Client in kurzer Zeit zwei unmittelbar aufeinanderfolgende Pakete empfangen werden müssen.
Für den Client entsteht daraus die Notwendigkeit, häufiger als die Senderate im Server auf ankommenden Pakete zu überprüfen.
Ein weiterer Grund für die häufige Abfragen neuer Pakete beim Empfänger ist die Kompatibilität zu anderen \gls{rtsp}-Servern, wie beispielsweise dem VLC Media Player.
Diese senden im Gegensatz zum Server des "`RTSP-Streaming"'"=Projektes mehrere Pakete pro Videoframe.
Ein Frame des Videos wird von diesen Sendern demnach auf mehrere \gls{rtp}-Pakete aufgeteilt, um die Größe der einzelnen Pakete zu reduzieren.
Es entsteht damit für den Client die Erfordernis, entweder häufiger empfangene Datenpakete abzufragen oder bei unveränderter Abrufrate jeweils mehrere Pakete pro Aufruf zu verarbeiten.
Im vorliegenden Projekt war die erste Möglichkeit der häufigeren Abfragen umgesetzt.

Dementsprechend ergab sich die Überlegung, ob der Paketverlust am Beginn der Übertragung durch eine andere Sende- und Empfangsstrategie verringert oder verhindert werden kann.
Deshalb wurde die folgende Strategie entworfen und versuchsweise umgesetzt.
Im Client sollte die Abrufrate auf das Doppelte der Bildwiederholrate des Videos verringert werden.
Damit die \gls{fec}-Pakete des Servers auch weiterhin empfangen werden können, musste dessen Sendeverhalten angepasst werden.
Das wurde durch eine Verdopplung der Senderate erreicht.
So war das alternierende Verschicken von Medien- und Korrekturpaketen möglich.
Ist für das Sendefenster eines \gls{fec}-Paketes kein solches bereit zum Versenden, wird der entsprechende Durchgang übersprungen.
Für die Medienpakete wird damit trotz der Alternation eine effektive Senderate von 25 Paketen pro Sekunde erreicht.

In den praktischen Versuchen zeigte sich sowohl für den Laptop als auch für den Raspberry Pi eine Reduktion der Ausführungszeit des Paketabrufes im Client auf etwa ein Zehntel des Wertes vor dieser Anpassung.
Erwartungsgemäß stieg die Prozessorauslastung des Servers leicht.
Im Vergleich zur Reduktion derselben im Client und zur Gesamtleistung ist das allerdings vernachlässigbar.
Weitaus wichtiger ist die Entwicklung im Bezug auf die Verzögerung des Abspielvorgangs und die Paketverluste.
Auch wenn die Prozessorauslastung über die gesamte Ausführungszeit im Mittel gesunken ist, wurde für den Raspberry Pi festgestellt, dass sich sowohl die Verzögerung als auch die Paketverluste am Beginn der Übertragung verstärkt haben.
Zu sehen ist das an der Anzahl verlorener Pakete in Tabelle \ref{tab:opt-rtp-timer}.
Für den Client in Verbindung mit dem Server des Projektes konnten die Ausführungszeiten stark reduziert werden.
Jedoch konnten Übertragungen vom VLC Media Player nicht mehr vollständig empfangen und angezeigt werden.
Die experimentelle Änderung der Sende- und Empfangsstrategie der \gls{rtp}-Pakete zeigte demnach keinen Erfolg.
Genau wie bei der Analyse vor dieser Änderung wurde beobachtet, dass diese Probleme nicht bei einem wiederholten Abspielen eines Videos in derselben Ausführung auftreten.
In einem weiteren Versuch wurde der Abspielvorgang im Bezug auf den Beginn der Übertragung der \gls{rtp}-Pakete verzögert.
Dabei war festzustellen, dass die Paketverluste direkt mit dem Beginn des Abspielens des Videos zusammenhängen.
Als Ursache dafür wurde die Konvertierung der Bilddaten in eine der grafischen Oberfläche eigene Struktur erkannt.
Diese trägt die Bezeichnung \texttt{ImageIcon} und wird für die Anzeige des Bildes in der grafischen Oberfläche genutzt.


\section{Verwendung einer eigenen Zeichenfunktion}

Alle grafischen Oberflächen des Projektes werden mit dem Framework Java Swing erzeugt.
In diesem werden Bilder üblicherweise als \texttt{ImageIcon} angezeigt.
Diese Struktur stellte sich in der vorigen Betrachtung als sehr kostspielig heraus.
Besonders die Konvertierung eines Bildes in die Struktur der grafischen Oberfläche besaß im Vergleich zu den anderen Verarbeitungsschritten der Bilddaten eine hohe Ausführungszeit.
Deshalb wurde die Visualisierung als \texttt{ImageIcon} durch eine eigene Funktion ersetzt.

Statt der Darstellung des konvertierten Bildes wird ein \texttt{JPanel} dargestellt.
Dabei handelt es sich um ein abstraktes Objekt des Frameworks, welches vor allem zur Gliederung grafischer Oberflächen verwendet wird.
Wie jedes Element einer solchen Oberfläche besitzt es eine \texttt{paint()}-Methode, welche das entsprechende Objekt an die gewünschte Position zeichnet.
Diese Methode wird immer dann aufgerufen, wenn sich der Inhalt des Objektes ändert und dieses deshalb neu gezeichnet werden muss.
Für konkrete Instanzen eines solchen Objektes kann die Implementierung der genannten Methode überschrieben werden.
In dem hier betrachteten Fall kann sie auf diese Weise genutzt werden, um das aktuelle Frame des Videos zu zeichnen.
Dafür müssen beim Aufruf zum Anzeigen des Frames alle 40 Millisekunden nur die Bilddaten als Membervariable zwischengespeichert und das erneute Zeichnen des \texttt{JPanel}s aufgerufen werden.

Bei der Untersuchung dieser Modifikation zeigte sich eine deutlich reduzierte Prozessorlast in der Entwicklungsumgebung des Laptops, dargestellt in Tabelle \ref{tab:opt-paint}.
Statt der Initialisierung eines \texttt{ImageIcon}s ist darin die eigens implementierte \texttt{paint()}"=Methode verzeichnet.
Dagegen hat sich die Leistung auf dem Raspberry Pi verschlechtert.
Die Anzeige des Videos war deutlich verzögert und verlangsamt, vergleichbar mit dem Stand vor den hier beschriebenen Änderungen zur Optimierung der Software.
Außerdem war eine deutlich erhöhte Zahl an Paketverlusten zu verzeichnen.
Zurückzuführen ist das auf die architektonischen Unterschiede der beiden Testrechner.
Während im Laptop Hardware zur Unterstützung individueller Zeichenfunktionen eingebaut ist, müssen diese Berechnungen auf dem Raspberry Pi in Software geschehen.
Demzufolge ist die Verwendung einer eigenen Zeichenfunktion nicht dafür geeignet, die Leistung des Projektes auf dem Raspberry Pi zu verbessern.


\section{Herstellung der Kompatibilität mit dem VLC Media Player}

Bei der im Projekt implementierten Softwarearchitektur für das \gls{rtsp}"=gesteuerte Streaming von Videos gibt es die beiden Rollen Server und Client.
Während der Server Videos einliest und als Pakete versendet, empfängt der Client die Pakete und verarbeitet diese.
Dementsprechend gibt es zwei Szenarien, wie die Implementierung des Projektes mithilfe des VLC Media Players validiert werden kann.
Zum einen kann das besagte Programm als Server fungieren, dessen Datenübertragung vom Client des "`RTSP-Streaming"'"=Projektes empfangen wird.
Zum anderen kann diese Zuständigkeit umgekehrt werden, wobei der Server des Projektes Pakete an den VLC Media Player als Client sendet.

Im Rahmen der Überprüfung der Kompatibilität konnte festgestellt werden, dass der Client keine Implementierungsfehler enthält, die sich auf den Empfang und die Darstellung von \gls{rtp}- und \gls{rtsp}"=Paketen auswirken.
Diese Erkenntnis basiert auf dem Versuch, bei welchem der VLC Media Player als Server eingesetzt und der Client des "`RTSP-Streaming"'"=Projektes verwendet wurde.
Bei diesem Versuchsaufbau wurde das Video fehlerfrei übertragen.

Zu einem anderen Ergebnis kam es bei dem Einsatz des VLC Media Players als Client, dem der Server des Projektes Pakete zusendete.
Zunächst wurde die \gls{rtsp}"=Verbindung vom Empfänger nicht angenommen.
Um die Ursache dafür zu ermitteln, wurde die \gls{rtsp}"=Verbindung zwischen zwei Instanzen des VLC Media Players als Server"=Client"=Architektur mit den Rückmeldungen des Servers des "`RTSP-Streaming"'"=Projektes verglichen.
Dazu wurde die Netzwerkübertragung zweier Instanzen des VLC Media Players mit dem Programm Wireshark aufgezeichnet und analysiert.
Dabei stellte sich heraus, dass der VLC Media Player in der Funktion als Client auf die \gls{rtsp}"=Anfrage \texttt{SETUP} eine Antwort mit bestimmten Parametern erwartet.
Während der Server im "`RTSP-Streaming"' bereits die meisten Parameter in seine Antwort integriert, informiert er nicht über die Ports, welche er für die \gls{rtp}"=Verbindung verwendet.
Diese Angabe ist Teil des Feldes \texttt{Transport:} und wird mit dem Schlüsselbegriff \texttt{server\_port} angegeben (\acrshort{vgl} \cite{rfc2326}, \acrshort{seite} 61).
Wird diese Information in die Antwort des Servers integriert, akzeptiert der VLC Media Player die \gls{rtsp}"=Verbindung.
Es soll hier noch einmal betont werden, dass es sich bei dem verwendeten VLC Media Player um die Version 3.0.9.2 handelt.
Das ist insbesondere von Bedeutung, da dieser das \gls{rtsp}"=Protokoll in der ersten Version verwendet.
Mittlerweile existiert seit einigen Jahren die Version 2.0 des Protokolles (\acrshort{vgl} \cite{rfc7826}).
In dieser werden die Schlüsselbegriffe \texttt{client\_port} und \texttt{server\_port} durch \texttt{src\_addr} und \texttt{dest\_addr} ersetzt.
Bei einer zukünftigen Änderung des VLC Media Players auf die neue Version des \gls{rtsp} ist deshalb eine Überarbeitung der betrachteten Angaben notwendig.

Auch im Anschluss an die Ergänzung der Angaben zum \gls{rtp}"=Port am Server konnte der VLC Media Player die Videoübertragung nicht abspielen.
Da in der Logausgabe des Programmes keine Fehler verzeichnet waren, jedoch gemäß der Aufzeichnungen von Wireshark Pakete verschickt wurden, wurde ein weiteres Programm zum Erproben der Übertragung hinzugezogen.
Dabei handelte es sich um den LIVE555 Media Server in der Version 0.99, dessen Bibliotheken auch intern vom VLC Media Player für \gls{rtsp}"=Übertragungen verwendet werden.
In dessen Debugausgaben wurde die Fehlermeldung generiert, dass die empfangenen Medienpakete zum Zeitpunkt des Empfangs zu alt waren.
Als Konsequenz daraus war der Fehler in der Implementierung bei der Zeitangabe der Pakete zu suchen.
Eine erneute Betrachtung des RFC 2435 \cite{rfc2435} ergab, dass die Zeitangabe von \gls{jpeg}"=Paketen in der Einheit 90000Hz angegeben sein müssen.
Außerdem muss in jedem letzten Paket eines Frames das Markerbit im \gls{rtp}"=Header gesetzt werden.
Beides war im Server des "`RTSP-Streaming"'"=Projektes nicht der Fall.
Die Behebung beider Fehler ermöglichten schließlich die Verbindung und das Abspielen dem Server des Projektes und dem VLC Media Player als Client.

