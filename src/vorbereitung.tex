\chapter{Vorbereitung der Verschlüsselung von Mediendaten}

Neben der Codierung und echtzeitfähigen Übertragung von Mediendaten spielt auch die Verschlüsselung derselben eine wichtige Rolle.
Während eine solche Sicherung bei der Anwendung auf dem eigenen Computer oder in einem lokalen Netzwerk nicht sehr wichtig scheint, ist die Verschlüsselung bei der Übertragung zu Computern außerhalb des eigenen Netzwerkes von umso größerer Bedeutung.
Nicht nur werden damit die Daten vor dem Zugriff Dritter geschützt.
Auch der Inhalt der Übertragung \acrshort{bzw} das im Video Gezeigte bleibt so vertraulich.
Für kommerzielle Dienste ist die Verschlüsselung ebenfalls eine wichtige Technologie.
Beispielsweise können Streamingdienste auf diese Weise ausschließlich registrierten Nutzern Zugriff auf ihre meist kostenpflichtigen Dienste geben.

Das bereits in Kapitel \ref{chap:optimierung} vorgestellte Projekt "`RTSP-Streaming"' implementiert neben der Steuerung einer \gls{rtp}"=Übertragung mit dem namensgebenden Protokoll auch ein Verfahren zur Fehlerkorrektur verlorengegangener Pakete mittels \gls{fec}.
Eine Verschlüsselung der übertragenen Daten wurde jedoch noch nicht realisiert.
Im Falle einer Übertragung eines Videostreams auf einen entfernten Computer geschieht diese demnach im Klartext, sodass jeder am Transport der Daten beteiligte Rechner das Video ohne Weiteres dekodieren und abspielen kann.

Die Implementierung einer Verschlüsselung im Projekt erhöht dessen Sicherheit bei der Übertragung.
Außerdem kann die Verschlüsselung so in dem bestehenden Projekt praktisch erprobt und angewendet werden.
Zuletzt besteht dadurch auch die Möglichkeit, die Verschlüsselungen im Zusammenspiel mit der bereits implementierten Fehlerkorrektur zu erproben.
Die Nutzung der Verschlüsselung im Projekt soll allerdings wählbar bleiben.
So können Nutzer individuell entscheiden, ob unverschlüsselt oder durch ein Verschlüsselungsverfahren geschützt übertragen werden soll.

Nachfolgend sollen zunächst die Anforderungen an eine solche Verschlüsselung unter Echtzeitbedingungen analysiert werden.
Als Szenario wird dabei angenommen, dass der Inhalt des übertragenen Videos nicht von Dritten angesehen werden kann.
Im Anschluss daran wird das bestehende Projekt analysiert, um eine geeignete Struktur für die Implementierung von Verschlüsselungsverfahren zu entwerfen.
Die daraus folgende Restrukturierung wird zum Ende dieses Kapitels erläutert.


\section{Anforderungen an die Verschlüsselung}
\label{sec:enc-requirements}

Je nach Anwendungsszenario ergeben sich unterschiedliche Anforderungen an die Verschlüsselung der Daten.
Das Ziel ist dabei vor allem der geeignete Einsatz eines Verschlüsselungsverfahrens.
Deshalb werden häufig Kryptosysteme genutzt, die schon vielfach getestet wurden und noch nicht gebrochen werden konnten.
Die Erstellung eines eigenen Verschlüsselungsverfahrens ist bei der Anwendung von Verschlüsselung in einer Software nicht das Ziel.

Allen Szenarien, in denen Daten verschlüsselt werden sollen, ist das Erfordernis der kryptografischen Sicherheit gemein.
Darunter ist zu verstehen, dass das verwendete Verschlüsselungsverfahren zum Zeitpunkt der Verwendung nicht gebrochen ist und als sicher gilt.
Beispielsweise ist der \gls{des} ein gebrochenes Verfahren, welches im Jahr 2005 zurückgezogen wurde (\acrshort{vgl} \cite{des-withdrawal}) und deshalb nicht mehr verwendet werden sollte.

Eine weitere Anforderung, besonders im Bereich von Medien, ist die Sicherheit im Bezug auf die Wahrnehmung (\acrshort{engl} \textit{perceptive security}).
Gemeint ist damit, dass der Inhalt von Bildern oder Videos nach der Verschlüsselung nicht mehr erkennbar sein soll.
Ein anschauliches Negativbeispiel dafür ist der Betriebsmodus \gls{ecb} für Blockchiffren.
Werden unkomprimierte Bilddaten auf diese Weise verschlüsselt, lassen sich immer noch die Konturen derselben erkennen.
Ein Beispiel dafür zeigt Abbildung \ref{fig:tux-ecb}.
Hervorgerufen wird dieser Effekt durch die Tatsache, dass inhaltlich gleiche Blöcke der Originaldaten auf identische Blöcke an verschlüsselten Daten abgebildet werden.
Beispielsweise werden alle Bildblöcke des weißen Hintergrundes beim Pinguin Tux in den verschlüsselten Daten durch eine schräge Schraffierung dargestellt.
Dieser Effekt trifft in äquivalenter Weise, wenngleich weniger anschaulich, auch auf komprimierte Bild-, Video- oder allgemein andere Daten zu.

\begin{figure}[h]
    \centering
    \hfill
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{res/img/Tux.jpg}
        \caption{Originalbild vom Pinguin Tux, dem Linux-Maskottchen}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{res/img/Tux_ecb.jpg}
        \caption{Mit \gls{ecb}-Modus verschlüsseltes Originalbild}
    \end{subfigure}
    \hfill
    \caption[Vergleich des Originalbildes mit dessen \acrshort{ecb}-verschlüsselter Variante.]{
        Vergleich des Originalbildes mit dessen \acrshort{ecb}-verschlüsselter Variante.
        (Originalbild von Larry Ewing \href{mailto:lewing@isc.tamu.edu}{lewing@isc.tamu.edu} mit \href{https://www.gimp.org}{GIMP} erstellt;
        verschlüsseltes Bild von Wikipedia"=Nutzer Lunkwill erstellt auf Basis des Originalbildes, Adresse: \href{https://commons.wikimedia.org/wiki/File:Tux\_ecb.jpg}{https://commons.wikimedia.org/wiki/File:Tux\_ecb.jpg} (besucht am 02.07.2021)}
    \label{fig:tux-ecb}
\end{figure}

Im Kontext der Anwendung der Verschlüsselung bei der Übertragung eines Videos per \gls{rtsp} und \gls{rtp} erhält die Echtzeitfähigkeit des Verfahrens eine besondere Bedeutung.
Um die Ausführungszeit der Verschlüsselung gering zu halten \acrshort{bzw} bei Bedarf zu reduzieren, gibt es zwei Ansatzpunkte (\acrshort{vgl} \cite{lian09} \acrshort{seite} 15).
Zum einen kann die Menge der zu verschlüsselnden Daten reduziert werden.
Erreicht werden kann das, indem nur wichtige Teile der Daten verschlüsselt werden.
Im Fall von Bilddaten können das beispielsweise die zur Decodierung benötigten Tabellen, aber auch die Kompressionswerte der mittleren Helligkeit und der Grundstrukturen sein.
Damit können die Mediendaten nicht von Dritten abgespielt oder angezeigt werden und die Verschlüsselung ist schneller fertiggestellt.
Die andere Variante besteht in der Verwendung eines im Bezug auf die Rechenkomplexität leichtgewichtigen Verschlüsselungsverfahrens.
Eine geringere Komplexität der Berechnungen resultiert in einer schnelleren Ausführungszeit.
Das ist der Grund dafür, dass für die Anwendung von Verschlüsselung in solchen Szenarien hauptsächlich symmetrische Verfahren genutzt werden.
Denn die meisten symmetrischen Verfahren "`können einige hundert- bis tausendmal schneller verschlüsseln als asymmetrische Algorithmen"' (\cite{paar16}, \acrshort{seite} 179), da letztgenannte besonders viele Berechnungen benötigen.
Während die Vorteile asymmetrischer Verschlüsselungsverfahren beispielsweise beim Schlüsselaustausch liegen, sind sie für die Verschlüsselung von großen Datenmengen in begrenzter Zeit weniger geeignet.

Auch die Beibehaltung des Datenformates ist eine Anforderung mancher Anwendungen.
In einigen Fällen ist es erforderlich, dass die Formatinformationen der zu verschlüsselnden Daten beibehalten werden.
Das dient vor allem dazu, die korrekte Verarbeitung der Daten zu gewährleisten.
Konkret bedeutet das beispielsweise, dass eine auf Paketebene operierende Verschlüsselung die erforderliche Paketstruktur beibehält.
Ohne die Einhaltung dieses Formates können Empfänger den Typ des Paketes nicht richtig erkennen und dementsprechend die enthaltenen Daten nicht richtig verarbeiten.
Übertragen auf eine auf der Bildebene operierende Verschlüsselung müssen die Formatinformationen des Bildes beibehalten werden, um dieser Anforderung zu entsprechen.
Für den Anwendungsfall von \gls{jpeg}"=Bildern sind die Marker zu berücksichtigen.
Umgekehrt bedeutet das im Kontext der Decodierung \acrshort{bzw} Entschlüsselung aber auch, dass durch die Verschlüsselung keine Daten erzeugt werden, die fälschlicherweise für Formatinformationen gehalten werden können.
Wird das beim Entwurf der Verschlüsselung nicht beachtet, resultiert daraus ein Fehlschlagen der Decodierung.
Das kann soweit führen, dass ein verschlüsseltes Medium nicht wieder entschlüsselt werden kann.
In einem solchen Fall führen durch die Verschlüsselung erzeugten Formatinformationen zu Fehlentscheidungen im Entschlüsselungsalgorithmus.

Neben den genannten Anforderungen für den Einsatz von Verschlüsselung existierten noch weitere (\acrshort{vgl} \cite{lian09}).
Diese hängen ebenfalls stark vom konkreten Anwendungsszenario ab.
Ein Beispiel dafür ist die Eignung des Verfahrens für die Kompression, wenn die Verschlüsselung vor oder während der Kompression stattfinden soll.
Auf diese soll hier jedoch nicht näher eingegangen werden.


\section{Analyse des bestehenden Projektes}

Das Projekt "`RTSP-Streaming"' ist in seiner Implementierung in zwei Teile getrennt.
Zum einen ist ein Server implementiert, welcher die Videodaten einliest und diese überträgt.
Zum anderen empfängt ein Client die Datenpakete und verarbeitet sie, um diese in einer grafischen Anwendung anzeigen zu können.
Trotz der beiden Teile ist ein Großteil der Funktionalitäten vereint.
So übernimmt beispielsweise der \texttt{FecHandler} die Verarbeitung aller \gls{rtp}"=Pakete.
Dazu zählt sowohl die Bereitstellung von \gls{fec}"=Paketen im Server als auch die Korrektur fehlender Pakete im Client.
Ein anderes Beispiel dafür ist die Klasse \texttt{JpegFrame}, welche sowohl \gls{jpeg}"=Bilder in \gls{rtp}"=Pakete transformiert als auch die umgekehrte Operation bereitstellt.

Bei detaillierterer Betrachtung fällt zunächst auf, dass beide Hauptklassen \texttt{Server} und \texttt{Client} Programmlogik enthalten, während sie gleichzeitig die grafische Oberfläche definieren und deren Funktionalität implementieren.
Dieses Prinzip ist für kleinere Projekte praktikabel.
Je umfangreicher allerdings ein Projekt wird, desto wichtiger ist eine klare Trennung der Zuständigkeiten.
Diese ist vor allem notwendig, um die Übersichtlichkeit zu wahren.
Ein gängiges Vorgehen für größere Projekte ist deshalb, die grafische Darstellung von der Programmlogik zu trennen.
Zur Darstellung gehören dabei neben grafischen Oberflächen auch Kommandozeilenoberflächen.
Durch die Trennung werden die beiden angesprochenen Komponenten einer Anwendung an verschiedenen Stellen implementiert, was vor allem die einzelnen Dateien übersichtlicher macht.
Außerdem entsteht dabei zwangsläufig eine Schnittstelle der Anzeige zur Programmlogik.
Wird diese bewusst entworfen und strukturiert, sorgen einzelne Funktionsaufrufe der Logikkomponente für einen durchschaubaren und leicht nachvollziehbaren Programmablauf.

Eine weitere Feststellung lässt sich in Betracht der Klassenstruktur treffen, die in Abbildung \ref{fig:classes-prev} visualisiert ist.
Die beiden Klassen \texttt{Server} und \texttt{Client} greifen auf die Funktionalität vieler anderer Klassen zu, die jeweils spezielle Aufgaben erfüllen.
Diese auffällig flache Hierarchie weist darauf hin, dass ein Großteil der Programmlogik in den beiden Hauptklassen zu finden ist.
Um diese Logik gewährleisten zu können, müssen sie auf viele andere Klassen zugreifen.
Diese Struktur ist nicht unüblich und in vielen Softwareprojekten zu finden.
In dem hier betrachteten Projekt verdeutlicht sie jedoch die mangelnde Struktur in der Implementierung.
Sowohl die \gls{rtsp}"=Kommunikation als auch die \gls{rtp}"=Übertragung laufen in den beiden Hauptklassen ab.
Damit werden die Implementierungen des \gls{rtsp} und des \gls{rtp} vermischt.
So existiert für die beiden Protokolle keine Stelle, an der sie eindeutig und getrennt von anderer Funktionalität implementiert sind.
Dazu kommt die im vorigen Absatz angesprochene Realisierung der grafischen Oberfläche in den besagten Klassen.
Zusammengefasst verringert das die Übersichtlichkeit des Quellcodes in den angesprochenen Klassen deutlich.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/classes-prev.png}
    \caption{Bestehende Klassenstruktur des Projektes "`RTSP-Streaming"'}
    \label{fig:classes-prev}
\end{figure}

Eine weitere strukturelle Schwachstelle findet sich in der Klasse \texttt{FecHandler}, welche die Fehlerkorrektur implementiert.
Wie bereits erwähnt wird in dieser Klasse die Funktionalität für die Bereitstellung von \gls{fec}"=Paketen und die Fehlerkorrektur implementiert.
Der Server prüft so selbstständig auf das Vorhandensein eines Korrekturpaketes und versendet dieses bei Bedarf.
Dagegen wird der funktionelle Umfang der Klasse auf der Empfängerseite erweitert.
Statt einer Bereitstellung von Paketen, die verloren gegangen sind oder bei der Übertragung beschädigt wurden, werden alle \gls{rtp}"=Pakete durch diese Klasse verwaltet.
Unabhängig vom Fehlerstatus eines Paketes erfragt der Client so alle Medienpakete vom \texttt{FecHandler}.
Zurückzuführen ist diese Entwicklungsentscheidung vermutlich auf die schrittweise Erweiterung des Projektes.
Für die Implementierung der Fehlerkorrektur per \gls{fec} im Projekt erwies sich die Verwaltung der \gls{rtp}"=Pakete auf Empfängerseite als einfach zu realisieren.
Der Einfluss dieser Struktur auf die Erweiterbarkeit des Projektes wurde dabei nicht in die Entscheidung einbezogen.

Für die Realisierung einer Verschlüsselung für die Übertragung erweist sich diese Struktur als hinderlich.
Auf Paketebene agierende Verschlüsselungen, die als Schnittstelle zwischen Anwendung und Übertragung operieren, verarbeiten alle zu übermittelnden oder empfangenen \gls{rtp}"=Pakete.
Eine klare Zuständigkeit für diese Funktionalität und damit verbunden auch eine eindeutige Stelle ihrer Implementierung existiert im bestehenden Projekt nicht.
Die Erfüllung dieser Anforderung ist somit im Kontext der bestehenden Struktur schwierig.
Ähnlich verhält es sich mit Verschlüsselungen, die auf der Bildebene arbeiten und so zwischen Bildbereitstellung und Paketverarbeitung \acrshort{bzw} für den Empfänger zwischen Paketverarbeitung und Bildanzeige stattfinden müssen.
Auch für diese Verarbeitung existiert keine Klasse im Projekt, in deren direkte Zuständigkeit sie fällt.
Eine Erweiterung des Projektes durch die Option der Verschlüsselung ohne eine Umstrukturierung im Bereich der Paketverarbeitung würde damit die Übersichtlichkeit des Quellcodes weiter reduzieren.
Außerdem würde die Anwendung der Verschlüsselung wiederum in den Klassen \texttt{Server} und \texttt{Client} stattfinden.
Statt einer klaren Schnittstelle zwischen den Anwendungen und der Implementierung des \gls{rtp}"=Protokolls entstünden damit zwei noch deutlicher mit Funktionalität überladene Hauptklassen.
Eine Restrukturierung ist deshalb, zumindest für die Verarbeitung von \gls{rtp}"=Paketen, unumgänglich.


\section{Entwurf einer Neustrukturierung des Projektes}

Für die Neustrukturierung des Projektes bedarf es eines ausführlichen Entwurfes.
Dabei ist auf eine klare Trennung der Definition grafischer Oberflächen und der Programmlogik zu achten.
Außerdem sollten Schnittstellen entworfen werden, welche die Zuständigkeiten der Klassen für bestimmte Aufgabenbereich klar regeln.
Nachfolgend soll der Entwurf der besagten Neustrukturierung grob skizziert werden.

Zunächst kann die Implementierung der grafischen Oberflächen von der Funktionalität der Programme getrennt werden.
Dies ist mit der Erstellung der Klassen \texttt{ServerGui} und \texttt{ClientGui} möglich.
Beide Klassen stellen ausschließlich die für die Darstellung der Oberflächen benötigten Berechnungen an.
Benötigte Informationen, wie beispielsweise das anzuzeigende Bild oder die Empfangsstatistik, sind bei den Logikklassen \texttt{Server} und \texttt{Client} zu erfragen.
Ähnlich verhält es sich mit Nutzereingaben.
Diese werden ohne weitere Verarbeitung an die besagten Klassen weitergegeben.
Auf diese Weise bleiben \texttt{ServerGui} und \texttt{ClientGui} ausschließlich für die Darstellung der grafischen Oberflächen verantwortlich.

Die bereits im Projekt bestehenden Klassen \texttt{Server} und \texttt{Client} bleiben erhalten und stellen die Schnittstelle der grafischen Oberfläche zur Programmfunktionalität dar.
Grafisch dargestellt ist das in Abbildung \ref{fig:classes-complete-refactoring}.
Allerdings wird die Funktionalität beider Klassen auf die Ausführung der übergeordneten Logik reduziert.
Dazu gehören das Senden und Empfangen von Netzwerkpaketen und die Delegation der Abarbeitung und Beantwortung von Nachrichten an untergeordnete Klassen.
Außerdem bleibt der Server weiterhin dafür verantwortlich, das Auslesen von Videodaten für das Versenden aufzurufen.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/classes-complete-refactoring.png}
    \caption[Klassenstruktur des Projektes nach einer Neustrukturierung.]{
        Klassenstruktur des Projektes nach einer Neustrukturierung.
        Der Aufruf der Programme geschieht über die Klassen der grafischen Oberflächen.
        Getrennt davon sind Server"= und Clientfunktionalität, welche wiederum an Unterklassen delegieren.}
    \label{fig:classes-complete-refactoring}
\end{figure}

Für das Auslesen von Bildern aus der geforderten Videodatei sowie das Extrahieren der entsprechenden Metadaten wird die Klasse \texttt{VideoHandler} verantwortlich.
Diese kapselt jeglichen Zugriff auf Videodateien für den Server.
Die Extraktion von Metadaten geschieht dabei über die bereits bestehenden Klassen \texttt{AviMetadataParser} und \texttt{QuickTimeMetadataParser}.
Das Auslesen von Bildern übernimmt weiterhin der \texttt{VideoReader}.
Dieser benötigt für das Parsen von \gls{jpeg}"=Bildern Informationen über deren Aufbau.
In der aktuellen Implementierung sind diese in der Klasse \texttt{JpegFrame} enthalten.
Diese realisiert jedoch außerdem die Verarbeitung von \glspl{jpeg} zu \gls{rtp}"=Paketen, worauf der \texttt{VideoReader} keinen Zugriff benötigt.
Deshalb werden alle \gls{jpeg}"=spezifischen Konstanten in eine neue Klasse \texttt{JpegConstants} ausgelagert, die in ihrer Funktionalität den Headerdateien der Programmiersprachen C und C++ ähnlich sind.
Die Verarbeitung der \gls{jpeg}"=Bilder übernimmt weiterhin die Klasse \texttt{JpegFrame}, die jedoch aufgrund der passenderen Bezeichnung zu \texttt{JpegHandler} umbenannt wird.

Da die Klassen \texttt{Server} und \texttt{Client} zwar Netzwerkpakete empfangen, diese aber nicht verarbeiten sollen, werden die Implementierungen der Protokolle \gls{rtp} und \gls{rtsp} ausgelagert.
Für die \gls{rtsp}"=Funktionalität entsteht dabei die Klasse \texttt{RtspHandler}.
Ihre Aufgaben umfassen die Bewerkstelligung des gesamten \gls{rtsp}"=Verkehrs.
Für den Client entspricht das dem Versenden von Anfragen sowie dem Parsen der erhaltenen Antworten.
Umgekehrt implementiert die Klasse für den Server das Parsen von Anfragen und deren Beantwortung.
Auch die entsprechende Behandlung von Nachrichten sowie das Weitergeben der erhaltenen Informationen liegt in der Verantwortung der Klasse \texttt{RtspHandler}.
Die Auslagerung der \gls{rtp}"=Funktionalität geschieht auf ähnliche Weise.
Sie wird im nächsten Abschnitt genauer besprochen.


\section{Restrukturierung der Verarbeitung von RTP-Paketen}
\label{sec:refactoring}

Die Umsetzung der Neustrukturierung des gesamten Projektes entsprechend des Entwurfs des vorigen Abschnittes ist aus verschiedenen Gründen nicht möglich.
Zum einen stellt sie selbst eine komplexe und umfangreiche Aufgabe dar.
Neben der Implementierung der geplanten Änderungen gehört auch eine detailliertere Ausarbeitung des Entwurfes dazu.
Zum anderen dient die Umstrukturierung in diesem Kontext ausschließlich dazu, die Implementierung von Verschlüsselungen für die Übertragung von Mediendaten zu ermöglichen und zu vereinfachen.
Deshalb wird an dieser Stelle ausschließlich die strukturelle Verbesserung für die Verarbeitung von \gls{rtp}"=Paketen vorgenommen.

Startpunkt einer verbesserten Klassenstruktur bildet die Auslagerung aller Funktionalitäten des \gls{rtp} in eine neue Klasse, die als \texttt{RtpHandler} bezeichnet wird.
Die Eingliederung dieser Klasse im Projekt sowie die von ihr bereitgestellten Schnittstellen sind in Abbildung \ref{fig:classes-after} visualisiert.
Wie im vorigen Abschnitt bereits festgestellt wurde, findet serverseitig die meiste Programmlogik in Verbindung mit dem \gls{rtp} in der Klasse \texttt{Server} statt.
Das Auslesen neuer Videodaten wird dabei vom Server selbst gesteuert.
Dasselbe trifft auf die Verarbeitung der einzelnen Bilder zu \gls{rtp}"=Paketen zu, wofür die Klasse \texttt{JpegFrame} benötigt wird.
Der \texttt{FecHandler} stellt dabei ausschließlich die Korrekturpakete bereit, welche nach ihrer Fertigstellung versendet werden.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{res/graph/classes-after.png}
    \caption[Vereinfachte Darstellung der neuen Klassenstruktur mit \texttt{RtpHandler}]{
        Vereinfachte Darstellung der neuen Klassenstruktur nach der Verschiebung der \acrshort{rtp}"=Funktionalität in eine separate Klasse.
    Für diese Klasse \texttt{RtpHandler} sind außerdem die Methoden dargestellt, welche die Schnittstelle für Server und Client definieren.
    Neben der klaren Schnittstelle zur Implementierung des \acrshort{rtp}"=Protokolls ist auch ein vereinfachter Mechanismus zum Abruf der Empfangsstatistik realisiert.}
    \label{fig:classes-after}
\end{figure}

Da ausschließlich die \gls{rtp}"=Verarbeitung umstrukturiert werden soll, ruft der Server auch nach der Einführung des \texttt{RtpHandler}s weiterhin selbst neue Videodaten vom \texttt{VideoReader} ab.
Für die weitere Verarbeitung der Bilder werden diese jedoch direkt per \texttt{jpegToRtpPacket()} an den \texttt{RtpHandler} übergeben.
Dieser sorgt für die Umformatierung der Daten zu \gls{rtp}"=Paketen.
Außerdem wird das erzeugte Paket bei aktivierter \gls{fec}"=Funktion an den \texttt{FecHandler} weitergegeben, ohne dass der Server davon Kenntnis nimmt.
Nach dem Erhalt des angeforderten Paketes und dem Versenden desselben kann der Server mit \texttt{isFecAvailable()} beim \texttt{RtpHandler} ein \gls{fec}"=Paket erfragen.
Gibt der Aufruf den Wert \texttt{true} zurück, holt sich der Server per \texttt{createFecPacket()} das entsprechende Paket und versendet dieses.
Alternativ dazu ist auch der direkte Aufruf der \gls{fec}"=Paket"=erstellenden Methode möglich.
Diese gibt den Wert \texttt{null} zurück, sollte kein solches Paket verfügbar sein.

Mit dieser neuen Struktur reduziert sich die Zuständigkeit des Servers auf den alleinigen Abruf von \gls{rtp}- und \gls{fec}"=Paketen beim neu erstellen Handler.
Jegliche Vorverarbeitung der Pakete findet damit direkt im \texttt{RtpHandler} statt.
Weitere wichtige Funktionalitäten bleiben jedoch im Server erhalten \acrshort{bzw} werden erst nach Eingaben in der grafischen Oberfläche ausgelöst.
Dabei handelt es sich einerseits um die Veränderung der Gruppengröße des \gls{fec}"=Fehlerschutzes, welche dem Nutzer als Slider in der Oberfläche des Servers zugänglich gemacht ist.
Eine Änderung des damit assoziierten Wertes hat den Aufruf der Methode \texttt{setFecGroupSize()} beim \texttt{RtpHandler} zufolge, welche dieser wiederum an den \texttt{FecHandler} weiterleitet.
Andererseits wird die Häufigkeit, mit der Videodaten gelesen und Bilder verschickt werden, weiterhin vom Server festgelegt.
Dieser erhält die Information entweder aus der Videodatei oder aus einem gesetzten Standardwert und initialisiert damit den in der grafischen Oberfläche implementierten Timer.
Ausschließlich die periodisch erzeugten Timerereignisse sorgen für die Verarbeitung von Daten im \texttt{RtpHandler}.
Somit stellt die letztgenannte Klasse zwar Funktionalität bereit, deren Aufruf erfolgt aber nur über den steuernden Server.

Die im Bezug auf die Strukturierung der Funktionalität größere Veränderung ist auf der Clientseite zu finden.
Dabei gibt es mit der Verarbeitung von \gls{rtp}"=Paketen und dem Abruf von \gls{jpeg}"=Bildern aus dem Speicher zur Anzeige des Videos zwei Schwerpunkte, in denen sich die Zuständigkeiten ändern.
Auch mit der Auslagerung von Funktionalität in den \texttt{RtpHandler} steuert der Client den Empfang von Datenpaketen selbst.
Allerdings werden die Pakete nun der neu eingeführten Klasse per \texttt{processRtpPacket()} übergeben.
Dieser Aufruf gilt auch für \gls{fec}"=Pakete, da diese der Grundstruktur von \gls{rtp}"=Paketen folgen.
Daraufhin ist es die Aufgabe des \texttt{RtpHandler}s, empfangene Pakete anhand ihres Inhaltes zu unterscheiden und entsprechend zu verarbeiten.
Der Ablauf dieser Verarbeitung hat sich im Gegensatz zu ihrer Zuständigkeit durch die Restrukturierung nicht geändert.
Handelt es sich bei dem empfangenen Paket um ein \gls{rtp}"=Paket mit \gls{jpeg}"=Inhalt, wird dieses in einer Datenstruktur gespeichert.
Außerdem wird in Listen festgehalten, welche Medienpakete zusammen ein \gls{jpeg}"=Bild bilden.
Für die Verarbeitung von \gls{fec}"=Paketen ist auch weiterhin der \texttt{FecHandler} zuständig.

Der Abruf von Bildern zur Anzeige des Videos hat sich durch die Einführung der Klasse \texttt{RtpHandler} vereinfacht.
Statt eine Liste von \gls{rtp}"=Paketen vom \texttt{FecHandler} zu erfragen, um diese selbst weiterzuverarbeiten, muss der Client ausschließlich die Methode \texttt{nextPlaybackImage()} aufrufen.
Mit diesem Aufruf stellt der \texttt{RtpHandler} die benötigten \gls{rtp}"=Pakete als Liste zusammen.
Fehlt eines dieser Pakete, wird bei aktivierter \gls{fec}"=Funktion der \texttt{FecHandler} mit der Korrektur der entsprechenden Pakete beauftragt.
Scheitert eine Korrektur, kann kein Bild zurückgegeben werden.
Ist die Liste der Medienpakete jedoch vollständig, erstellt der \texttt{RtpHandler} daraus mithilfe der Klasse \texttt{JpegFrame} ein entsprechendes Bild.
Dieses kann dann an den Client zurückgegeben und daraufhin in der grafischen Oberfläche angezeigt werden.
Ebenso wie das Senden von Paketen im Server wird der Abruf und die Anzeige von Bildern im Client von einem Timer gesteuert.
Dieser ist an die grafische Oberfläche gebunden und wird vom Client initialisiert.
Damit stellt die Klasse \texttt{RtpHandler} auch clientseitig Funktionalität bereit, deren Aufruf ausschließlich von außen erfolgt.

Auch die Bereitstellung der im Client angezeigten Empfangsstatistik erhält eine bessere und einfachere Schnittstelle.
Statt alle Werte einzeln zu erfragen, können diese mit der Methode \texttt{getReceptionStatistic()} von der Klasse \texttt{RtpHandler} erhalten werden.
Dafür wird eine neue Klasse \texttt{ReceptionStatistic} bereitgestellt, welche als Datenstruktur dient und keine weitere Funktionalität enthält.
Auf diese Weise kann der Abruf der Empfangsstatistik ohne mehrfache Methodenaufrufe erfolgen.
Für den Fall der Erweiterung der erfassten Werte ist außerdem keine neue Methode, sondern lediglich die Anpassung der entsprechenden Klasse sowie deren Verarbeitung notwendig.

