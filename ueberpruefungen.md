# Überprüfungen vor Abgabe der Arbeit
Einige Überprüfungen, die vor der Abgabe der Arbeit erfolgen sollten.


## Danksagung
evtl. Danksagung schreiben
Prof. Vogt für gute und intensive Betreuung
Prof. Schönthier für die Anmerkungen
Kommilitonen für Korrekturlesen
Lisa/Familie


## [X] TODOs im Latex-Code
Die mit `% todo` markierten Anmerkungen im Latex-Code der Arbeit sollten gänzlich bearbeitet und anschließend entfernt werden.
Außerdem sollten die Kapitel ab Vorbereitung Verschlüsselung nochmal auf Überprüfbarkeit kontrolliert werden. (Was Schönthier dann nicht mehr angemerkt hat.)


## [X] Datum der Abgabe und Unterschrift
Das Datum der Abgabe sowie der Unterschrift der Selbstständigkeitserklärung muss angepasst werden.


## [X] Vorgaben aus den Richtlinien für Abschlussarbeiten
Die Richtlinien für Abschlussarbeiten der Fakultät geben bestimmte Formatierungsregeln vor.
Diese müssen nicht immer eingehalten werden, bieten aber eine gute Orientierung.
Es sollte überprüft werden, ob der Großteil dieser Vorgaben eingehalten sind.


## [X] Inhalt
Da die Arbeit nicht-linear geschrieben wird, kann es zu inhaltlichen Differenzen zwischen Abschnitten kommen.
Zu überprüfen ist deshalb, ob in einem Abschnitt angesprochene Tatsachen bereits in einem vorigen Abschnitt ausgeführt wurden bzw. ob die Voraussetzungen für die Ausführungen eines Abschnitten bereits erläutert wurden.

* Verschlüsselung SRTP
    * Master Salt, Salting, Salz, Schlüssel für Salz, ...


## [X] Rechtschreibung
Neben der allgemeinen Rechtschreibprüfung sollte geschaut werden, dass gleiche/ähnliche Wörter ähnlich geschrieben werden.
Das gilt insbesondere für solche Formulierungen, die mehrere anerkannte Schreibweisen besitzen.
Gegebenenfalls kann dazu der [Duden](duden.de) zurate gezogen werden.
Dabei gilt im Allgemeinen der Grundsatz, die dort empfohlene Schreibweise zu verwenden.

Die nachfolgende Auflistung ist alphabetisch und nach folgender Struktur gegliedert:
Der Hauptpunkt bezeichnet die in der Arbeit verwendete Schreibweise.
Das ist im Allgemeinen die vom Duden empfohlene.
Für Fachbegriffe und ähnliches wird gegebenenfalls davon abgewichen.
Alle Unterpunkte bezeichnen mögliche Schreibweisen.
So können bei der Überprüfung einfach alle im Text vorkommenden Schreibweisen der Unterpunkte durch die des Hauptpunktes ersetzt werden.


* aufwendig
    * aufwändig
* ohne Weiteres
    * ohne weiteres
* Coder (Encoder, Decoder)
    * Koder
* codieren
    * kodieren
* infrage
    * in Frage
* mithilfe
    * mit Hilfe


## [X] Verwendung von Abkürzungen und Wörtern des Glossars
Bei der Erarbeitung werden Glossar und Abkürzungsverzeichnis regelmäßig um Einträge erweitert.
Vor Abgabe der Arbeit sollte überprüft werden, dass die vorhandenen Einträge auch in der Arbeit verwendet werden.


## [X] Worttrennungen
Für die korrekte Worttrennung nutzt LaTeX bestimmte Pakete.
Diese enthalten im Allgemeinen die korrekten Trennungen für die deutsche Sprache.
Es sollte jedoch noch einmal überprüft werden, ob alle getrennten Wörter auch korrekt getrennt wurden.

* HTWDD
    * HTWDD
* RtpHandler
    * Rtp | Handler
* RtspHandler
    * Rtsp | Handler
* Videodateien
    * Vi | deo | da | tei | en


## [X] Gendern
Um das Aufblähen der Inhalte durch das Gendern von Personen oder -gruppen zu vermeiden, sollte zu Beginn der Arbeit auf die einheitliche Verwendung der männlichen Form und deren Geltung für alle Geschlechter hingewiesen werden.
Da in den Richtlinien nichts zum Gendern steht, wird kein Hinweis dazu eingebracht.


## [X] Zeichensetzung und Layout
Damit Latex den Text richtig formatieren kann, sind teilweise besondere Befehle erforderlich.
Diese nutzen teilweise Anführungszeichen oder werden in einer Monospace-Schriftart gesetzt.
Bei der fehlerhaften Verwendung solcher Befehle kann es zu ungewünschten Zeichen im Textsatz kommen.
Auf diese soll die Arbeit vor der Abgabe überprüft werden.
Außerdem kommt es auch vor, dass in Monospace gesetzte Wortgruppen über das Zeilenende hinaus in den Randbereich gehen.
In solchen Fällen muss manuell eine Lösung für den korrekten Schriftsatz gefunden werden.


## [X] Titel von Abbildungen und Tabellen
Für Abbildungsverzeichnis und Tabellenverzeichnis werden die jeweils vergebenen Titel genutzt.
Um die Übersichtlichkeit dieser Verzeichnisse nicht durch zu lange Titel zu gefährden, können bei der Definition von Titeln auch Kurztitel für die Verwendung in solchen Verzeichnissen angegeben werden.
Vor der Abgabe der Arbeit ist deshalb zu überprüfen, dass Abbildungen und Tabellen mit zu langen Titeln einen entsprechend kürzeren Titel für die Verzeichnisse bereitstellen.


## [X] Schreibweise von Fachbegriffen
Für Fachbegriffe wird eine bestimmte Schreibweise genutzt.
Häufig sind sie im Glossar (oder Abkürzungsverzeichnis) enthalten.
Teilweise werden sie aber auch nur in Anführungsstrichen oder in einer Monospace-Schriftart gesetzt.
Vor der Abgabe der Arbeit sollte überprüft werden, dass bei allen Fachbegriffen eine einheitliche Schreibweise verwendet wird.


## [X] Glossareinträge und Abkürzungen
Beim der ersten Verwendung von Abkürzungen wird die volle Schreibweise, bestehend aus langer Formulierung und deren Abkürzung in Klammern, dargestellt.
Bei Kreuzverweisen oder der Verwendung von Abkürzungen im Glossar selbst gelten diese dann bereits als eingeführt, auch wenn sie im Haupttext noch nicht aufgetaucht sind.
Deshalb ist zu überprüfen, dass im Glossar der Befehl `\acrfull` verwendet wird.

In Überschriften sollten Glossareinträge nicht verwendet werden, da dabei derselbe Effekt auftritt.
Der Eintrag der Überschrift im Inhaltsverzeichnis enthielte gegebenenfalls die volle Schreibweise, während die Überschrift im Text nur die Abkürzung enthält.
Auch in Über- oder Unterschriften von Abbildungen, Quellcodes oder Tabellen muss auf die korrekte Verwendung geachtet werden.


## [X] Seitenzahlen
Im Vergleich zu anderen digitalen Publikationen ist aufgefallen, dass Seitenzahlen im besten Fall eindeutig sind.
Um doppelte Seitenzahlen zu vermeiden, sollte den Seiten vor dem Inhaltverzeichnis (Deckblatt, Copyright) eine besondere Identifikation bzw. Seitennummer zugewiesen werden.
Ansonsten sind die Seitenzahlen 1 und 2 doppelt zugeordnet (Deckblatt f., Einleitung f.).


## [X] Selbstständigkeitserklärung
Um die Erklärung etwas umfangreicher zu gestalten, kann ggf. die Formulierung einer anderen Arbeit verwendet werden.
Andererseits entspricht die bereits vorhandene Erklärung dem Wortlaut in den Richtlinien zu Abschnlussarbeiten der Fakultät.

